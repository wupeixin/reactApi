const commonContextActions = {
  updateConfig: "common-update-config-json",
  updateUser: "common-context-set-user",
};

export default commonContextActions;
