import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { commonContextActions, useCommonContext } from "./common-context";
import _ from "lodash";
import { apiLoadingStatus } from "./infrastructure/constants/api";

const CommonAppActions = (props) => {
  const [{ user, config }, dispatchCommonContext] = useCommonContext();

  useEffect(() => {
    const fetchConfig = async () => {
      try {
        const response = await fetch("/client_config.json");
        const data = await response.json();
        dispatchCommonContext({
          type: commonContextActions.updateConfig,
          payload: data,
        });
      } catch (error) {
        console.error("Error fetching config:", error);
      }
    };

    // fetchConfig();
  }, []);

  // this is where you load the user.
  useEffect(() => {
    // you need to load it from the API endpoint
    const tempUser = {
      loadingStatus: apiLoadingStatus.unloaded||localStorage.getItem('isLogin'),
      data: {
       
      },
    };
    dispatchCommonContext({
      type: commonContextActions.updateUser,
      payload: tempUser,
    });
  }, []);

  const { children } = props;
  return <>{children}</>;
};

CommonAppActions.propTypes = {
  children: PropTypes.any,
};

export default CommonAppActions;
