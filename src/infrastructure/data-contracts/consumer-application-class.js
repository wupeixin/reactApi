import _ from "lodash";
import { removeNullOrUndefinedProperties } from "../helpers";

class ConsumerApplicationClass {
  constructor(obj) {
    this.id = _.get(obj, "id") || null;
    this.firstName = _.get(obj, "firstName") || null;
  }
}

export default ConsumerApplicationClass;
