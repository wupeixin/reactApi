//@ref https://stackoverflow.com/questions/7225407/convert-camelcasetext-to-sentence-case-text
const camelToTitle = (str) => str.replace(/([A-Z])/g, " $1");

//@ref https://stackoverflow.com/questions/6122571/simple-non-secure-hash-function-for-javascript
const hashCode = (str) => {
	var hash = 0;
	if (str.length === 0) {
			return hash;
	}
	for (var i = 0; i < str.length; i++) {
			var char = this.charCodeAt(i);
			hash = ((hash<<5)-hash)+char;
			hash = hash & hash; // Convert to 32bit integer
	}
	return hash;
}

export {camelToTitle, hashCode};