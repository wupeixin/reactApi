const fetchFileAndSave = async (url, method, body) => {
  const tempMethod = (method || "GET").toUpperCase();
  const config = {
    credentials: "include", // Will include the cookies in the request
    method: tempMethod,
  };
  if (tempMethod !== "GET") {
    const tempBody = body || {};
    config.body = tempBody;
  }
  const response = await fetch(url, config);
  if (!response.ok) {
    throw new Error(response);
  } else {
    const blob = await response.blob();
    const contentDisposition = response.headers.get("Content-Disposition");
    let filename = "download";

    if (contentDisposition) {
      let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
      let matches = filenameRegex.exec(contentDisposition);
      if (matches != null && matches[1]) {
        filename = matches[1].replace(/['"]/g, "");
      }
    }

    const downloadUrl = window.URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.href = downloadUrl;
    link.download = filename;
    document.body.appendChild(link);
    link.click();
    link.remove();
  }
};

export default fetchFileAndSave;
