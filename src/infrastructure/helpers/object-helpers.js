const removeUndefinedProperties = (obj) => {
  if (_.isObject(obj)) {
    return _.pickBy(obj, _.identity, _.isObject);
  }
  return obj;
};
const removeNullOrUndefinedProperties = (obj) => {
  if (_.isObject(obj)) {
    return _.pickBy(obj, (value) => !(_.isNull(value) || _.isUndefined(value)));
  }
  return obj;
};
export { removeUndefinedProperties, removeNullOrUndefinedProperties };
