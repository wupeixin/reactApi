import _ from "lodash";

const permutationHelper = (collection, n) => {
  const array = _.values(collection);
  if (array.length < n) {
    return [];
  }
  const recur = (array, n) => {
    if (--n < 0) {
      return [[]];
    }
    let permutations = [];
    array.forEach((value, index, array) => {
      array = array.slice();
      array.splice(index, 1);
      recur(array, n).forEach((permutation) => {
        permutation.unshift(value);
        permutations.push(permutation);
      });
    });
    return permutations;
  };
  return recur(array, n);
};
export default permutationHelper;
