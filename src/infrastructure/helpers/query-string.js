const getQueryString = (paramName, defaultValue) => {
	const query = new URLSearchParams(window.location.search);
	const value = query.get(paramName);
	return value != null ? value : defaultValue;
};

/** Helper function to generate a URLSearchParams for axios. Current use case is when there are multiple fields with *same* key in query param
 *  @ref https://stackoverflow.com/questions/42898009/multiple-fields-with-same-key-in-query-params-axios-request
 *  @params {[{key: value}]} Array of parameter objects, as key value pairs
 *  @return a URLSearchParams object
 */
const formatParams = (params) => {
	let params_ = new URLSearchParams();
	params.forEach((param) =>
		Object.entries(param).forEach(([key, val]) =>
			Array.isArray(val)
				? val.forEach((val_) => params_.append(key, val_))
				: params_.append(key, val)
		)
	);
	return params_;
};

export { getQueryString, formatParams };
