const positionHelpers = {
  findElementPosition: (ele) => {
    var currenttop = 0;
    if (ele.offsetParent) {
      do {
        currenttop += ele.offsetTop;
      } while ((ele = ele.offsetParent));
      return [currenttop];
    }
  },
};

export default positionHelpers;
