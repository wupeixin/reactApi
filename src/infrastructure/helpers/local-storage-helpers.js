const localStorageHelpers = {
  setWithExpiry: (key, value, until) => {
    const item = {
      value: value,
      expiry: until,
    };
    localStorage.setItem(key, JSON.stringify(item));
  },
  getWithExpiry: (key) => {
    const itemStr = localStorage.getItem(key);
    if (!itemStr) {
      return null;
    }
    const item = JSON.parse(itemStr);
    const now = new Date();
    if (now.getTime() > item.expiry) {
      localStorage.removeItem(key);
      return null;
    }
    return item.value;
  },
};

export default localStorageHelpers;
