import PropTypes from "prop-types";

/**
 * Helper function to generate Prop Types
 * @param {[Object] | {{Object{}} Array or Object with {type, key} properties
 * @ref https://stackoverflow.com/questions/34287100/extract-read-react-proptypes
 */
const processPropTypes = (propTypes) => {
  let output = {};
  for (const key in propTypes) {
    if (propTypes.hasOwnProperty(key)) {
      if (propTypes[key].value) {
        output[key] = PropTypes[propTypes[key].type](propTypes[key].value);
      }
      else 
        output[key] = PropTypes[propTypes[key].type];
    }
  }
  return output;
};

export default processPropTypes;
