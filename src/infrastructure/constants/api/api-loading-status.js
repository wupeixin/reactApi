const apiLoadingStatus = {
  unloaded: 0,
  loaded: 1,
  error: 2,
  loading: 3,
};

export default apiLoadingStatus
