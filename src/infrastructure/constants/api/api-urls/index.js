import userIdentityUrls from "./user-identity";

// create files and add as many as you wish
const apiUrls = {
  userIdentityUrls,
};

export default apiUrls;
