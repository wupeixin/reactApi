const userIdentityUrls = {
  createUser: `/api/account/createuser`,
  updatePassword: `/api/account/updatePassword`,
  // Add as many as you wish
};

export default userIdentityUrls;
