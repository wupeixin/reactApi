import React, { Component, useEffect } from "react";
import styles from "./app.module.scss";
import { CommonContextProvider } from "./common-context";
import CommonAppActions from "./common-app-actions";
import RoutingHandler from "./router/routing-handler";
import 'react-toastify/dist/ReactToastify.css';

import { getUserInfo} from   "./utils/storage"

const App = () => {
  
  return (
    // <div className={styles.wrapper}>
    
      <CommonContextProvider >
          <CommonAppActions>
              <RoutingHandler ></RoutingHandler>
          </CommonAppActions>
         
      </CommonContextProvider>
    // </div>
  );
};
export default App;
