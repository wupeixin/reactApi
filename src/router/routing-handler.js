import React,{useEffect,useState} from "react";
import { useCommonContext } from "../common-context";
import {IntlProvider} from 'react-intl'
import {Route,Routes,Navigate,BrowserRouter} from 'react-router-dom'


import En from '../locals/en'
import Es from '../locals/es'

import Login from "../pages/login";
import ForgotPwd from "../pages/forgotPassword";
import ForgotPwd2 from "../pages/forgotPassword2";
import Email from "../pages/email";
import NewPassword from "../pages/newPassword";
import Home from "../pages/home";
import Applications from "../pages/applications";
import Consent from '../pages/applications/consent'
import Locations from "../pages/locations";
import UserManagement from "../pages/user-management";
import UserProfile from '../pages/userProfile'
import NewForm from "../pages/newForm";
import ChangePassword from "../pages/changePassword";
import PatientConsent from "../pages/patientConsent";
import PatientStart from "../pages/patientStart";
import PatientStartError from "../pages/patientStartError";
import PatientHome from "../pages/patientHome";
import Billing from "../pages/billing";
import Dashboard from "../pages/dashboard";
import Leads from "../pages/Leads";
import Details from "../pages/LocationDetails";
import Test from '../pages/Test'
import AccountUpdate from '../pages/user-management/accountUpdate'


import TermService from '../pages/termService'
import NotFind from "../pages/404"



const RoutingHandler = (props) => {
  // for example, if you need user, load it from here:
  const [{ user, config }] = useCommonContext();
  const [langInfo,setLangInfo]=useState({
    ...En
  })
  let flag=!!user.loadingStatus||!!localStorage.getItem("isLogin")

  useEffect(()=>{
    console.log(config,'change lang')
    let langType=localStorage.getItem("lang")
    if(config.lang==='es'||langType==='es'){
      setLangInfo({...Es})
    }else if(config.lang==="en"||langType==="en"){
      setLangInfo({...En})
    }

  },[config])


  return (
    <IntlProvider messages={langInfo} locale={config.lang}>
    <BrowserRouter>
      <Routes>
        {/* React v6!!!!  */}
        <Route path="/" element={<Navigate to="/account/login"  replace/>}/>
        <Route path="/account/login" element={<Login/>} />
        <Route path="/account/recovery" element={<ForgotPwd/>} />
        <Route path="/account/recovery-change" element={<ForgotPwd2/>} />
        <Route path="/account/send-email/:code" element={<Email/>} />
        <Route path="/account/update-password/:code" element={<NewPassword/>} />
        <Route path="/account/account-update/:code" element={<AccountUpdate/>}></Route>
        <Route path="/account/patient-start" element={<PatientStart/>} />
        <Route path="/account/patient-start-error" element={<PatientStartError/>} />
        <Route path="/account/term-service" element={<TermService/>} />
        
        <Route path="/account/home" element={flag?<Home/>:<Navigate to="/account/login"  replace/>}  />
       
        <Route path="/account/applications"  element={<>{flag?<Applications/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/consent"  element={<>{flag?<Consent/>:<Navigate to="/account/login"  replace/>}</>} />

        <Route path="/account/locations" element={<>{flag?<Locations/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/user-management" element={<>{flag?<UserManagement/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/user-profile" element={<>{flag?<UserProfile/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/new-form" element={<>{flag?<NewForm/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/change-password" element={<>{flag?<ChangePassword/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/patient-consent" element={<>{flag?<PatientConsent/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/patient-home" element={<>{flag?<PatientHome/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/billing" element={<>{flag?<Billing/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/dashboard" element={<>{flag?<Dashboard/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/leads" element={<>{flag?<Leads/>:<Navigate to="/account/login"  replace/>}</>} />
        <Route path="/account/location-detail" element={<>{flag?<Details/>:<Navigate to="/account/login"  replace/>}</>} />

        <Route path="/account/test" element={<Test/>}></Route>

        <Route path="/404" element={<NotFind/>} ></Route>
        <Route path="*" element={<Navigate to="/404"  replace/>} ></Route>
      </Routes>
    </BrowserRouter>
    </IntlProvider>
  );
};

RoutingHandler.propTypes = {};

export default RoutingHandler;
