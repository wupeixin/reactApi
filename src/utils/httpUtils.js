import request from './request.js'
import Qs from 'qs'

// export function sendGet(url) {
//   return request({
//     url: url,
//     method: 'get',
//     params: null
//   })
// }

export function sendGetKeyValue(url,param) {
  return request({
    url: url,
    method: 'get',
    params: param,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      "User-ID":"1",
      "X-Correlation-ID":"00000000-0000-0000-0000-000000000000",
      "X-Forwarded-For":"192.168.0.1",
      // "User-Agent":"Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3",
    },
  })
}

export function sendGet(url,userId) {
  return request({
    url: url,
    method: 'get',
    params: null,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      "User-ID":userId,
      "X-Correlation-ID":"00000000-0000-0000-0000-000000000000",
      "X-Forwarded-For":"192.168.0.1",
      // "User-Agent":"Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3",
    },
  })
}

export function sendPost(url) {
  return request({
    url: url,
    method: 'post',
    params: null,
    transformRequest: [function(data) {
      data = JSON.stringify(data)
      return data
    }],
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function sendPostByKeyValue(url, param) {
  return request({
    url: url,
    method: 'post',
    params: param,
    transformRequest: [function(data) {
      data = Qs.stringify(data)
      return data
    }],
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      "User-ID":3,
      "X-Correlation-ID":"00000000-0000-0000-0000-000000000000",
      "X-Forwarded-For":"192.168.0.1",
    }
  })
}

export function sendPostByFormData(url, value) {
  return request({
    url: url,
    method: 'post',
    params: {},
    // transformRequest: [function(data) {
    //   data = JSON.stringify(data)
    //   return data
    // }],
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    data: value
  })
}

export function sendPostByJson(url, param) {
  return request({
    url: url,
    method: 'post',
    params: {},
    transformRequest: [function(data) {
      data = JSON.stringify(data)
      return data
    }],
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      "User-ID":3,
      "X-Correlation-ID":"00000000-0000-0000-0000-000000000000",
      "X-Forwarded-For":"192.168.0.1",
      // "User-Agent":"Mozilla/5.0 (iPhone; CPU iPhone OS 5_1_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B206 Safari/7534.48.3",
    },
    data: param
  })
}
export function uploadFiles(url, param,headers) {
  return request({
    url: url,
    method: 'post',
    params: {},
  
    headers:headers,
    data: param
  })
}