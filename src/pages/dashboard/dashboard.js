import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { checkIsEmail } from "../../utils/check.js";
import TitleBar from "../../components/header/titleBar.js";
import $ from "jquery"
// import { FormattedMessage,useIntl} from "react-intl";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";
// import PropTypes from "prop-types";
// import loginStyle from "../../css/static.css";
// import { useSnackbar } from 'notistack';
import { setUserInfo } from "../../utils/storage.js"
// import OutlinedInput from '@mui/material/OutlinedInput';
import DashboardChart from "../../components/dashboardChart/dashboardChart.js"
import DashboardMap from "../../components/dashboardMap/dashboardMap.js"
import KpiReport from "../../components/KpiReport/index.js"
const Dashboard = (props) => {
  const navigate = useNavigate();

    const closePopup = () => {
    $(".popup").removeClass("show");

  }
  const openPopup = () => {
    $(".popup").addClass("show");
  }
  const openFifter = () => {

    $(".offcanvas").addClass("show");

  };


  const delLocation = () => {
    $(".popup").removeClass("show");

  }
  return (
    <>
      <div className="layout">
        <header>
          <div className="nav container">
            <button className="btn_return me_3" onClick={() => navigate("/account/home")} />
            <div className="title">Dashboard</div>
          </div>
        </header>
        <div className="main">
          <div className="container">
            <KpiReport />

            {/* <button className="btn btn_primary  col" >KPI Reporting</button> */}
            <div className="card h_100 mt_4 mb_4">
              <div className="row">
                <div className="col col_2_sm col_2_md  mt_5" style={{ height: "300px" }}>
                  <DashboardMap />
                </div>
                <div className="col col_2_sm col_2_md  mt_5" style={{ height: "300px" }}>
                  <DashboardChart />
                </div>
              </div>
            </div>

            <button className="btn btn_primary col" >Summary Report</button>
            <div className="card h_100 mt_4 mb_4">
              <div className="card_content d_flex_sm justify_between my_2_md" style={{ overflowX: "auto" }}>
                <table cellSpacing={10} style={{ overflowX: "auto" }}>
                  <thead>
                    <tr>
                      <th scope="col" style={{ width: "400px",border:"2px"  }}>KPI</th>
                      <th scope="col" style={{ width: "200px" }}>#1 HGC</th>
                      <th scope="col" style={{ width: "200px" }}>#2 HGC</th>
                      <th scope="col" style={{ width: "200px" }}>#3 HGC</th>
                      <th scope="col" style={{ width: "200px" }}>#4 HGC</th>
                      <th scope="col" style={{ width: "200px" }}>Other</th>
                      <th scope="col" style={{ width: "200px" }}>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr style={{ width: "200px",textAlign:"center",border:"2px" }}>
                      <td style={{border:"2px" }}>Initiated Applications</td>
                      <td  style={{border:"2px" }}>London Calling</td>
                      <td>London Calling</td>
                      <td>ne you shouldn't've</td>
                      <td>	London Calling</td>
                      <td>	London Calling</td>
                      <td>	London Calling</td>
                    </tr>

                    <tr style={{ width: "200px" ,textAlign:"center" }}>
                      <td >Pre-approved Applications</td>
                      <td>London Calling</td>
                      <td>London Calling</td>
                      <td>London Calling</td>
                    </tr>

                    <tr style={{ width: "200px",textAlign:"center"  }}>
                      <td >Submitted Applications</td>
                      <td>London Calling</td>
                      <td>London Calling</td>
                      <td>No More Heroes</td>
                    </tr>
                    <tr style={{ width: "200px",textAlign:"center"  }}>
                      <td >Submitted Rate</td>
                      <td>London Calling</td>
                      <td>London Calling</td>
                      <td>No More Heroes</td>
                    </tr>
                    <tr style={{ width: "200px",textAlign:"center"  }}>
                      <td >Total Initiated Applications Amount</td>
                      <td>London Calling</td>
                      <td>London Calling</td>
                      <td>No More Heroes</td>
                    </tr>
                    <tr style={{ width: "200px",textAlign:"center"  }}>
                      <td >Total Pre-approved Amount</td>
                      <td>London Calling</td>
                      <td>London Calling</td>
                      <td>No More Heroes</td>
                    </tr>
                    <tr style={{ width: "200px",textAlign:"center"  }}>
                      <td >Total Submitted Applications Amount</td>
                      <td>London Calling</td>
                      <td>London Calling</td>
                      <td>No More Heroes</td>
                    </tr>
                    <tr style={{ width: "200px" ,textAlign:"center" }}>
                      <td >Average Initiated Applications Amount</td>
                      <td>London Calling</td>
                      <td>London Calling</td>
                      <td>No More Heroes</td>
                    </tr>
                    <tr style={{ width: "200px",textAlign:"center"  }}>
                      <td >Average Pre-approved Amount</td>
                      <td>London Calling</td>
                      <td>London Calling</td>
                      <td>No More Heroes</td>
                    </tr>
                    <tr style={{ width: "200px",textAlign:"center"  }}>
                      <td >Average Submitted Applications Amount</td>
                      <td>London Calling</td>
                      <td>London Calling</td>
                      <td>No More Heroes</td>
                    </tr>
                  </tbody>
                  {/* <tfoot>
                    <tr style={{width:"200px"}}>
                    <td >Pre-approved Rate</td>
                      <td >77</td>
                    </tr>
                  </tfoot> */}
                </table>
              </div>
            </div>
            <button className="btn btn_primary  col" >Download Center</button>
            <ul className="row mt_4 mb_4">
              <li className="col col_2_sm col_2_md">
                <div className="card h_100">
                  <div className="card_title">
                    <div className="text">
                      <span>Report 1</span>
                    </div>
                    <button className="btn down ms_auto"/>
                    {/* <button className="btn delete" /> */}
                  </div>
                  <div className="card_content d_flex_sm justify_between my_2_md">
                    <table className="table">
                      <tbody className="">
                        <tr>
                          <td className="color_gray fs_sm">KPI:</td>
                          <td className="color_gray fs_sm">Total Submitted Applications Amount</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm">User:</td>
                          <td className="color_gray fs_sm">All</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> HCG:</td>
                          <td className="color_gray fs_sm">All</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> Location:</td>
                          <td className="color_gray fs_sm">All</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> From:</td>
                          <td className="color_gray fs_sm"> 8/20/23</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> To:</td>
                          <td className="color_gray fs_sm">9/20/23</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> Report Status:</td>
                          <td className="color_success">Ready</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </li>
              <li className="col col_2_sm col_2_md">
                <div className="card h_100">
                  <div className="card_content  justify_between my_2_md">
                    <form className="form ">
                      <div className="row">
                        <div className="mb_2 col col_2_sm col_1_md">
                          <input
                            className="form_control"
                            type="text"
                            placeholder="Report Name"

                          />
                        </div>
                        <div className="mb_2 col col_2_sm col_3_md d_none d_block_md">
                          <input
                            className="form_control d_none d_block_md"
                            type="submit"
                            value="Create"
                            style={{ backgroundColor: "#14A38B", color: "#fff", fontSize: "15px" }}
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="mb_2 col col_sm col_md">
                          <select className="form_control">
                            <option>KPI</option>
                          </select>
                        </div>
                      </div>
                      <div className="row">
                        <div className="mb_2 col col_sm col_md">
                          <select className="form_control">
                            <option>User</option>
                          </select>
                        </div>
                      </div>
                      <div className="row">
                        <div className="mb_2 col col_sm col_md">
                          <select className="form_control">
                            <option>HCG</option>
                          </select>
                        </div>
                      </div>
                      <div className="row">
                        <div className="mb_2 col col_sm col_md">
                          <select className="form_control">
                            <option>Location</option>
                          </select>
                        </div>
                      </div>
                      <div className="row">
                        <div className="mb_2 col col_2_sm col_2_md">
                          <input type="Date" className="form_control"></input>
                        </div>
                        <div className="mb_2 col col_2_sm col_2_md">
                          <input type="Date" className="form_control"></input>

                        </div>
                      </div>
                      <div className="row">
                        <input
                          className="form_control d_none_md btn"
                          type="submit"
                          value="Create"
                          style={{ backgroundColor: "#14A38B", color: "#fff" }}
                        />
                      </div>
                    </form>
                  </div>
                </div>
              </li>
            </ul>

            <button className="btn btn_primary  col" >KPI Definitions</button>
            <div className="mt_4 mb_4 ">
              <p>              Number of submitted Applications: The distinct number of applications sent by DentiRate for pre-approval.
              </p>
              <p>              Number of Pre-Approved Applications: The distinct number of applications that pre-approved
              </p>
              <p>              Number of Leads: The distinct number of leads that sent by patient(s)
              </p>
              <p>              Pre-Approved Rate: the percentage of pre-approved applications among all applications submitted through the DentiRate app.
              </p>
              <p>              Submitted Rate: the percentage of delivered leads out of all applications requested for pre-approval through the DentiRate App.
              </p>
              <p>              Total Requested Amount: the total requested amount via DentiRate App
              </p>
              <p>              Total Pre-Approved Amount: the total amount pre-approved by lender
              </p>
              <p>              Total Lead Amount: total amount of finalized leads
              </p>
              <p>              Average submitted Applications Amount: Total requested Amount / Number of submitted Applications
              </p>
              <p>              Average Pre-Approved Amount: Total Pre-Approved Amount / Number of Pre-Approved Applications
              </p>
              <p>              Average Submitted Applications Amount: Total lead amount / Number of leads
              </p>
              <p>              Number of Active Users: the number of users with active profile
              </p>
              <p>              Number of Active Locations: the number of registered locations that are active
              </p>
              <p>              Number of Active HCG: the number of registered HCG that are active
              </p>

            </div>

          </div>
        </div>
      </div>


    </>


  );
};

Dashboard.propTypes = {};

export default Dashboard;
