import React, { useState,useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import {commonContextActions,useCommonContext} from '../../common-context'
import { checkIsEmail } from "../../utils/check.js";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";
import {clearAllStorages} from "../../utils/storage"
import { setUserInfo,getUserInfo,getItem} from "../../utils/storage.js"

const UserProfile = (props) => {
    const [{ user, config }, dispatchCommonContext] = useCommonContext();
    const navigate = useNavigate();
    const source =parseInt(getItem("source")) 
    const [account,setAccount]=useState({})
    const roleName= JSON.parse(getItem("role")) 
       useEffect(()=>{
           let user=getUserInfo()
          console.log(user);
           if(user){
         
            setAccount({...user})
           }
    },[])   
    
    const Logout=()=>{
        clearAllStorages()
        user.loadingStatus=1
        dispatchCommonContext(
            {
              type: commonContextActions.updateUser,
              payload: user,
            }
          )
        navigate('/')
    }
    const changePwd=()=>{
        navigate('/account/change-password')
    }
    return (
        <>


            <div className="layout">
                <header>
                    <div className="nav container">
                        {(source===101||source===100)?<button className="btn_return me_3" onClick={() => navigate("/account/applications")} />:
                        <button className="btn_return me_3" onClick={() => navigate("/account/home")} />}
                        <div className="title">Admin profile</div>
                    </div>
                </header>
                <div className="main">
                    <div className="container profile_container">
                        <div className="profile_box mt_2 mt_4_sm mt_8_md">
                            <img
                                className="icon"
                                src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGNpcmNsZSBjeD0iMTIiIGN5PSI2IiByPSI0IiBmaWxsPSIjMUY0QTk0Ii8+CjxlbGxpcHNlIGN4PSIxMiIgY3k9IjE3IiByeD0iNyIgcnk9IjQiIGZpbGw9IiMxRjRBOTQiLz4KPC9zdmc+Cg=="
                                alt=""
                            />
                            <div className="">
                                <div className="name">
                                    {account.firstName?account.firstName:""}&nbsp;{account.lastName?account.lastName:""} 
                                    <span className="role">{" ("+roleName+")"}</span>
                                </div>
                                <div className="email mt_1">{account.email?account.email:''}</div>
                            </div>
                        </div>
                        <div className="mt_7 mt_8_md">
                            <a className="link_primary link" href="" onClick={changePwd}>
                                <img
                                    className="icon me_2"
                                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTUuMzMzMzcgOC42NjY5OUM1LjcwMTU2IDguNjY2OTkgNi4wMDAwNCA4LjM2ODUyIDYuMDAwMDQgOC4wMDAzM0M2LjAwMDA0IDcuNjMyMTQgNS43MDE1NiA3LjMzMzY2IDUuMzMzMzcgNy4zMzM2NkM0Ljk2NTE5IDcuMzMzNjYgNC42NjY3MSA3LjYzMjE0IDQuNjY2NzEgOC4wMDAzM0M0LjY2NjcxIDguMzY4NTIgNC45NjUxOSA4LjY2Njk5IDUuMzMzMzcgOC42NjY5OVoiIGZpbGw9IiMzNDdBRjYiLz4KPHBhdGggZD0iTTguNjY2NzEgOC4wMDAzM0M4LjY2NjcxIDguMzY4NTIgOC4zNjgyMyA4LjY2Njk5IDguMDAwMDQgOC42NjY5OUM3LjYzMTg1IDguNjY2OTkgNy4zMzMzNyA4LjM2ODUyIDcuMzMzMzcgOC4wMDAzM0M3LjMzMzM3IDcuNjMyMTQgNy42MzE4NSA3LjMzMzY2IDguMDAwMDQgNy4zMzM2NkM4LjM2ODIzIDcuMzMzNjYgOC42NjY3MSA3LjYzMjE0IDguNjY2NzEgOC4wMDAzM1oiIGZpbGw9IiMzNDdBRjYiLz4KPHBhdGggZD0iTTEwLjY2NjcgOC42NjY5OUMxMS4wMzQ5IDguNjY2OTkgMTEuMzMzNCA4LjM2ODUyIDExLjMzMzQgOC4wMDAzM0MxMS4zMzM0IDcuNjMyMTQgMTEuMDM0OSA3LjMzMzY2IDEwLjY2NjcgNy4zMzM2NkMxMC4yOTg1IDcuMzMzNjYgMTAgNy42MzIxNCAxMCA4LjAwMDMzQzEwIDguMzY4NTIgMTAuMjk4NSA4LjY2Njk5IDEwLjY2NjcgOC42NjY5OVoiIGZpbGw9IiMzNDdBRjYiLz4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik02LjYyOTEgMi4xNjY5OUM1LjQwMzkzIDIuMTY2OTggNC40MzM1IDIuMTY2OTcgMy42NzQwMyAyLjI2OTA4QzIuODkyNDEgMi4zNzQxNyAyLjI1OTc4IDIuNTk1NTggMS43NjA4NyAzLjA5NDQ5QzEuMjYxOTYgMy41OTM0IDEuMDQwNTUgNC4yMjYwMyAwLjkzNTQ2NCA1LjAwNzY0QzAuODMzMzU2IDUuNzY3MTIgMC44MzMzNjQgNi43Mzc1NCAwLjgzMzM3NCA3Ljk2MjdWOC4wMzc5NEMwLjgzMzM2NCA5LjI2MzExIDAuODMzMzU2IDEwLjIzMzUgMC45MzU0NjQgMTAuOTkzQzEuMDQwNTUgMTEuNzc0NiAxLjI2MTk2IDEyLjQwNzMgMS43NjA4NyAxMi45MDYyQzIuMjU5NzggMTMuNDA1MSAyLjg5MjQxIDEzLjYyNjUgMy42NzQwMyAxMy43MzE2QzQuNDMzNSAxMy44MzM3IDUuNDAzOTEgMTMuODMzNyA2LjYyOTA3IDEzLjgzMzdIOS4zNzA5OEMxMC41OTYxIDEzLjgzMzcgMTEuNTY2NiAxMy44MzM3IDEyLjMyNjEgMTMuNzMxNkMxMy4xMDc3IDEzLjYyNjUgMTMuNzQwMyAxMy40MDUxIDE0LjIzOTIgMTIuOTA2MkMxNC43MzgxIDEyLjQwNzMgMTQuOTU5NSAxMS43NzQ2IDE1LjA2NDYgMTAuOTkzQzE1LjE2NjcgMTAuMjMzNSAxNS4xNjY3IDkuMjYzMTIgMTUuMTY2NyA4LjAzNzk2VjcuOTYyNzJDMTUuMTY2NyA2LjczNzU2IDE1LjE2NjcgNS43NjcxMSAxNS4wNjQ2IDUuMDA3NjRDMTQuOTU5NSA0LjIyNjAzIDE0LjczODEgMy41OTM0IDE0LjIzOTIgMy4wOTQ0OUMxMy43NDAzIDIuNTk1NTggMTMuMTA3NyAyLjM3NDE3IDEyLjMyNjEgMi4yNjkwOEMxMS41NjY2IDIuMTY2OTcgMTAuNTk2MiAyLjE2Njk4IDkuMzcxIDIuMTY2OTlINi42MjkxWk0yLjQ2Nzk4IDMuODAxNTlDMi43NTAxMiAzLjUxOTQ2IDMuMTM2NTUgMy4zNTAzNCAzLjgwNzI3IDMuMjYwMTdDNC40OTIzOCAzLjE2ODA1IDUuMzk1NDkgMy4xNjY5OSA2LjY2NjcxIDMuMTY2OTlIOS4zMzMzN0MxMC42MDQ2IDMuMTY2OTkgMTEuNTA3NyAzLjE2ODA1IDEyLjE5MjggMy4yNjAxN0MxMi44NjM1IDMuMzUwMzQgMTMuMjUgMy41MTk0NSAxMy41MzIxIDMuODAxNTlDMTMuODE0MiA0LjA4MzczIDEzLjk4MzQgNC40NzAxNyAxNC4wNzM1IDUuMTQwODlDMTQuMTY1NiA1LjgyNiAxNC4xNjY3IDYuNzI5MTEgMTQuMTY2NyA4LjAwMDMzQzE0LjE2NjcgOS4yNzE1NCAxNC4xNjU2IDEwLjE3NDcgMTQuMDczNSAxMC44NTk4QzEzLjk4MzQgMTEuNTMwNSAxMy44MTQyIDExLjkxNjkgMTMuNTMyMSAxMi4xOTkxQzEzLjI1IDEyLjQ4MTIgMTIuODYzNSAxMi42NTAzIDEyLjE5MjggMTIuNzQwNUMxMS41MDc3IDEyLjgzMjYgMTAuNjA0NiAxMi44MzM3IDkuMzMzMzcgMTIuODMzN0g2LjY2NjcxQzUuMzk1NDkgMTIuODMzNyA0LjQ5MjM4IDEyLjgzMjYgMy44MDcyNyAxMi43NDA1QzMuMTM2NTUgMTIuNjUwMyAyLjc1MDEyIDEyLjQ4MTIgMi40Njc5OCAxMi4xOTkxQzIuMTg1ODQgMTEuOTE2OSAyLjAxNjcyIDExLjUzMDUgMS45MjY1NSAxMC44NTk4QzEuODM0NDQgMTAuMTc0NyAxLjgzMzM3IDkuMjcxNTQgMS44MzMzNyA4LjAwMDMzQzEuODMzMzcgNi43MjkxMSAxLjgzNDQ0IDUuODI2IDEuOTI2NTUgNS4xNDA4OUMyLjAxNjcyIDQuNDcwMTcgMi4xODU4NCA0LjA4MzczIDIuNDY3OTggMy44MDE1OVoiIGZpbGw9IiMzNDdBRjYiLz4KPC9zdmc+Cg=="
                                    alt=""
                                />
                                <span className="fs_lg fw_medium">Change password</span>
                            </a>
                        </div>
                        <div className="mt_5 mt_6_md">
                            <a className="link_error link" href=""  onClick={Logout}>
                                <img
                                    className="icon me_2"
                                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzM5NTVfMTg2OTcpIj4KPHBhdGggZD0iTTkuOTYzNiAwLjgzMzAwOEM5LjA1MTg4IDAuODMyOTk1IDguMzE2OTkgMC44MzI5ODQgNy43MzkwMiAwLjkxMDY5MUM3LjEzODk0IDAuOTkxMzY4IDYuNjMzNyAxLjE2Mzk2IDYuMjMyNDIgMS41NjUyNEM1Ljg4MjQ2IDEuOTE1MiA1LjcwNTY0IDIuMzQ1MjQgNS42MTI4MSAyLjg1MDU4QzUuNTIyNjIgMy4zNDE2MiA1LjUwNTM2IDMuOTQyNTQgNS41MDEzNSA0LjY2MzU2QzUuNDk5ODEgNC45Mzk3IDUuNzIyNDIgNS4xNjQ4IDUuOTk4NTYgNS4xNjYzM0M2LjI3NDcgNS4xNjc4NyA2LjQ5OTggNC45NDUyNiA2LjUwMTM0IDQuNjY5MTNDNi41MDUzOSAzLjk0MDEzIDYuNTI0MzMgMy40MjM0IDYuNTk2MzYgMy4wMzEyNEM2LjY2NTc3IDIuNjUzMzcgNi43NzcyMiAyLjQzNDY1IDYuOTM5NTIgMi4yNzIzNUM3LjEyNDAzIDIuMDg3ODQgNy4zODMwOCAxLjk2NzU0IDcuODcyMjYgMS45MDE3N0M4LjM3NTgzIDEuODM0MDcgOS4wNDMyNCAxLjgzMzAxIDEwLjAwMDIgMS44MzMwMUgxMC42NjY5QzExLjYyMzggMS44MzMwMSAxMi4yOTEyIDEuODM0MDcgMTIuNzk0OCAxLjkwMTc3QzEzLjI4NCAxLjk2NzU0IDEzLjU0MyAyLjA4Nzg0IDEzLjcyNzUgMi4yNzIzNUMxMy45MTIgMi40NTY4NiAxNC4wMzIzIDIuNzE1OTEgMTQuMDk4MSAzLjIwNTA5QzE0LjE2NTggMy43MDg2NiAxNC4xNjY5IDQuMzc2MDYgMTQuMTY2OSA1LjMzMzAxVjEwLjY2NjNDMTQuMTY2OSAxMS42MjMzIDE0LjE2NTggMTIuMjkwNyAxNC4wOTgxIDEyLjc5NDNDMTQuMDMyMyAxMy4yODM0IDEzLjkxMiAxMy41NDI1IDEzLjcyNzUgMTMuNzI3QzEzLjU0MyAxMy45MTE1IDEzLjI4NCAxNC4wMzE4IDEyLjc5NDggMTQuMDk3NkMxMi4yOTEyIDE0LjE2NTMgMTEuNjIzOCAxNC4xNjYzIDEwLjY2NjkgMTQuMTY2M0gxMC4wMDAyQzkuMDQzMjQgMTQuMTY2MyA4LjM3NTgzIDE0LjE2NTMgNy44NzIyNiAxNC4wOTc2QzcuMzgzMDggMTQuMDMxOCA3LjEyNDAzIDEzLjkxMTUgNi45Mzk1MiAxMy43MjdDNi43NzcyMiAxMy41NjQ3IDYuNjY1NzcgMTMuMzQ2IDYuNTk2MzYgMTIuOTY4MUM2LjUyNDMzIDEyLjU3NTkgNi41MDUzOSAxMi4wNTkyIDYuNTAxMzQgMTEuMzMwMkM2LjQ5OTggMTEuMDU0MSA2LjI3NDcgMTAuODMxNSA1Ljk5ODU2IDEwLjgzM0M1LjcyMjQyIDEwLjgzNDYgNS40OTk4MSAxMS4wNTk3IDUuNTAxMzUgMTEuMzM1OEM1LjUwNTM2IDEyLjA1NjggNS41MjI2MiAxMi42NTc3IDUuNjEyODEgMTMuMTQ4OEM1LjcwNTY0IDEzLjY1NDEgNS44ODI0NiAxNC4wODQyIDYuMjMyNDIgMTQuNDM0MUM2LjYzMzcgMTQuODM1NCA3LjEzODk0IDE1LjAwOCA3LjczOTAyIDE1LjA4ODdDOC4zMTcgMTUuMTY2NCA5LjA1MTg4IDE1LjE2NjQgOS45NjM2IDE1LjE2NjNIMTAuNzAzNEMxMS42MTUyIDE1LjE2NjQgMTIuMzUgMTUuMTY2NCAxMi45MjggMTUuMDg4N0MxMy41MjgxIDE1LjAwOCAxNC4wMzMzIDE0LjgzNTQgMTQuNDM0NiAxNC40MzQxQzE0LjgzNTkgMTQuMDMyOCAxNS4wMDg1IDEzLjUyNzYgMTUuMDg5MiAxMi45Mjc1QzE1LjE2NjkgMTIuMzQ5NSAxNS4xNjY5IDExLjYxNDcgMTUuMTY2OSAxMC43MDI5VjUuMjk2NDNDMTUuMTY2OSA0LjM4NDcgMTUuMTY2OSAzLjY0OTgyIDE1LjA4OTIgMy4wNzE4NEMxNS4wMDg1IDIuNDcxNzcgMTQuODM1OSAxLjk2NjUyIDE0LjQzNDYgMS41NjUyNEMxNC4wMzMzIDEuMTYzOTYgMTMuNTI4MSAwLjk5MTM2OCAxMi45MjggMC45MTA2OTFDMTIuMzUgMC44MzI5ODQgMTEuNjE1MiAwLjgzMjk5NSAxMC43MDM0IDAuODMzMDA4SDkuOTYzNloiIGZpbGw9IiNGRjcxNzEiLz4KPHBhdGggZD0iTTEwIDcuNDk5NjhDMTAuMjc2MiA3LjQ5OTY4IDEwLjUgNy43MjM1MyAxMC41IDcuOTk5NjhDMTAuNSA4LjI3NTgyIDEwLjI3NjIgOC40OTk2OCAxMCA4LjQ5OTY4SDIuNjg1TDMuOTkyMSA5LjYyMDA1QzQuMjAxNzcgOS43OTk3NiA0LjIyNjA1IDEwLjExNTQgNC4wNDYzNCAxMC4zMjUxQzMuODY2NjIgMTAuNTM0NyAzLjU1MDk3IDEwLjU1OSAzLjM0MTMxIDEwLjM3OTNMMS4wMDc5OCA4LjM3OTNDMC44OTcxNTUgOC4yODQzMSAwLjgzMzM3NCA4LjE0NTY0IDAuODMzMzc0IDcuOTk5NjhDMC44MzMzNzQgNy44NTM3MSAwLjg5NzE1NSA3LjcxNTA0IDEuMDA3OTggNy42MjAwNUwzLjM0MTMxIDUuNjIwMDVDMy41NTA5NyA1LjQ0MDM0IDMuODY2NjIgNS40NjQ2MiA0LjA0NjM0IDUuNjc0MjhDNC4yMjYwNSA1Ljg4Mzk0IDQuMjAxNzcgNi4xOTk1OSAzLjk5MjEgNi4zNzkzTDIuNjg1IDcuNDk5NjhIMTBaIiBmaWxsPSIjRkY3MTcxIi8+CjwvZz4KPGRlZnM+CjxjbGlwUGF0aCBpZD0iY2xpcDBfMzk1NV8xODY5NyI+CjxyZWN0IHdpZHRoPSIxNiIgaGVpZ2h0PSIxNiIgcng9IjUiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="
                                    alt=""
                                />
                                <span className="fs_lg fw_medium">Log out</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>




        </>



    );
};

UserProfile.propTypes = {};

export default UserProfile;
