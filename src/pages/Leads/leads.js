import React, { useState,useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";




import TitleBar from "../../components/header/titleBar.js";
import Filter from "../element/fifter/fifter.js";
import $ from "jquery";

import { sendPostByJson } from "../../utils/httpUtils.js";



const sortList=[
  "SubmitDate",
  "Amount",
  "CreateDate",
  "ApplicationStatus",
  "ExpirationDate",
  "AbandonDate",
  "TreatmentType",
  "FirstName",
  "LastName",
  "LastUpdateDate",
]
const stateList=[
  "None",
  "Initiated",
  "Submitted",
  "PendingMoreInformation",
  "Processing",
  "ProcessedSuccessfuly",
  "ProcessingError",
  "OpenedByApplicant",
  "ApplicantApplied",
  "Abandoned"
]
const treatmentTypeList=[
  {key:"1",value:"One Week"},
  {key:"2",value:"One Month"},
  {key:"3",value:"One Year"},
  {key:"4",value:"Max"},
]
const marks={
  "0":"0",
  "20000": "$200,00"
}

const Leads = (props) => {

  const navigate = useNavigate();
  $(".js-range-slider").ionRangeSlider({
    type: "double",
    min: 0,
    max: 20000,
    from: 200,
    to: 800,
    prefix: "$",

  });
  const [dataList,setDataList]=useState([{
    
      "id": "",
      "firstName": "",
      "lastName": "",
      "applicationStatus": "None",
      "homePhone": "",
      "workPhone": "",
      "cellPhone": "",
      "emailAddress": "",
      "amount": 0,
      "treatmentType": "",
      "preferredMonthlyPayment": 0,
      "locationId": 0,
      "healthCareGroupId": 0,
      "initiatorUserId": "",
      "createDate": "",
      "submittedByUserId": "",
      "submitDate": "",
      "lastUpdateUserId": "",
      "lastUpdateDate": ""
    }
  ])
  const [filterObj,setFilterObj]=useState({
    keyWord:"",
    sortBy:"",
    status:"",
    amount:"",
    treatmentTypeCode:1,
    treatmentType:"",
    address:"",
    minAmount:"",
    maxAmount:""
  })
  const [showFilter,setShowFilter]=useState(false)
  const [locationStr,setLocationStr]=useState("")
  const [locationList,setLocationList]=useState([])
  
  //effect
  useEffect(()=>{

  },[])


  const getApplicationList=async()=>{
    const url="/Application/GetPaginated"
    const data={}
    const res= await sendPostByJson(url,data)
    if(res.data){
      setDataList(res.data.consumerApplicationCollection)
    }
  }

  
  const filterHandl=(e)=>{
    e.preventDefault()
    //  getApplicationList()
  }
  const changeSortBy=(v)=>{
      setFilterObj({...filterObj,sortBy:v})
      
  }
  const changeState=(v)=>{
    setFilterObj({...filterObj,status:v})
  }
  const changeSelect=(e)=>{
    let val=e.target.value
    let type = treatmentTypeList.find(v=>v.key==val).value
    console.log(type);
    filterObj.treatmentTypeCode=val
    filterObj.treatmentType=type
    setFilterObj({...filterObj})
  
  }
  const changeRange=(v)=>{
   
    filterObj.minAmount=v[0]
    filterObj.maxAmount=v[1]
    setFilterObj({...filterObj})
  }
  const openFifter = () => {

    setShowFilter(true)

  };
  const closeFifter = () => {

    setShowFilter(false)

  };

  const addFifter = () => {
    setShowFilter(false)

   

  };
  return (
    <>
      <div className="layout">
        <TitleBar title={"Leads"}/>
        <div className="main">
          <div className="container">
            <div className="filter_box mt_2">
              <button className="btn btn_filter btn_md" onClick={openFifter}>
                <img
                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0zLjMwMTY4IDEuNUMzLjMxMjA5IDEuNSAzLjMyMjUzIDEuNSAzLjMzMzAxIDEuNUwxMi42OTc3IDEuNUMxMy4xNDIzIDEuNDk5OTggMTMuNTIyNCAxLjQ5OTk2IDEzLjgyNjIgMS41MzgyOEMxNC4xNDg0IDEuNTc4OTIgMTQuNDU5OCAxLjY3MDIgMTQuNzE1NiAxLjkxMDE2QzE0Ljk3NTggMi4xNTQzNiAxNS4wNzkzIDIuNDU5MDQgMTUuMTI0NyAyLjc3NjAzQzE1LjE2NjQgMy4wNjcyMyAxNS4xNjY0IDMuNDI5MzEgMTUuMTY2MyAzLjg0MjM5TDE1LjE2NjMgNC4zNjAwOEMxNS4xNjY0IDQuNjg1NzUgMTUuMTY2NCA0Ljk2NzMgMTUuMTQyIDUuMjAyMDdDMTUuMTE1OSA1LjQ1NDcxIDE1LjA1ODcgNS42OTE3MyAxNC45MjE0IDUuOTE5MjlDMTQuNzg1MSA2LjE0NTEzIDE0LjYwMTUgNi4zMDkwNyAxNC4zOSA2LjQ1NjAyQzE0LjE5MDcgNi41OTQ0NiAxMy45MzYgNi43Mzc4IDEzLjYzNjYgNi45MDYzN0wxMS42NzQ5IDguMDEwNjNDMTEuMjI4NCA4LjI2MTk5IDExLjA3MjkgOC4zNTI1IDEwLjk2OTEgOC40NDI2MkMxMC43MzA3IDguNjQ5NTggMTAuNTk0MiA4Ljg3OTIxIDEwLjUzMDEgOS4xNjY5MkMxMC41MDI2IDkuMjg5OTEgMTAuNDk5NyA5LjQ0NDgxIDEwLjQ5OTcgOS45MTUyOUwxMC40OTk3IDExLjczNjdDMTAuNDk5NyAxMi4zMzc1IDEwLjQ5OTcgMTIuODQ3NiAxMC40Mzc5IDEzLjIzOTdDMTAuMzcyMiAxMy42NTY3IDEwLjIxOTUgMTQuMDU2NiA5LjgxOTUyIDE0LjMwNjhDOS40Mjg2MiAxNC41NTEzIDguOTk4MDEgMTQuNTI4OSA4LjU3OTU0IDE0LjQyOTVDOC4xNzY1MSAxNC4zMzM4IDcuNjc5ODUgMTQuMTM5NiA3LjA4MzkxIDEzLjkwNjZMNy4wMjU5OSAxMy44ODRDNi43NDY4NCAxMy43NzQ5IDYuNTAyNDEgMTMuNjc5MyA2LjMwODkxIDEzLjU3OTRDNi4xMDA5NCAxMy40NzIgNS45MDc4MSAxMy4zMzg0IDUuNzYwMDQgMTMuMTMwNUM1LjYxMDYyIDEyLjkyMDIgNS41NTA3MSAxMi42OTQ3IDUuNTI0IDEyLjQ2NDJDNS40OTk2MyAxMi4yNTQgNS40OTk2NSAxMi4wMDE4IDUuNDk5NjggMTEuNzIwNEw1LjQ5OTY4IDkuOTE1MjlDNS40OTk2OCA5LjQ0NDgxIDUuNDk2NzEgOS4yODk5MSA1LjQ2OTI5IDkuMTY2OTJDNS40MDUxNCA4Ljg3OTIxIDUuMjY4NjIgOC42NDk1OCA1LjAzMDIzIDguNDQyNjJDNC45MjY0MyA4LjM1MjUgNC43NzA5NSA4LjI2MTk5IDQuMzI0NDMgOC4wMTA2M0wyLjM2MjgxIDYuOTA2MzdDMi4wNjMzMyA2LjczNzggMS44MDg3IDYuNTk0NDYgMS42MDk0IDYuNDU2MDJDMS4zOTc4NCA2LjMwOTA3IDEuMjE0MjggNi4xNDUxMyAxLjA3Nzk5IDUuOTE5MjlDMC45NDA2NjQgNS42OTE3MyAwLjg4MzQ3OSA1LjQ1NDcxIDAuODU3MzA2IDUuMjAyMDdDMC44MzI5ODQgNC45NjczIDAuODMyOTk2IDQuNjg1NzUgMC44MzMwMSA0LjM2MDA4TDAuODMzMDExIDMuODc2NDRDMC44MzMwMTEgMy44NjUwNSAwLjgzMzAxIDMuODUzNjkgMC44MzMwMSAzLjg0MjM4QzAuODMyOTc4IDMuNDI5MzEgMC44MzI5NTEgMy4wNjcyMyAwLjg3NDY3OSAyLjc3NjAzQzAuOTIwMTA0IDIuNDU5MDQgMS4wMjM1MSAyLjE1NDM2IDEuMjgzNzkgMS45MTAxNkMxLjUzOTU2IDEuNjcwMiAxLjg1MDk3IDEuNTc4OTIgMi4xNzMxNiAxLjUzODI4QzIuNDc2OTYgMS40OTk5NiAyLjg1NzA1IDEuNDk5OTggMy4zMDE2OCAxLjVaTTIuMjk4MzEgMi41MzA0MkMyLjA3NTg4IDIuNTU4NDggMi4wMDUxNCAyLjYwNDYxIDEuOTY4MDEgMi42Mzk0NEMxLjkzNTQgMi42NzAwNCAxLjg5MjM2IDIuNzIzOSAxLjg2NDU3IDIuOTE3ODlDMS44MzQyMSAzLjEyOTcgMS44MzMwMSAzLjQxOTEyIDEuODMzMDEgMy44NzY0NFY0LjMzNjMyQzEuODMzMDEgNC42OTI0NiAxLjgzMzYzIDQuOTIxODUgMS44NTE5OCA1LjA5OTAzQzEuODY5MDggNS4yNjQwNCAxLjg5ODQyIDUuMzQzMzggMS45MzQxNyA1LjQwMjYxQzEuOTcwOTQgNS40NjM1NSAyLjAzMjAxIDUuNTMyIDIuMTc5OSA1LjYzNDczQzIuMzM1NzEgNS43NDI5NiAyLjU0ODU4IDUuODYzMzkgMi44NzI5NiA2LjA0NTk5TDQuODE0OTggNy4xMzkyMkM0LjgzMzE2IDcuMTQ5NDUgNC44NTEwOCA3LjE1OTUzIDQuODY4NzIgNy4xNjk0NkM1LjI0MTE5IDcuMzc5IDUuNDk0ODYgNy41MjE3MiA1LjY4NTgxIDcuNjg3NDlDNi4wODAwNSA4LjAyOTc1IDYuMzMyOTkgOC40NDU0OCA2LjQ0NTMyIDguOTQ5M0M2LjQ5OTkyIDkuMTk0MTcgNi40OTk4MiA5LjQ2ODc0IDYuNDk5NjkgOS44NTYzMkM2LjQ5OTY4IDkuODc1NjkgNi40OTk2OCA5Ljg5NTM0IDYuNDk5NjggOS45MTUyOVYxMS42OTQ5QzYuNDk5NjggMTIuMDA5NyA2LjUwMDQ1IDEyLjIwMzMgNi41MTczNSAxMi4zNDkxQzYuNTMyNjIgMTIuNDgwOSA2LjU1NjY5IDEyLjUyNTIgNi41NzUyMSAxMi41NTEyQzYuNTk1NCAxMi41Nzk3IDYuNjM1MzkgMTIuNjIyNSA2Ljc2NzcgMTIuNjkwOUM2LjkwOTMxIDEyLjc2NCA3LjEwNDQgMTIuODQwOSA3LjQxMjQ5IDEyLjk2MTRDOC4wNTMxNiAxMy4yMTE4IDguNDg0MSAxMy4zNzkgOC44MTA2MiAxMy40NTY2QzkuMTI5NjkgMTMuNTMyMyA5LjIzNDgzIDEzLjQ5MyA5LjI4OTIzIDEzLjQ1OUM5LjMzNDUxIDEzLjQzMDcgOS40MDQ1MyAxMy4zNzMxIDkuNDUwMTIgMTMuMDg0QzkuNDk4MTkgMTIuNzc5MSA5LjQ5OTY4IDEyLjM0OSA5LjQ5OTY4IDExLjY5NDlWOS45MTUyOUM5LjQ5OTY4IDkuODk1MzUgOS40OTk2NyA5Ljg3NTcgOS40OTk2NiA5Ljg1NjMzQzkuNDk5NTMgOS40Njg3NSA5LjQ5OTQ0IDkuMTk0MTcgOS41NTQwMyA4Ljk0OTNDOS42NjYzNyA4LjQ0NTQ3IDkuOTE5MzEgOC4wMjk3NSAxMC4zMTM1IDcuNjg3NDlDMTAuNTA0NSA3LjUyMTcyIDEwLjc1ODIgNy4zNzkwMSAxMS4xMzA2IDcuMTY5NDdDMTEuMTQ4MyA3LjE1OTU0IDExLjE2NjIgNy4xNDk0NiAxMS4xODQ0IDcuMTM5MjFMMTMuMTI2NCA2LjA0NTk5QzEzLjQ1MDggNS44NjMzOSAxMy42NjM2IDUuNzQyOTYgMTMuODE5NSA1LjYzNDczQzEzLjk2NzMgNS41MzIgMTQuMDI4NCA1LjQ2MzU1IDE0LjA2NTIgNS40MDI2MUMxNC4xMDA5IDUuMzQzMzggMTQuMTMwMyA1LjI2NDA0IDE0LjE0NzQgNS4wOTkwM0MxNC4xNjU3IDQuOTIxODUgMTQuMTY2MyA0LjY5MjQ2IDE0LjE2NjMgNC4zMzYzMlYzLjg3NjQ0QzE0LjE2NjMgMy40MTkxMiAxNC4xNjUxIDMuMTI5NyAxNC4xMzQ4IDIuOTE3ODlDMTQuMTA3IDIuNzIzOSAxNC4wNjQgMi42NzAwNCAxNC4wMzEzIDIuNjM5NDRDMTMuOTk0MiAyLjYwNDYxIDEzLjkyMzUgMi41NTg0OCAxMy43MDEgMi41MzA0MkMxMy40NjczIDIuNTAwOTQgMTMuMTUxIDIuNSAxMi42NjYzIDIuNUgzLjMzMzAxQzIuODQ4MzIgMi41IDIuNTMyMDQgMi41MDA5NCAyLjI5ODMxIDIuNTMwNDJaIiBmaWxsPSIjNzk3ODc4Ii8+Cjwvc3ZnPgo="
                  alt=""
                />
                <span className="d_none d_inline_block_sm">Filters</span>
              </button>
              <div className="search_bar">
                <input
                  className="form_control"
                  type="text"
                  placeholder="Search applicants..."
                  value={filterObj.keyWord}
                  onChange={e=>{setFilterObj({...filterObj,keyWord:e.target.value.trim()})}}
                />
                <button className="btn btn_icon" onClick={filterHandl}>
                  <img
                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzQ0MDhfMjUyNykiPgo8cGF0aCBkPSJNMTUuNTAwNiAxNC4wMDA2SDE0LjcxMDZMMTQuNDMwNiAxMy43MzA2QzE1LjYzMDYgMTIuMzMwNiAxNi4yNTA2IDEwLjQyMDYgMTUuOTEwNiA4LjM5MDYzQzE1LjQ0MDYgNS42MTA2MyAxMy4xMjA2IDMuMzkwNjMgMTAuMzIwNiAzLjA1MDYzQzYuMDkwNjMgMi41MzA2MyAyLjUzMDYzIDYuMDkwNjMgMy4wNTA2MyAxMC4zMjA2QzMuMzkwNjMgMTMuMTIwNiA1LjYxMDYzIDE1LjQ0MDYgOC4zOTA2MyAxNS45MTA2QzEwLjQyMDYgMTYuMjUwNiAxMi4zMzA2IDE1LjYzMDYgMTMuNzMwNiAxNC40MzA2TDE0LjAwMDYgMTQuNzEwNlYxNS41MDA2TDE4LjI1MDYgMTkuNzUwNkMxOC42NjA2IDIwLjE2MDYgMTkuMzMwNiAyMC4xNjA2IDE5Ljc0MDYgMTkuNzUwNkMyMC4xNTA2IDE5LjM0MDYgMjAuMTUwNiAxOC42NzA2IDE5Ljc0MDYgMTguMjYwNkwxNS41MDA2IDE0LjAwMDZaTTkuNTAwNjMgMTQuMDAwNkM3LjAxMDYzIDE0LjAwMDYgNS4wMDA2MyAxMS45OTA2IDUuMDAwNjMgOS41MDA2M0M1LjAwMDYzIDcuMDEwNjMgNy4wMTA2MyA1LjAwMDYzIDkuNTAwNjMgNS4wMDA2M0MxMS45OTA2IDUuMDAwNjMgMTQuMDAwNiA3LjAxMDYzIDE0LjAwMDYgOS41MDA2M0MxNC4wMDA2IDExLjk5MDYgMTEuOTkwNiAxNC4wMDA2IDkuNTAwNjMgMTQuMDAwNloiIGZpbGw9IiM4RThFOEUiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMF80NDA4XzI1MjciPgo8cmVjdCB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="
                    alt=""
                  />
                </button>
              </div>
              {/* <button className="btn btn_primary add_new ms_auto" onClick={() => navigate('/account/new-form?source=1')}>
                + New Application
              </button> */}
            </div>
            <ul className="row mt_5">
              {
                dataList.map((v,i)=>{
                  return(
                    <li className="col" key={i}>
                    <div className="card">
                      <div className="card_title">
                        <div className="text">
                          <span>{v.firstName}&nbsp;{v.lastName}&nbsp;</span>
                          {/* <span className="color_warning suffix">(Initieated)</span> */}
                        </div>
                        {/* <button className="btn copy ms_auto" /> */}
                        {/* <button className="btn edit" /> */}
                      </div>
                      <div className="card_content d_flex_sm justify_between my_2_md">
                        <table className="table">
                          <tbody className="">
                            <tr>
                              <td className="color_gray fs_sm">phone number:</td>
                              <td>{'+'+v.homePhone||''}</td>
                            </tr>
                            <tr>
                              <td className="color_gray fs_sm">Email:</td>
                              <td>{v.email||""}</td>
                            </tr>
                            <tr>
                              <td className="color_gray fs_sm">Date:</td>
                              <td>{v.createDate||""}</td>
                            </tr>
                          </tbody>
                        </table>
                        <div className="lh dash w_100 d_none_sm" />
                        <div className="lv dash d_none d_block_sm" />
                        <table className="table">
                          <tbody className="">
                            <tr>
                              <td className="color_gray fs_sm">Amount:</td>
                              <td className="fw_bold">${v.amount||0}</td>
                            </tr>
                            <tr>
                              <td className="color_gray fs_sm">Treatment type:</td>
                              <td className="fw_bold">{v.treatmentType||""}</td>
                            </tr>
                            <tr>
                              <td className="color_gray fs_sm">
                                preferred monthly payment:
                              </td>
                              <td className="">${v.preferredMonthlyPayment||0}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </li>
                  )
                })
              }
             
           
            </ul>
          </div>
        </div>
      </div>

      <div className={`offcanvas ${showFilter?'show':""}`}>
        <div className="offcanvas_box">
          <div className="offcanvas_header">
            <span className="title">Filters</span>
            <button className="btn_close ms_auto" onClick={closeFifter} />
          </div>
          <div className="offcanvas_content filter_content">
            <div className="mt_5 mt_7_sm mt_4_md">
              <div className="filter_title">Sort by</div>
              <div className="sort_list my_3">
                {
                  sortList.map((v,i)=>{
                    return (
                      <div onClick={()=>changeSortBy(v)} key={i} className={`item  ${filterObj.sortBy==v?'active':''}`}>{v}</div>
                    )
                  })
                }
                
              </div>
            </div>
            <div className="lh" />
            <div className="mt_5 mt_4_md">
              <div className="filter_title">Status</div>
              <div className="sort_list my_3">
                {
                  stateList.map((v,i)=>{
                    return (
                      <div  onClick={()=>changeState(v)} key={i} className={`item ${filterObj.status==v?'active':''}`}>{v}</div>

                    )
                  })
                }
                 
              </div>
            </div>
            <div className="filter_container">
              <div className="mt_5 mt_4_md">
                <div className="filter_title mb_2">Amount</div>
                <div className="row">
                  <div className="col col_2_sm col_2_md">
                    <input type="text" className="js-range-slider" name="my_range" defaultValue={0} />
                  </div>
                  <div className="col col_2_sm col_2_md" style={{paddingRight:"32px"}}>
                  
                  {/* <Slider
                      range
                      min={0}
                      max={20000}
                      marks={marks}
                      defaultValue={[0, 20000]}
                      tipFormatter={(value) => `${value==0?value:'$ '+value}`}
                      onChange={(value)=>changeRange(value)}
                    /> */}
                  </div>
                </div>
                <div className="my_3" />
              </div>
              <div className="mt_5">
                <label className="form_label" htmlFor="">
                  Treatment type
                </label>
                <select value={filterObj.treatmentTypeCode} className="form_control" onChange={changeSelect}>
                  { 
                    treatmentTypeList.map((v,i)=>{
                      return ( <option className="option" value={v.key} key={i}>{v.value}</option>
                     ) })
                  } 
                  </select>
               
                
              </div>
              <div className="mt_5 mt_4_md">
                <div className="filter_title">Locations</div>
                <div className="search_bar mt_3">
                  <input
                    className="form_control"
                    type="text"
                    placeholder="Search applicants..."
                  />
                  <button className="btn btn_icon">
                    <img
                      src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzQ0MDhfMjUyNykiPgo8cGF0aCBkPSJNMTUuNTAwNiAxNC4wMDA2SDE0LjcxMDZMMTQuNDMwNiAxMy43MzA2QzE1LjYzMDYgMTIuMzMwNiAxNi4yNTA2IDEwLjQyMDYgMTUuOTEwNiA4LjM5MDYzQzE1LjQ0MDYgNS42MTA2MyAxMy4xMjA2IDMuMzkwNjMgMTAuMzIwNiAzLjA1MDYzQzYuMDkwNjMgMi41MzA2MyAyLjUzMDYzIDYuMDkwNjMgMy4wNTA2MyAxMC4zMjA2QzMuMzkwNjMgMTMuMTIwNiA1LjYxMDYzIDE1LjQ0MDYgOC4zOTA2MyAxNS45MTA2QzEwLjQyMDYgMTYuMjUwNiAxMi4zMzA2IDE1LjYzMDYgMTMuNzMwNiAxNC40MzA2TDE0LjAwMDYgMTQuNzEwNlYxNS41MDA2TDE4LjI1MDYgMTkuNzUwNkMxOC42NjA2IDIwLjE2MDYgMTkuMzMwNiAyMC4xNjA2IDE5Ljc0MDYgMTkuNzUwNkMyMC4xNTA2IDE5LjM0MDYgMjAuMTUwNiAxOC42NzA2IDE5Ljc0MDYgMTguMjYwNkwxNS41MDA2IDE0LjAwMDZaTTkuNTAwNjMgMTQuMDAwNkM3LjAxMDYzIDE0LjAwMDYgNS4wMDA2MyAxMS45OTA2IDUuMDAwNjMgOS41MDA2M0M1LjAwMDYzIDcuMDEwNjMgNy4wMTA2MyA1LjAwMDYzIDkuNTAwNjMgNS4wMDA2M0MxMS45OTA2IDUuMDAwNjMgMTQuMDAwNiA3LjAxMDYzIDE0LjAwMDYgOS41MDA2M0MxNC4wMDA2IDExLjk5MDYgMTEuOTkwNiAxNC4wMDA2IDkuNTAwNjMgMTQuMDAwNloiIGZpbGw9IiM4RThFOEUiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMF80NDA4XzI1MjciPgo8cmVjdCB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="
                      alt=""
                    />
                  </button>
                </div>
                <ul className="">
                  <li className="my_3 my_5_sm d_flex">
                    <input className="form_checkbox" type="checkbox" />
                    <label className="checkbox_label mt_1 ms_2">
                      <div className="color_primary700">
                        <span className="fw_medium">legal name</span> (owner name)
                      </div>
                      <div className="mt_2 fs_sm">
                        4000 La Rica Ave Suite D Baldwin Park, CA 91706
                      </div>
                    </label>
                  </li>
                  <li className="my_3 my_5_sm d_flex">
                    <input className="form_checkbox" type="checkbox" />
                    <label className="checkbox_label mt_1 fs_sm ms_2">
                      <div className="color_primary700">
                        <span className="fw_medium">legal name</span> (owner name)
                      </div>
                      <div className="mt_2 fs_sm">
                        4000 La Rica Ave Suite D Baldwin Park, CA 91706
                      </div>
                    </label>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="offcanvas_footer filter_footer">
            <div className="btn_group">
              <button className="btn btn_primary_line_transparent" onClick={closeFifter}>Cancel</button>
              <button className="btn btn_primary ms_auto col" onClick={addFifter} >Apply</button>
            </div>
          </div>
        </div>
      </div>


    </>


  );
};

Leads.propTypes = {};

export default Leads;
