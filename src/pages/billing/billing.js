import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { checkIsEmail } from "../../utils/check.js";
import TitleBar from "../../components/header/titleBar.js";
import $ from "jquery"
// import { FormattedMessage,useIntl} from "react-intl";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";
// import PropTypes from "prop-types";
// import loginStyle from "../../css/static.css";
// import { useSnackbar } from 'notistack';
import { setUserInfo } from "../../utils/storage.js"
// import OutlinedInput from '@mui/material/OutlinedInput';

const Billing = (props) => {
  const navigate = useNavigate();
  const searchValue = useLocation();
  console.log(searchValue);

  if (searchValue != null) {
    //fifter param
    //Refresh load list
  }
  const closePopup = () => {
    $(".popup").removeClass("show");

  }
  const openPopup = () => {
    $(".popup").addClass("show");
  }
  const openFifter = () => {

    $(".offcanvas").addClass("show");

  };


  const delLocation = () => {
    $(".popup").removeClass("show");

  }
  return (
    <>
      <div className="layout">
        <header>
          <div className="nav container">
            <button className="btn_return me_3" onClick={() => navigate("/account/home")} />
            <div className="title">Billing</div>
          </div>
        </header>
        <div className="main">
          <div className="container">
            <ul className="row mt_1">
              <li className="col col_3_sm">
                <div className="card h_100">
                  <div className="card_title">
                    <div className="text">
                      <span>5/11/23</span>
                    </div>
                    <button className="btn down ms_auto" />
                    {/* <button className="btn delete" /> */}
                  </div>
                  <div className="card_content d_flex_sm justify_between my_2_md">
                    <table className="table">
                      <tbody className="">
                        <tr>
                          <td className="color_gray fs_sm">Number of Leads:</td>
                          <td>300</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm">Amount Due:</td>
                          <td>$300</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> Due Date:</td>
                          <td>9/20/23</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> Payment Status:</td>
                          <td className="color_success">Paid</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </li>
              <li className="col col_3_sm">
                <div className="card h_100">
                  <div className="card_title">
                    <div className="text">
                      <span>5/11/23</span>
                    </div>
                    <button className="btn down ms_auto" />
                    {/* <button className="btn delete" /> */}
                  </div>
                  <div className="card_content d_flex_sm justify_between my_2_md">
                    <table className="table">
                      <tbody className="">
                        <tr>
                          <td className="color_gray fs_sm">Number of Leads:</td>
                          <td>300</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm">Amount Due:</td>
                          <td>$300</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> Due Date:</td>
                          <td>9/20/23</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> Payment Status:</td>
                          <td className="color_success">Paid</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </li>
              <li className="col col_3_sm">
                <div className="card h_100">
                  <div className="card_title">
                    <div className="text">
                      <span>5/11/23</span>
                    </div>
                    <button className="btn down ms_auto" />
                    {/* <button className="btn delete" /> */}
                  </div>
                  <div className="card_content d_flex_sm justify_between my_2_md">
                    <table className="table">
                      <tbody className="">
                        <tr>
                          <td className="color_gray fs_sm">Number of Leads:</td>
                          <td>300</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm">Amount Due:</td>
                          <td>$300</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> Due Date:</td>
                          <td>9/20/23</td>
                        </tr>
                        <tr>
                          <td className="color_gray fs_sm"> Payment Status:</td>
                          <td className="color_success">Paid</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="popup">
        <div className="popup_container">
          <div className="popup_box">
            <button className="btn btn_close" onClick={closePopup}></button>
            <div className="popup_content">
              <div>Please enter the TaxID of this location to confirm:</div>
              <input className="form_control mt_5 mt_6_md" type="text" placeholder="Tax ID"></input>
              <div className="btn_group mt_5 mt_6_md">
                <button className="btn btn_ok btn_primary_line" onClick={closePopup}>Cancel</button>
                <button className="btn btn_ok btn_primary ms_auto" onClick={delLocation}>Confirm</button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </>


  );
};

Billing.propTypes = {};

export default Billing;
