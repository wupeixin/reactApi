import React, { useState } from "react";
import { useNavigate, useParams,useSearchParams  } from "react-router-dom";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";
import ApplicationsForm from "../../components/applicationsForm/applicationsForm.js";
import LocationForm from "../../components/locationForm/locationForm.js";
import UserManagementForm from "../../components/userManagementForm/userManagementForm.js";
import UserManagementFormUpdata from "../../components/userManagementForm/userManagementFormUpdata.js";


const NewForm = (props) => {
  const navigate = useNavigate();
  // const [step,setStep] =useState();
  const [searchParams] = useSearchParams();

  const uid = parseInt(searchParams.get('uid'))
  const type = parseInt(searchParams.get('typeId'))
  const locationId = parseInt(searchParams.get('lId'))

  const renderForm=(arg)=>{
    switch (arg) {

      case 1:
        return <ApplicationsForm  />
      case 2:
        return <LocationForm  lid={locationId}/>

      case 3:
        return <UserManagementForm  />
      case 4:
        return <UserManagementFormUpdata uid={uid} />
      default:
        return null
    }
  }
  return (<>
        {renderForm(type)}
  </>);
};

NewForm.propTypes = {};

export default NewForm;
