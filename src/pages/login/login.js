import React, { useState, useRef } from "react";
import { useNavigate } from "react-router-dom";
import {commonContextActions,useCommonContext} from '../../common-context'
import { ToastContainer } from 'react-toastify';
import { checkIsEmail, ToastInfo, ToastError, } from "../../utils/check.js";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";

import { setItem, setUserInfo ,setToken} from "../../utils/storage.js"
import logo from "../../assets/logo-1.svg"

const Login = (props) => {
  const navigate = useNavigate();
  const toastId = useRef(null);
  const [loginInfo, setLoginInfo] = useState({ email: "", password: "" });

  const [error, setError] = useState({
    emailError: false,
    passwordError: false
  })
  const [{ user, config }, dispatchCommonContext] = useCommonContext();

  const signHandle = () => {



    if (!loginInfo.email) {
      setError({ emailError: true, passwordError: false })

    }
    if (!loginInfo.password) {
      setError({ emailError: false, passwordError: true })

    }
    if (!loginInfo.email && !loginInfo.password) {
      setError({ emailError: true, passwordError: true })
      return
    }
    if (!checkIsEmail(loginInfo.email)) {
      setError({ emailError: true, passwordError: false })
      return false
    }
    if (loginInfo.email.trim() && loginInfo.password.trim()) {
      setError({ emailError: false, passwordError: false })
      loginHandle()
    }
  }
  const loginHandle = async () => {

    setError({

      emailError: false,

      passwordError: false

    })

    const url = "Account/Login"
    try {

      const res = await sendPostByJson(url, { contactText: loginInfo.email, password: loginInfo.password })
      if (res) {
        // ToastInfo('Load success', toastId)
        user.loadingStatus=1
        dispatchCommonContext(
          {
            type: commonContextActions.updateUser,
            payload: user,
          }
        )
          localStorage.setItem("isLogin",1)
        // ToastInfo('Load success',toastId)
        setToken(res.data.token)
        setUserInfo(res.data.membershipUser)
              // let roleName;
              // switch(role) 
              //  {case 1: roleName="SuperAdministrator";
              //   break;
              //   case 2: roleName="Admin";
              //   break;
              //   case 3: roleName="Sales";
              //   break;
              //   case 4: roleName="Support";
              //   break;
              //   case 100: roleName="Administrator";
              //   break;
              //   case 101: roleName="User";
              //   break;
              //   case 200: roleName="Administrator";
              //   break;
              //   case 201: roleName="User";
              //   break;
              //   case 300: roleName="User";
              //   break;
              //   default:
              //   roleName="none";
              // } 

              setItem("role",res.data.membershipUser.userType)

              let role=0;
              let group = res.data.membershipUser.userTypeId;
              //1-DentiRate  2-Healthcare Group  3-Lender 4-Regular
              if (group === 1) {
                navigate('/account/home', { state: { source: 1 } })

                setItem("source",1)

              } else if (group === 2) {
                // if (role === "User") {
                  navigate('/account/applications', { state: { source: 101 } })
                  setItem("source",101)

                // }
                //  else {
                //   navigate('/account/home', { state: { source: 100 } })

                // }

              } else if (group === 3) {
                if (role === 201) {
                  navigate('/account/home', { state: { source: 201 } })
                  setItem("source",201)


                } else {
                  navigate('/account/home', { state: { source: 200 } })
                  setItem("source",200)

                }

              } else if (group === 4) {
                navigate('/account/patient-home', { state: { source: 300 } })
                setItem("source",300)


              }
            


          


      } 
      else {
        ToastError('Account or password error', toastId)
      }
    } catch (error) {
      if(error.response.status===423){
        ToastError('The account has been locked', toastId)

      }else{
        ToastError('Account or password error', toastId)
      }
    }
  }

  return (<>

    <div className="container">
      <div className="login_container">
        <div className="logo mb_4">
        <img  style={{width:"128px",height:"30px"}}
            src={logo}
              alt=""
            />
        </div>
        <h5 className="t_center mb_8">Welcome to Dentirent</h5>
        <form className="form mb_4">
          <div className="mb_6">
            <label className="form_label" htmlFor="">
              User name
            </label>

            <input
              className="form_control"
              type="text"
              placeholder="Enter your user name"
              value={loginInfo.email}
              onChange={(e) => setLoginInfo({ ...loginInfo, email: e.target.value })}

            />
            <div className="color_error mb_1" style={{ visibility: error.emailError ? 'visible' : "hidden" }}>
              Please enter your correct email address!
            </div>
          </div>
          <div className="">
            <label className="form_label" htmlFor="">
              Password
            </label>
            <input
              className="form_control"
              type="password"
              placeholder="Enter your password"
              value={loginInfo.password}
              onChange={(e) => setLoginInfo({ ...loginInfo, password: e.target.value })}

            />
            <div className="color_error mb_1" style={{ visibility: error.passwordError ? 'visible' : "hidden" }}>
              Please enter correct password!

            </div>
          </div>
          <a className="link_primary fw_middle mt_3" href="" onClick={() => navigate("/account/recovery")}>
            Forgot password?
          </a>
        </form>
        <div className="service">
          <span>By continuing, you agree to our </span>
          <a className="link_primary fw_bold" href="">
            Terms of Service
          </a>
        </div>
        <button className="btn btn_primary w_100 mt_3" onClick={signHandle}>Login</button>
      </div>
      <ToastContainer limit={1} />
    </div>
  </>);
};

Login.propTypes = {};

export default Login;
