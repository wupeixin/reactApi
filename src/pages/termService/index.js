import React from "react"
import { useNavigate } from "react-router-dom"


const TermService=(props)=>{
    const navigate=useNavigate()
    return(
        <>
        <div className="container">
        <button className="btn login_return" onClick={() => navigate("/account/patient-start")}>
        <img
            src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTMwLjk3NjIgOC44NjExNEMzMS42MDUyIDkuNDAwMjggMzEuNjc4IDEwLjM0NzIgMzEuMTM4OSAxMC45NzYyTDE5Ljk3NTYgMjRMMzEuMTM4OSAzNy4wMjM4QzMxLjY3OCAzNy42NTI4IDMxLjYwNTIgMzguNTk5OCAzMC45NzYyIDM5LjEzODlDMzAuMzQ3MiAzOS42NzggMjkuNDAwMiAzOS42MDUyIDI4Ljg2MTEgMzguOTc2MkwxNi44NjExIDI0Ljk3NjJDMTYuMzc5NiAyNC40MTQ1IDE2LjM3OTYgMjMuNTg1NiAxNi44NjExIDIzLjAyMzhMMjguODYxMSA5LjAyMzg0QzI5LjQwMDIgOC4zOTQ4NSAzMC4zNDcyIDguMzIyMDEgMzAuOTc2MiA4Ljg2MTE0WiIgZmlsbD0iIzMzMzMzMyIvPgo8L3N2Zz4K"
            alt=""
        />
        
        </button>
        <header>
            
        </header>
        <div className="login_container">
            term  service
        </div>
       
      </div>
        </>
    )
}
export default TermService