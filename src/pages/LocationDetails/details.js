import React, { useEffect, useState } from "react";
import { useLocation, useNavigate,useSearchParams } from "react-router-dom";
import TitleBar from "../../components/header/titleBar.js";
import { getItem, setUserInfo } from "../../utils/storage.js"
import { sendPostByJson } from "../../utils/httpUtils.js";


const Details = (props) => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const [location, setLocation] = useState({

  })
  const [locationId,setLocationId]=useState("")
  

  //effect
  useEffect(() => {
    
  }, [])


  const getDetailInfo = async () => {
    const id = parseInt(searchParams.get('id'))
    setLocationId(id)
    const url = ""
    const data = {}
    const res = await sendPostByJson(url, data)
    if (res.data) {
      //setlocation
    }
  }
  const editPage=()=>{
      navigate(`/account/new-form?typeId=2&lId=${locationId}`)
  }
  return (
    <>
      <div className="layout">
      <header>
      <div className="nav container">
     
        <div className="title">legal name (owner name)</div>
        <div className="nav_right ">
        <button className=" btn btn_edit ms_auto" onClick={editPage}></button>
        <button className=" btn btn_close ms_auto" onClick={()=>navigate('/account/locations')}></button>
        </div>
      </div>
  

    </header>

        <div className="main">
          <div className="container d_flex justify_between d_flex_col_row" >
              <div className="content_left">
                  <div className="mb_2">
                    <div className="tbl_title title_fs">Location information</div>
                    <div className="table_dis">
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Corporate/legal name:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">name</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Associated Practice Name or Store Number:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text ">Associated</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Office/DBA Name:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Office/DBA Name</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Tax ID Number:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Tax ID Number</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Total number of employees:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">ownership</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Date Business Started:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">22</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Practice/Medical Specialty, or Business Type:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">2022/12/12</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Office Phone Number:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">2423453656t</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Office Website:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Office Website</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Fax Number:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Fax Number</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Practice Management Software:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">aa</div>
                        </div>
                    </div>
                  </div>

                  <div className="mb_2">
                    <div className="tbl_title title_fs">Physical Address</div>
                    <div className="table_dis">
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Address Line 1:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Address Line 1</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Address Line2:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Address Line 2</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">City:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">City</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">State:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">State</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">ZIP Code:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">ZIP Code</div>
                        </div>
                     
                    </div>
                  </div>
                  <div className="mb_2">
                    <div className="tbl_title title_fs">Mailing Address</div>
                    <div className="table_dis">
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Address Line 1:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Address Line 1</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Address Line2:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Address Line 2</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">City:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">City</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">State:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">State</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">ZIP Code:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">ZIP Code</div>
                        </div>
                     
                    </div>
                  </div>
              </div>
              <div className="content_right">
                 <div className="mb_2">
                    <div className="tbl_title title_fs">Owner</div>
                    <div className="table_dis">
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text ">First Name:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">First Name</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Last Name:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Last Name</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Medical/Business/State License Numbe:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Medical/Business</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Social Security Number:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Social Security Number</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Address Line 1:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Address Line 1</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Address Line 2:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Address Line 2</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">City:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">City</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">State:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">State</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">ZIP Code:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">ZIP Code</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Phone Number:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Phone Number</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Email:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">aa</div>
                        </div>
                    </div>
                  </div>

                  <div className="mb_2">
                    <div className="tbl_title title_fs">Managing Doctor</div>
                    <div className="table_dis">
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">First Name:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">First Name</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Last Name:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Last Name</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Medical License Number:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Medical/Business</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Medical License State:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Social Security Number</div>
                        </div>
                      
                    </div>
                  </div>
                  <div className="mb_2">
                    <div className="tbl_title title_fs">Contact</div>
                    <div className="table_dis">
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">First Name:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">First Name</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Last Name:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Last Name</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Email:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Phone Number</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Phone:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">aa</div>
                        </div>
                      
                    </div>
                  </div>
                  <div className="mb_2">
                    <div className="tbl_title title_fs">Bank information</div>
                    <div className="table_dis">
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Bank Name:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Bank Name</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Routing Number:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Routing Number</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Account Number:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Account Number</div>
                        </div>
                        <div className="table_row">
                              <div className="table_cell tbl_cl_text">Bank Letter or Void Check as an attachment:</div>
                              <div className="table_cell tbl_cell_text tbl_cr_text">Bank Letter</div>
                        </div>
                      
                    </div>
                  </div>
              </div>
            
          </div>
        </div>
      </div>




    </>


  );
};

Details.propTypes = {};

export default Details;
