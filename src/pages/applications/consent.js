import { useNavigate } from "react-router-dom"


const Consent=(props)=>{
    const navigate=useNavigate()

    const goAdd=()=>{
        navigate('/account/new-form?typeId=1')
    }
    return (
        <div className="layout">
        {/* <header>
          <div className="nav container">
            <button className="btn_return me_3" onClick={() => navigate("/account/home")} />
            <div className="title">Dashboard</div>
          </div>
        </header> */}
        <div className="main">
          <div className="container">
                <div className="consent_title mb_2">Patient consent</div>
                <div className="consent_text">
                um is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.um is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem 
                </div>
                <div className="consent_footer">
                  <button className="btn btn_primary  ms_auto consent_btn"  onClick={goAdd}>
                Consent
              </button> 
                </div>
            </div>
        </div>
        </div>
    )
}
export default Consent