import React ,{useState,useRef}from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer } from 'react-toastify';
import { checkIsEmail ,ToastError,ToastSucces} from "../../utils/check.js";
import { sendGet, sendPost,sendPostByJson } from "../../utils/httpUtils.js";
import logo from "../../assets/logo-1.svg"

const ForgotPassword = (props) => {
  const toastId = useRef(null);
  const navigate = useNavigate();
  const [email,setEmail]=useState("")
  const [emailError,setEmailError]=useState(false)
  const signHandle = () => {
   
    if(!email||(email&&!checkIsEmail(email))){
      setEmailError(true)
    }else{
      setEmailError(false)
      loginHandle()
    }

  }
  const loginHandle = async () => {
    try {
      
      const res= await sendPostByJson("Account/RecoveryInit",{contactText:email})
      if(res.data.code===200){
        // ToastSucces("The verification email has been sent, please check your email !",toastId)
        localStorage.setItem("email",email)
         setTimeout(() => {
           navigate("/account/recovery-change")
         }, 200);
          
       }else{
        ToastError("No email found or wrong email",toastId)
       }
    } catch (error) {
      ToastError("Email sending error, please try again later ! ",toastId)
    }
  }

  return (
    <>
      <div className="container">
      <button className="btn login_return" onClick={() => navigate("/account/login")}>
      <img
        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTMwLjk3NjIgOC44NjExNEMzMS42MDUyIDkuNDAwMjggMzEuNjc4IDEwLjM0NzIgMzEuMTM4OSAxMC45NzYyTDE5Ljk3NTYgMjRMMzEuMTM4OSAzNy4wMjM4QzMxLjY3OCAzNy42NTI4IDMxLjYwNTIgMzguNTk5OCAzMC45NzYyIDM5LjEzODlDMzAuMzQ3MiAzOS42NzggMjkuNDAwMiAzOS42MDUyIDI4Ljg2MTEgMzguOTc2MkwxNi44NjExIDI0Ljk3NjJDMTYuMzc5NiAyNC40MTQ1IDE2LjM3OTYgMjMuNTg1NiAxNi44NjExIDIzLjAyMzhMMjguODYxMSA5LjAyMzg0QzI5LjQwMDIgOC4zOTQ4NSAzMC4zNDcyIDguMzIyMDEgMzAuOTc2MiA4Ljg2MTE0WiIgZmlsbD0iIzMzMzMzMyIvPgo8L3N2Zz4K"
        alt=""
      />
    </button>
        <div className="login_container">
          <div className="logo mb_4">
            <img  style={{width:"128px",height:"30px"}}
            src={logo}
              alt=""
            />
          </div>
          <h5 className="t_center mb_8">Forget Password</h5>
          <form className="form mb_5">
            <div className="mb_6">
              <label className="form_label" htmlFor="">
                Email
              </label>
              <input
                className="form_control"
                type="text"
                placeholder="Enter your Email"
                value={email}
                onChange={(e) => setEmail( e.target.value)}
              />
              <div className="color_error mt_2" style={{visibility:emailError?'visible':"hidden"}}> Please enter your correct email address! </div>
            </div>

          </form>
          <button className="btn btn_primary w_100 mt_3" onClick={signHandle}>Countinue</button>
        </div>
        <ToastContainer limit={1}/>
      </div>
    </>

  );
};

ForgotPassword.propTypes = {};

export default ForgotPassword;
