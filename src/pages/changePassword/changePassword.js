import React, { useEffect, useState, useRef } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import PropTypes from "prop-types";

import { ToastContainer } from 'react-toastify';
import { ToastInfo, ToastError, ToastSucces, } from "../../utils/check.js";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";
import { getItem, getUserInfo, setUserInfo } from "../../utils/storage.js"

const ChangePassword = (props) => {

    const toastId = useRef(null);
    const navigate = useNavigate();
    const [oldErr, setOldErr] = useState(false)
    const [reEnterErr, setReEnterErr] = useState(false)
    const [newErr, setNewErr] = useState(false)
    const [error, setError] = useState({
        newFmtError: false,
        reNewFmtError: false
    })





    const [pwdInfo, setpwdInfo] = useState({ oldPwd: "", newPwd: "", reEnterPwd: "" });

    const inputOld = (e) => {
        let val = e.target.value
        setpwdInfo({ ...pwdInfo, oldPwd: val })
        if (!val) {
            setOldErr(true)
        } else {
            setOldErr(false)
        }

    }
    const inputNewPwd = (e) => {

        let val = e.target.value
        setpwdInfo({ ...pwdInfo, newPwd: val })
        if (!val) {
            setNewErr(true)
        } else {
            setNewErr(false)
        }
        let str = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_])[A-Za-z\d\W_]{8,30}$/
        let f = str.test(val)
        if (!f) {
            setError({ ...error, newFmtError: true })
        } else {
            setError({ ...error, newFmtError: false })
        }
    }

    const inputRePwd = (e) => {

        let val = e.target.value
        setpwdInfo({ ...pwdInfo, reEnterPwd: val })
        if (!val) {
            setReEnterErr(true)
        } else {
            setReEnterErr(false)
        }
        let str = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_])[A-Za-z\d\W_]{8,30}$/
        let f = str.test(val)
        if (!f) {
            setError({ ...error, reNewFmtError: true })
        } else {
            setError({ ...error, reNewFmtError: false })
        }
    }


    const confirm = () => {
        if (!pwdInfo.oldPwd.trim()) {
            ToastError("Please enter the original password!", toastId)
            return
        }
        if (!pwdInfo.newPwd.trim()) {
            ToastError("Please enter the new password!", toastId)
            return
        }
        if (!pwdInfo.reEnterPwd.trim()) {
            ToastError("Please Re-enter the new password!", toastId)
            return
        }
        if (pwdInfo.reEnterPwd != pwdInfo.newPwd) {
            ToastError("The new password entered twice does not match!", toastId)
            return
        }

        if (pwdInfo.newPwd.trim()== pwdInfo.oldPwd.trim()) {
            ToastError("The new password cannot be the same as the old password", toastId)
            return
        }

        saveChange()



    }
    const saveChange = async () => {
        try {
            let userInfoString = getUserInfo();
            const res = await sendPostByJson("Account/UpdatePassword",
                { oldPassword: pwdInfo.oldPwd.trim(), newPassword: pwdInfo.newPwd.trim(), userId: userInfoString.userId })
            if (res.data.code === 200) {
                ToastSucces("Update new password success,Please log in with new password! ",toastId)
            } else if (res.data.code === 412) {
                ToastError("The current password is incorrect.", toastId)

            } else if (res.data.code === 400) {
                ToastError("Password format error", toastId)
            }
        } catch (error) {
            let code = error.response.status
            if (code === 412) {
                ToastError('The current password is incorrect.', toastId)

            } else if (code === 400) {
                ToastError("Password format error", toastId)
            }
            else {
                ToastError("Update new Password error,pleas try again latter!", toastId)

            }
        }
    }
    return (
        <>

            <div className="layout">
                <header>
                    <div className="nav container">
                        <button className="btn_return me_3 " onClick={() => navigate("/account/user-profile")} />
                        <div className="title">Change password</div>
                    </div>
                </header>
                <div className="main">
                    <div className="container ">
                        <form className="form mb_6 max_center">
                            <div className="mb_4">
                                <label className="form_label" htmlFor="Currentpassword">
                                    Current password
                                </label>
                                <input
                                    id="Currentpassword"
                                    className="form_control"
                                    type="password"
                                    value={pwdInfo.oldPwd}
                                    onChange={inputOld}
                                    placeholder="Enter your current password"
                                />
                                <div className="color_error mb_1" style={{ visibility: oldErr ? 'visible' : "hidden" }}>
                                    Please enter the original password!
                                </div>
                            </div>
                            <div className="mb_4">
                                <label className="form_label" htmlFor="Newpassword">
                                    New password
                                </label>
                                <input
                                    id="password"
                                    className="form_control"
                                    type="password"
                                    value={pwdInfo.newPwd}
                                    onChange={inputNewPwd}
                                    placeholder="Enter your new password"
                                />
                                <div className="color_error mb_1" style={{ visibility: newErr || error.newFmtError ? 'visible' : "hidden" }}>

                                    {newErr ? ' Please enter the new  password!' : 'Please enter passwords between 8 and 30, including uppercase and lowercase letters, numbers, and special symbols'}
                                </div>
                            </div>
                            <div className="mb_4">
                                <label className="form_label" htmlFor="Repassword">
                                    Re-enter new password
                                </label>
                                <input
                                    id="Repassword"
                                    className="form_control"
                                    type="password"
                                    value={pwdInfo.reEnterPwd}
                                    onChange={inputRePwd}
                                    placeholder="Repeat your new password"
                                />
                                <div className="color_error mb_1" style={{ visibility: reEnterErr || error.reNewFmtError ? 'visible' : "hidden" }}>

                                    {reEnterErr ? ' Please Re-enter the new  password!' : 'Please enter passwords between 8 and 30, including uppercase and lowercase letters, numbers, and special symbols'}
                                </div>
                            </div>
                        </form>
                        <div className="pos_fixed">
                            <button className="btn btn_primary w_100 mt_3 " onClick={confirm}>Confirm</button>

                        </div>
                    </div>
                </div>
                <ToastContainer limit={1} />
            </div>
        </>

    );
};

ChangePassword.propTypes = {};

export default ChangePassword;
