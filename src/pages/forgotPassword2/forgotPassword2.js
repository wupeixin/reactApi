import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import logo from "../../assets/logo-1.svg"

const ForgotPassword2 = (props) => {
  const navigate = useNavigate();
  const [email,setEmail]=useState("")
  useEffect(()=>{
   let email=  localStorage.getItem("email")
   if(email){
    setEmail(email)
    localStorage.removeItem('email')
   }
  },[])
  return (
    <>
      <div className="container">
      <button className="btn login_return" onClick={() => navigate("/account/recovery")}>
      <img
        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTMwLjk3NjIgOC44NjExNEMzMS42MDUyIDkuNDAwMjggMzEuNjc4IDEwLjM0NzIgMzEuMTM4OSAxMC45NzYyTDE5Ljk3NTYgMjRMMzEuMTM4OSAzNy4wMjM4QzMxLjY3OCAzNy42NTI4IDMxLjYwNTIgMzguNTk5OCAzMC45NzYyIDM5LjEzODlDMzAuMzQ3MiAzOS42NzggMjkuNDAwMiAzOS42MDUyIDI4Ljg2MTEgMzguOTc2MkwxNi44NjExIDI0Ljk3NjJDMTYuMzc5NiAyNC40MTQ1IDE2LjM3OTYgMjMuNTg1NiAxNi44NjExIDIzLjAyMzhMMjguODYxMSA5LjAyMzg0QzI5LjQwMDIgOC4zOTQ4NSAzMC4zNDcyIDguMzIyMDEgMzAuOTc2MiA4Ljg2MTE0WiIgZmlsbD0iIzMzMzMzMyIvPgo8L3N2Zz4K"
        alt=""
      />
    </button>
        <div className="login_container">
          <div className="logo mb_4">
          <img  style={{width:"128px",height:"30px"}}
            src={logo}
              alt=""
            />
          </div>
          <h5 className="t_center mb_8">Forgot Password</h5>
          <div className="w_100" style={{ display: "none" }}>
            <form className="form mb_6">
              <label className="form_label" htmlFor="">
                Email
              </label>
              <input
                className="form_control"
                type="text"
                placeholder="Enter your Email"
              />
            </form>
            <button className="btn btn_primary w_100">Countinue</button>
          </div>
        </div>
        <div className="login_check_email">
          <h5 className="fw_regular mb_6">
            Please open the link that we sent to <span>{email}</span>
          </h5>
          <button className="btn btn_primary_line" onClick={() => navigate("/account/login")}>Back</button>
        </div>
      </div>
    </>


  );
};

ForgotPassword2.propTypes = {};

export default ForgotPassword2;
