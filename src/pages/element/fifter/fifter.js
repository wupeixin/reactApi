import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import PropTypes from "prop-types";
import $ from "jquery";
// import 'ion-rangeslider/css/ion.rangeSlider.min.css';
// import 'ion-rangeslider/js/ion.rangeSlider.min.js';

const Filter = (props,{d}) => {
  console.log(d);
  // const fifterOpen = props.open
  // if (fifterOpen) {
  //   $('.offcanvas').addClass('show');
  // }
  const navigate = useNavigate();
  $(".js-range-slider").ionRangeSlider({
    type: "double",
    min: 0,
    max: 20000,
    from: 200,
    to: 800,
    prefix: "$",
    // onStart: () => { $(".js-range-slider").find('.irs-handle').addClass("custom-handle"); }, onFinish: () => { $(".js-range-slider").find('.irs-handle').removeClass("custom-handle"); }
  });

  const [params, setParams] = useSearchParams()
  const source = parseInt(params.get('source'))
  const getBack = async () => {
    //1 Applications 2 Locations 3 UserManagement
    if (source === 1) {
      navigate("/account/applications")
    } else if (source === 2) {
      navigate("/account/locations")
    } else if (source === 3) {
      navigate("/account/user-management")
    }
  }

  const addFifter = async () => {
    //1 Applications 2 Locations 3 UserManagement
    if (source === 1) {
      navigate("/account/applications", { state: { sortBy: "Date", statuse: "All", treatmentType: "1 week" } })
    } else if (source === 2) {
      navigate("/account/locations", { state: { sortBy: "sort2", statuse: "All", treatmentType: "2 week" } })
    } else if (source === 3) {
      navigate("/account/user-management", { state: { sortBy: "sort3", statuse: "All", treatmentType: "3 week" } })
    }
  }

  const closeFifter = (e) => {
    if ($('.offcanvas').hasClass('show')) {
      $('.offcanvas').removeClass('show');
    }
  }
  return (
    <>
      <div className={"offcanvas show"}>
        <div className="offcanvas_box">
          <div className="offcanvas_header">
            <span className="title">Filters</span>
            <button className="btn_close ms_auto" onClick={closeFifter} />
          </div>
          <div className="offcanvas_content filter_content">
            <div className="mt_5 mt_7_sm mt_4_md">
              <div className="filter_title">Sort by</div>
              <div className="sort_list my_3">
                <div className="item active">Date</div>
                <div className="item">Sort 2</div>
                <div className="item">Sort 3</div>
              </div>
            </div>
            <div className="lh" />
            <div className="mt_5 mt_4_md">
              <div className="filter_title">Status</div>
              <div className="sort_list my_3">
                <div className="item active">All</div>
                <div className="item">Preapproved</div>
                <div className="item">Preapproved</div>
                <div className="item">Preapproved</div>
                <div className="item">Preapproved</div>
                <div className="item">Preapproved</div>
              </div>
            </div>
            {/* <div className="mt_5 mt_4_md">
              <div className="filter_title">Amount</div>
              <div className="sort_list my_3">
                <input type="range"></input>
              </div>
            </div> */}
            <div className="filter_container">
              <div className="mt_5 mt_4_md">
                <div className="filter_title">Amount</div>
                <input type="range" style={{width:"70%"}}></input>
                {/* <input type="text" className="js-range-slider" name="my_range"  defaultValue={100}/> */}
                <div className="my_3" />
              </div>
              <div className="mt_5">
                <label className="form_label" htmlFor="">
                  Treatment type
                </label>
                <div className="form_select">
                  <div className="form_control">
                    <span>1 week</span>
                  </div>
                  <div className="option_box">
                    <div className="scroll_bar">
                      <div className="option">option1</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt_5 mt_4_md">
                <div className="filter_title">Locations</div>
                <div className="search_bar mt_3">
                  <input
                    className="form_control"
                    type="text"
                    placeholder="Search applicants..."
                  />
                  <button className="btn btn_icon">
                    <img
                      src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzQ0MDhfMjUyNykiPgo8cGF0aCBkPSJNMTUuNTAwNiAxNC4wMDA2SDE0LjcxMDZMMTQuNDMwNiAxMy43MzA2QzE1LjYzMDYgMTIuMzMwNiAxNi4yNTA2IDEwLjQyMDYgMTUuOTEwNiA4LjM5MDYzQzE1LjQ0MDYgNS42MTA2MyAxMy4xMjA2IDMuMzkwNjMgMTAuMzIwNiAzLjA1MDYzQzYuMDkwNjMgMi41MzA2MyAyLjUzMDYzIDYuMDkwNjMgMy4wNTA2MyAxMC4zMjA2QzMuMzkwNjMgMTMuMTIwNiA1LjYxMDYzIDE1LjQ0MDYgOC4zOTA2MyAxNS45MTA2QzEwLjQyMDYgMTYuMjUwNiAxMi4zMzA2IDE1LjYzMDYgMTMuNzMwNiAxNC40MzA2TDE0LjAwMDYgMTQuNzEwNlYxNS41MDA2TDE4LjI1MDYgMTkuNzUwNkMxOC42NjA2IDIwLjE2MDYgMTkuMzMwNiAyMC4xNjA2IDE5Ljc0MDYgMTkuNzUwNkMyMC4xNTA2IDE5LjM0MDYgMjAuMTUwNiAxOC42NzA2IDE5Ljc0MDYgMTguMjYwNkwxNS41MDA2IDE0LjAwMDZaTTkuNTAwNjMgMTQuMDAwNkM3LjAxMDYzIDE0LjAwMDYgNS4wMDA2MyAxMS45OTA2IDUuMDAwNjMgOS41MDA2M0M1LjAwMDYzIDcuMDEwNjMgNy4wMTA2MyA1LjAwMDYzIDkuNTAwNjMgNS4wMDA2M0MxMS45OTA2IDUuMDAwNjMgMTQuMDAwNiA3LjAxMDYzIDE0LjAwMDYgOS41MDA2M0MxNC4wMDA2IDExLjk5MDYgMTEuOTkwNiAxNC4wMDA2IDkuNTAwNjMgMTQuMDAwNloiIGZpbGw9IiM4RThFOEUiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMF80NDA4XzI1MjciPgo8cmVjdCB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="
                      alt=""
                    />
                  </button>
                </div>
                <ul className="">
                  <li className="my_3 my_5_sm d_flex">
                    <input className="form_checkbox" type="checkbox" />
                    <label className="checkbox_label mt_1 ms_2">
                      <div className="color_primary700">
                        <span className="fw_medium">legal name</span> (owner name)
                      </div>
                      <div className="mt_2 fs_sm">
                        4000 La Rica Ave Suite D Baldwin Park, CA 91706
                      </div>
                    </label>
                  </li>
                  <li className="my_3 my_5_sm d_flex">
                    <input className="form_checkbox" type="checkbox" />
                    <label className="checkbox_label mt_1 fs_sm ms_2">
                      <div className="color_primary700">
                        <span className="fw_medium">legal name</span> (owner name)
                      </div>
                      <div className="mt_2 fs_sm">
                        4000 La Rica Ave Suite D Baldwin Park, CA 91706
                      </div>
                    </label>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="offcanvas_footer filter_footer">
            <div className="btn_group">
              <button className="btn btn_primary_line_transparent" onClick={getBack}>Cancel</button>
              <button className="btn btn_primary ms_auto col" onClick={addFifter} >Apply</button>
            </div>
          </div>
        </div>
      </div>
    </>


  );
};

Filter.propTypes = {};

export default Filter;
