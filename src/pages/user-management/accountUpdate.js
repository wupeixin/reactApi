import React, { useState,useRef, useEffect } from "react";
import { useNavigate, useSearchParams,useLocation } from "react-router-dom";
import { ToastContainer,toast } from 'react-toastify';
import { checkIsEmail, ToastInfo, ToastError, ToastSucces, } from "../../utils/check.js";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";
import { getUserInfo } from "../../utils/storage.js";

import showPng from '../../assets/showpwd.png'
import hiedPng from '../../assets/hidepwd.png'
const AccountInfoUpdate = (props) => {
    const toastId = useRef(null);
    const navigate = useNavigate();
    const [showToast,setShowToast]=useState(false)
    const [showPwd,setShowPwd]=useState(false)
    const [user,setUser]=useState({
        "isActive":true,
        "firstName": "",
        "lastName": "",
        "email": "",
        "phoneNumber": "",
        "Password": "",
        "invitationCode": "",
        "organizationId": 0,
        "roleId":0,
        "province": "",
        "city": "",
        "address1": "",
        "address2": "",
        "zipCode": "",
        "id":'',
        "roleId":""
    })
  const [lsStr,setLsStr]=useState("")
  const [locationList,setLocationList]=useState([])
    //get userinfo from url and user change to save

    // const [step,setStep] =useState();

    useEffect(()=>{
        const {invitationCode}=getURLParams()
        if(invitationCode){
            getUserByCode(invitationCode)
        }
    },[])

    const getURLParams = () => {
 
        const searchParams = new URLSearchParams(window.location.search);
        const params = {};
      
        for (let param of searchParams.entries()) {
          params[param[0]] = param[1];
        }
      
        return params;
      };

      //get userInfo by code 
      const getUserByCode=async(code)=>{
         const url="/Account/InvitiationCode"
         const data={
            encryptedCode:code
         }
         try {
            const res= await sendPostByJson(url,data)
            if(res.data){
                  setUser(res.data.linkEmail) 
            }   
         } catch (error) {
            ToastError('Account information error, Please contact the administrator for processing ', toastId)
         }
       
      }

      const openPwd=()=>{
        setShowPwd(!showPwd)
      }
    const nextStep = (e) => {
       
            e.preventDefault();
          
          
           
       
        if(!user.firstName){
        
               ToastError('Please enter firstName', toastId)
               return false
        }
        if(!user.lastName){
            return   ToastError('Please enter lastName', toastId)
        }
        if(!user.email){
            return   ToastError('Please enter email', toastId)
        }
      
        
       
        if(user.phoneNumber.length!==10){
            return   ToastError('Please enter a 10 digit phone number', toastId)
        }
        // if(!user.password){
        //     return   ToastError('Please enter password', toastId)
        // }
        let str=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_])[A-Za-z\d\W_]{8,30}$/
        if(!str.test(user.password)){
            let errStr='Please enter passwords between 8 and 30, including uppercase and lowercase letters, numbers, and special symbols'
            return   ToastError(errStr, toastId)
        }
        saveDate()
    }
    const closePopup = () => {
        // $(".popup").removeClass("show");
        setShowToast(false)
    }
    const openPopup = () => {
        // $(".popup").addClass("show");
        setShowToast(true)
    }
    const saveDate=async()=>{
        const url="/Account/UpdateUserBio"
        try {
            let data={
                "firstName": user.firstName,
                "id": user.id,
                "lastName": user.lastName,
                "phoneNumber": user.phoneNumber,
                "Password":user.password,
                "province": user.province,
                "city": user.city,
                "address1": user.address1,
                "address2": user.address2,
                "zipCode": user.zipCode,
                "organizationId": user.roleId,
                "roleId":user.roleId,
            }
            const res= await sendPostByJson(url,data)
            if (res.data){
              
                ToastSucces("Account information updated successfully, please log in ",toastId)
                setTimeout(() => {
                    navigate("/")
                }, 2000);
              
            }
            // else if(res.code==409 ){
            //     ToastError("The email has already been used, please switch to another one!",toastId)
            // }else if(res.code==400){
            //     ToastError("Phone number not verified!",toastId)
            // }

        } catch (error) {
            ToastError("Update  user information failed,please try again latter!",toastId)    
        }

    }
 
    const filterLocation =(e)=>{
        e.preventDefault();
        console.log(lsStr);
    }

    const changeAct =()=>{
        let f=user.isActive
        setUser({...user,isActive:!f})
    }
    const goback=(arg)=>{
        if(arg==1){
            setShowToast(false)
            navigate("/")
        }else{
          //stay page and save data
          setShowToast(false)
        }
    }
    return (<>
      <ToastContainer limit={1} />
        <div className="layout">
            <header>
                <div className="nav container">
                    <button className="btn_return me_3" onClick={openPopup} />
                    <div className="title">User management</div>
                </div>
                {/* <ButtonModal/> */}

            </header>

            <div className="container" >
            {/* className=" mb_4" style={{ marginTop: "5vh",display:'flex' }} */}
                <div >
                {/* <label className="form_label" htmlFor=""  style={{ marginRight: "20px",fontWeight:'600',}}>
                                Active
                 </label>
                <div className={`form_switch ${user.isActive?'active':''}`} onClick={changeAct}  style={{cursor:"pointer"}}></div> */}
                  
                </div>
                <form className="form mb_4 mt_4" id="formOne">
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                First name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Enter first name"
                                value={user.firstName}
                                onChange={(e)=>{setUser({...user,firstName:e.target.value.trim()})}}
                            />
                        </div>
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Last name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Enter last name"
                                value={user.lastName}
                                onChange={(e)=>{setUser({...user,lastName:e.target.value.trim()})}}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Email
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                disabled
                                placeholder="Enter email address"
                                value={user.email}
                                onChange={(e)=>{setUser({...user,email:e.target.value.trim()})}}
                            />
                        </div>
                    </div>
                   
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                             Phone Number
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Enter phone number"
                                value={user.phoneNumber}
                                onChange={(e)=>{setUser({...user,phoneNumber:e.target.value.trim()})}}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md" style={{position:"relative"}}>
                            <label className="form_label" htmlFor="">
                              Password
                            </label>

                            <input
                                className="form_control"
                                type={showPwd?"text":'password'}
                                placeholder="Enter password"
                                value={user.password}
                                onChange={(e)=>{setUser({...user,password:e.target.value.trim()})}}
                            />
                              <div className="eye" onClick={openPwd} style={{position:"absolute",right:"28px",top:'38px',zIndex:'0'}}>
                                    <img src={showPwd?showPng:hiedPng} style={{width:'18px',height:'16px'}}></img>
                              </div>
                          
                        </div>
                    </div>
                    {/* <p className="mb_2">Locations</p> */}
                    <div className="row">
                        <div className="col col_2_sm col_2_md">
                            <div className="search_bar">
                                {/* <input
                                    className="form_control"
                                    type="text"
                                    value={lsStr}
                                    onChange={(e)=>{setLsStr(e.target.value.trim())}}
                                    placeholder="Search location..."
                                />
                                <button className="btn btn_icon" onClick={filterLocation}>
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzQ0MDhfMjUyNykiPgo8cGF0aCBkPSJNMTUuNTAwNiAxNC4wMDA2SDE0LjcxMDZMMTQuNDMwNiAxMy43MzA2QzE1LjYzMDYgMTIuMzMwNiAxNi4yNTA2IDEwLjQyMDYgMTUuOTEwNiA4LjM5MDYzQzE1LjQ0MDYgNS42MTA2MyAxMy4xMjA2IDMuMzkwNjMgMTAuMzIwNiAzLjA1MDYzQzYuMDkwNjMgMi41MzA2MyAyLjUzMDYzIDYuMDkwNjMgMy4wNTA2MyAxMC4zMjA2QzMuMzkwNjMgMTMuMTIwNiA1LjYxMDYzIDE1LjQ0MDYgOC4zOTA2MyAxNS45MTA2QzEwLjQyMDYgMTYuMjUwNiAxMi4zMzA2IDE1LjYzMDYgMTMuNzMwNiAxNC40MzA2TDE0LjAwMDYgMTQuNzEwNlYxNS41MDA2TDE4LjI1MDYgMTkuNzUwNkMxOC42NjA2IDIwLjE2MDYgMTkuMzMwNiAyMC4xNjA2IDE5Ljc0MDYgMTkuNzUwNkMyMC4xNTA2IDE5LjM0MDYgMjAuMTUwNiAxOC42NzA2IDE5Ljc0MDYgMTguMjYwNkwxNS41MDA2IDE0LjAwMDZaTTkuNTAwNjMgMTQuMDAwNkM3LjAxMDYzIDE0LjAwMDYgNS4wMDA2MyAxMS45OTA2IDUuMDAwNjMgOS41MDA2M0M1LjAwMDYzIDcuMDEwNjMgNy4wMTA2MyA1LjAwMDYzIDkuNTAwNjMgNS4wMDA2M0MxMS45OTA2IDUuMDAwNjMgMTQuMDAwNiA3LjAxMDYzIDE0LjAwMDYgOS41MDA2M0MxNC4wMDA2IDExLjk5MDYgMTEuOTkwNiAxNC4wMDA2IDkuNTAwNjMgMTQuMDAwNloiIGZpbGw9IiM4RThFOEUiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMF80NDA4XzI1MjciPgo8cmVjdCB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="
                                        alt=""
                                    />
                                </button> */}
                                {/* <ul className="">
                                    <li className="my_3 my_5_sm d_flex">
                                        <input className="form_checkbox" type="checkbox" />
                                        <label className="checkbox_label mt_1 ms_2">
                                            <div className="color_primary700">
                                                <span className="fw_medium">legal name</span> (owner name)
                                            </div>
                                            <div className="mt_2 fs_sm">
                                                4000 La Rica Ave Suite D Baldwin Park, CA 91706
                                            </div>
                                        </label>
                                    </li>
                                    <li className="my_3 my_5_sm d_flex">
                                        <input className="form_checkbox" type="checkbox" />
                                        <label className="checkbox_label mt_1 fs_sm ms_2">
                                            <div className="color_primary700">
                                                <span className="fw_medium">legal name</span> (owner name)
                                            </div>
                                            <div className="mt_2 fs_sm">
                                                4000 La Rica Ave Suite D Baldwin Park, CA 91706
                                            </div>
                                        </label>
                                    </li>
                                </ul> */}
                            </div>

                        </div>


                    </div>
                    <div className="offcanvas_footer filter_footer">
                        <div className="btn_group">
                            {/* <button className="btn btn_primary_line_transparent" onClick={getBack}>Cancel</button> */}
                            <button className="btn btn_primary ms_auto col" onClick={nextStep} >Create User</button>
                        </div>
                    </div>
                </form>


                {/* <button className="btn btn_primary w_100 mt_3" onClick={() => nextStep()}>Create User</button> */}
            </div>
        </div>

        <div className={`popup ${showToast?'show':''}`}>
            <div className="popup_container">
                <div className="popup_box">
                    <button className="btn btn_close" onClick={closePopup}></button>
                    <div className="popup_content">
                        <div>do you want to save informations?</div>
                        {/* <input className="form_control mt_5 mt_6_md" type="text" placeholder="Tax ID"></input> */}
                        <div className="btn_group mt_5 mt_6_md">
                            <button className="btn btn_ok btn_primary_line" onClick={()=>goback(1)}>No</button>
                            <button className="btn btn_ok btn_primary ms_auto" onClick={() =>goback(2)}>Yes</button>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>

    </>);
};

AccountInfoUpdate.propTypes = {};

export default AccountInfoUpdate;
