import React, { useEffect, useState ,useRef} from "react";
import { useLocation, useNavigate } from "react-router-dom";
import TitleBar from "../../components/header/titleBar.js";

import { ToastContainer,toast } from 'react-toastify';
import { checkIsEmail, ToastInfo, ToastError, ToastSucces, } from "../../utils/check.js";

import { sendGet, sendPost, sendPostByJson,sendGetKeyValue } from "../../utils/httpUtils.js";

import { setUserInfo,getUserInfo } from "../../utils/storage.js"

const ActiveStateList=[
  {key:null,value:"All"},
  {key:true,value:"Active"},
  {key:false,value:"Inactive"},
]

const UserManagement = (props) => {
  const toastId = useRef(null);
  const navigate = useNavigate();

  const [showFilter, setShowFilter] = useState(false)
  const [searchObj,setSearchObj]=useState({
    keyword:"",
    state:null,
    location:"",
    locationStr:""
  })
  const [userList, setUserList] = useState([
    // {
    //   "id": 2,
    //   "firstName": "Amir",
    //   "lastName": "Bagheri",
    //   "userTypeId": 1,
    //   "email": "amir@dentirate.com",
    //   "userType": "DentiRate",
    //   "createDate": "2023-09-02T01:13:16.8466667+00:00",
    //   "lastLoginAttempt": "2023-09-01T00:00:00-04:00",
    //   "emailVerificationDate": "2023-09-01T00:00:00-04:00",
    //   "needToChangePassword": false,
    //   "isEmailVerified": true,
    //   "isLockedOut": false,
    //   "isVerified": true,
    //   "isAdminLocked": false,
    //   "isDeleted": false,
    //   "isActiveByOrganization": true,
    //   "failedLoginAttempts": 0,
    //   "rowId": "5c768276-a22b-454d-a5de-4581e46c44be"
    // },
  ])
  const [locations,setLoations]=useState([])

  useEffect(()=>{
    // get user list
    // getUserList()
    addFifter()

  },[])

  const getUserList = async () => {
    const url ="/Account/GetUserList"
    const res = await sendPostByJson(url,{})
 
    if(res.data){
      setUserList(res.data.userList)
    }
  }
  //get loaction info
  const getLocations=async ()=>{
    const url=""
    const res= await sendPostByJson(url,{})
    if(res.data){
      setLoations(res.data)
    }
  }
  //change state 
  const changeState=async(v)=>{
    
    const url="/Account/ActiveByOrganization"
    const data={
      "userId":v.id,
      "isActiveByOrganization":!v.isActiveByOrganization,
      "lastDeactivatedById":v.id
    }
  
    try {
      
      const res= await sendPostByJson(url,data)
      if(res.data){
        let index=userList.findIndex(u=>u.id==v.id)
        v.isActiveByOrganization=v.isActiveByOrganization?false:true
        userList.splice(index,1,v)
        setUserList([...userList])
      }
    } catch (error) {
        ToastError("Failed to modify user status!",toastId)
    }
  }
  const serchLocationList=()=>{
    setLoations([
      {
      id:1,
      content:"test1",
      checked:false
    },
    {
      id:2,
      content:"test2",
      checked:false
    },
  ])
      // getLocations()
  } 

  const chooseActiveState=(arg)=>{
    setSearchObj({...searchObj,state:arg})
  }

  const alterSwitch = (v) => {
    changeState(v)
      
  }
  const openFifter = () => {

      setShowFilter(true)
      setLoations([])
      // renderLocationList()
  };
  const closeFifter = () => {

    setShowFilter(false)

  };

  const addFifter = async () => {
    const loginUser=getUserInfo()
    const url ="/Account/PaginatedUserList"
    try {
      
      const res = await sendGetKeyValue(url,{UserId:loginUser.userId,isActive:searchObj.state,location:searchObj.location,name:searchObj.keyword})
      if(res.data.code===200){
        setUserList(res.data.userList)
        setShowFilter(false)
      }
    } catch (error) {
      
    }
    //filter user list

  };
  const changeCheck=(i,e)=>{
    let val=e.target.checked
   
    let item=locations[i]
        item.checked=val
        locations.splice(i,1,item)
        setLoations([...locations])
        // renderLocationList()
  }
  const renderLocationList=()=>{
    if(locations.length==0){
      return false
    }
    return locations.map((v,i)=>{
        return(
          <li className="my_3 my_5_sm d_flex" key={i}>
                    <input className="form_checkbox" type="checkbox" value={v.checked}  checked={v.checked} onChange={(e)=>changeCheck(i,e)}  />
                    <label className="checkbox_label mt_1 fs_sm ms_2">
                      <div className="color_primary700">
                        <span className="fw_medium">legal name</span> (owner name)
                      </div>
                      <div className="mt_2 fs_sm">
                        4000 La Rica Ave Suite D Baldwin Park, CA 91706
                      </div>
                    </label>
             </li>
        )
    })
  }
  const renderStateList=()=>{
    return ActiveStateList.map((v,i)=>{
      return(
         <div className={`item ${searchObj.state==v.key?'active':''}`} key={i} onClick={()=>{chooseActiveState(v.key)}}>{v.value}</div>
      )
    })
  }
  const goUpdate=(arg)=>{
 
    navigate(`/account/new-form?uid=${arg.id}&typeId=4`) 
  }
  return (
    <>
  <ToastContainer limit={1} />
      <div className="layout">
      <TitleBar title={"User Management"}/>
        <div className="main">
          <div className="container">
            <div className="filter_box mt_2">
              <button className="btn btn_filter btn_md" onClick={openFifter}>
                <img
                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0zLjMwMTY4IDEuNUMzLjMxMjA5IDEuNSAzLjMyMjUzIDEuNSAzLjMzMzAxIDEuNUwxMi42OTc3IDEuNUMxMy4xNDIzIDEuNDk5OTggMTMuNTIyNCAxLjQ5OTk2IDEzLjgyNjIgMS41MzgyOEMxNC4xNDg0IDEuNTc4OTIgMTQuNDU5OCAxLjY3MDIgMTQuNzE1NiAxLjkxMDE2QzE0Ljk3NTggMi4xNTQzNiAxNS4wNzkzIDIuNDU5MDQgMTUuMTI0NyAyLjc3NjAzQzE1LjE2NjQgMy4wNjcyMyAxNS4xNjY0IDMuNDI5MzEgMTUuMTY2MyAzLjg0MjM5TDE1LjE2NjMgNC4zNjAwOEMxNS4xNjY0IDQuNjg1NzUgMTUuMTY2NCA0Ljk2NzMgMTUuMTQyIDUuMjAyMDdDMTUuMTE1OSA1LjQ1NDcxIDE1LjA1ODcgNS42OTE3MyAxNC45MjE0IDUuOTE5MjlDMTQuNzg1MSA2LjE0NTEzIDE0LjYwMTUgNi4zMDkwNyAxNC4zOSA2LjQ1NjAyQzE0LjE5MDcgNi41OTQ0NiAxMy45MzYgNi43Mzc4IDEzLjYzNjYgNi45MDYzN0wxMS42NzQ5IDguMDEwNjNDMTEuMjI4NCA4LjI2MTk5IDExLjA3MjkgOC4zNTI1IDEwLjk2OTEgOC40NDI2MkMxMC43MzA3IDguNjQ5NTggMTAuNTk0MiA4Ljg3OTIxIDEwLjUzMDEgOS4xNjY5MkMxMC41MDI2IDkuMjg5OTEgMTAuNDk5NyA5LjQ0NDgxIDEwLjQ5OTcgOS45MTUyOUwxMC40OTk3IDExLjczNjdDMTAuNDk5NyAxMi4zMzc1IDEwLjQ5OTcgMTIuODQ3NiAxMC40Mzc5IDEzLjIzOTdDMTAuMzcyMiAxMy42NTY3IDEwLjIxOTUgMTQuMDU2NiA5LjgxOTUyIDE0LjMwNjhDOS40Mjg2MiAxNC41NTEzIDguOTk4MDEgMTQuNTI4OSA4LjU3OTU0IDE0LjQyOTVDOC4xNzY1MSAxNC4zMzM4IDcuNjc5ODUgMTQuMTM5NiA3LjA4MzkxIDEzLjkwNjZMNy4wMjU5OSAxMy44ODRDNi43NDY4NCAxMy43NzQ5IDYuNTAyNDEgMTMuNjc5MyA2LjMwODkxIDEzLjU3OTRDNi4xMDA5NCAxMy40NzIgNS45MDc4MSAxMy4zMzg0IDUuNzYwMDQgMTMuMTMwNUM1LjYxMDYyIDEyLjkyMDIgNS41NTA3MSAxMi42OTQ3IDUuNTI0IDEyLjQ2NDJDNS40OTk2MyAxMi4yNTQgNS40OTk2NSAxMi4wMDE4IDUuNDk5NjggMTEuNzIwNEw1LjQ5OTY4IDkuOTE1MjlDNS40OTk2OCA5LjQ0NDgxIDUuNDk2NzEgOS4yODk5MSA1LjQ2OTI5IDkuMTY2OTJDNS40MDUxNCA4Ljg3OTIxIDUuMjY4NjIgOC42NDk1OCA1LjAzMDIzIDguNDQyNjJDNC45MjY0MyA4LjM1MjUgNC43NzA5NSA4LjI2MTk5IDQuMzI0NDMgOC4wMTA2M0wyLjM2MjgxIDYuOTA2MzdDMi4wNjMzMyA2LjczNzggMS44MDg3IDYuNTk0NDYgMS42MDk0IDYuNDU2MDJDMS4zOTc4NCA2LjMwOTA3IDEuMjE0MjggNi4xNDUxMyAxLjA3Nzk5IDUuOTE5MjlDMC45NDA2NjQgNS42OTE3MyAwLjg4MzQ3OSA1LjQ1NDcxIDAuODU3MzA2IDUuMjAyMDdDMC44MzI5ODQgNC45NjczIDAuODMyOTk2IDQuNjg1NzUgMC44MzMwMSA0LjM2MDA4TDAuODMzMDExIDMuODc2NDRDMC44MzMwMTEgMy44NjUwNSAwLjgzMzAxIDMuODUzNjkgMC44MzMwMSAzLjg0MjM4QzAuODMyOTc4IDMuNDI5MzEgMC44MzI5NTEgMy4wNjcyMyAwLjg3NDY3OSAyLjc3NjAzQzAuOTIwMTA0IDIuNDU5MDQgMS4wMjM1MSAyLjE1NDM2IDEuMjgzNzkgMS45MTAxNkMxLjUzOTU2IDEuNjcwMiAxLjg1MDk3IDEuNTc4OTIgMi4xNzMxNiAxLjUzODI4QzIuNDc2OTYgMS40OTk5NiAyLjg1NzA1IDEuNDk5OTggMy4zMDE2OCAxLjVaTTIuMjk4MzEgMi41MzA0MkMyLjA3NTg4IDIuNTU4NDggMi4wMDUxNCAyLjYwNDYxIDEuOTY4MDEgMi42Mzk0NEMxLjkzNTQgMi42NzAwNCAxLjg5MjM2IDIuNzIzOSAxLjg2NDU3IDIuOTE3ODlDMS44MzQyMSAzLjEyOTcgMS44MzMwMSAzLjQxOTEyIDEuODMzMDEgMy44NzY0NFY0LjMzNjMyQzEuODMzMDEgNC42OTI0NiAxLjgzMzYzIDQuOTIxODUgMS44NTE5OCA1LjA5OTAzQzEuODY5MDggNS4yNjQwNCAxLjg5ODQyIDUuMzQzMzggMS45MzQxNyA1LjQwMjYxQzEuOTcwOTQgNS40NjM1NSAyLjAzMjAxIDUuNTMyIDIuMTc5OSA1LjYzNDczQzIuMzM1NzEgNS43NDI5NiAyLjU0ODU4IDUuODYzMzkgMi44NzI5NiA2LjA0NTk5TDQuODE0OTggNy4xMzkyMkM0LjgzMzE2IDcuMTQ5NDUgNC44NTEwOCA3LjE1OTUzIDQuODY4NzIgNy4xNjk0NkM1LjI0MTE5IDcuMzc5IDUuNDk0ODYgNy41MjE3MiA1LjY4NTgxIDcuNjg3NDlDNi4wODAwNSA4LjAyOTc1IDYuMzMyOTkgOC40NDU0OCA2LjQ0NTMyIDguOTQ5M0M2LjQ5OTkyIDkuMTk0MTcgNi40OTk4MiA5LjQ2ODc0IDYuNDk5NjkgOS44NTYzMkM2LjQ5OTY4IDkuODc1NjkgNi40OTk2OCA5Ljg5NTM0IDYuNDk5NjggOS45MTUyOVYxMS42OTQ5QzYuNDk5NjggMTIuMDA5NyA2LjUwMDQ1IDEyLjIwMzMgNi41MTczNSAxMi4zNDkxQzYuNTMyNjIgMTIuNDgwOSA2LjU1NjY5IDEyLjUyNTIgNi41NzUyMSAxMi41NTEyQzYuNTk1NCAxMi41Nzk3IDYuNjM1MzkgMTIuNjIyNSA2Ljc2NzcgMTIuNjkwOUM2LjkwOTMxIDEyLjc2NCA3LjEwNDQgMTIuODQwOSA3LjQxMjQ5IDEyLjk2MTRDOC4wNTMxNiAxMy4yMTE4IDguNDg0MSAxMy4zNzkgOC44MTA2MiAxMy40NTY2QzkuMTI5NjkgMTMuNTMyMyA5LjIzNDgzIDEzLjQ5MyA5LjI4OTIzIDEzLjQ1OUM5LjMzNDUxIDEzLjQzMDcgOS40MDQ1MyAxMy4zNzMxIDkuNDUwMTIgMTMuMDg0QzkuNDk4MTkgMTIuNzc5MSA5LjQ5OTY4IDEyLjM0OSA5LjQ5OTY4IDExLjY5NDlWOS45MTUyOUM5LjQ5OTY4IDkuODk1MzUgOS40OTk2NyA5Ljg3NTcgOS40OTk2NiA5Ljg1NjMzQzkuNDk5NTMgOS40Njg3NSA5LjQ5OTQ0IDkuMTk0MTcgOS41NTQwMyA4Ljk0OTNDOS42NjYzNyA4LjQ0NTQ3IDkuOTE5MzEgOC4wMjk3NSAxMC4zMTM1IDcuNjg3NDlDMTAuNTA0NSA3LjUyMTcyIDEwLjc1ODIgNy4zNzkwMSAxMS4xMzA2IDcuMTY5NDdDMTEuMTQ4MyA3LjE1OTU0IDExLjE2NjIgNy4xNDk0NiAxMS4xODQ0IDcuMTM5MjFMMTMuMTI2NCA2LjA0NTk5QzEzLjQ1MDggNS44NjMzOSAxMy42NjM2IDUuNzQyOTYgMTMuODE5NSA1LjYzNDczQzEzLjk2NzMgNS41MzIgMTQuMDI4NCA1LjQ2MzU1IDE0LjA2NTIgNS40MDI2MUMxNC4xMDA5IDUuMzQzMzggMTQuMTMwMyA1LjI2NDA0IDE0LjE0NzQgNS4wOTkwM0MxNC4xNjU3IDQuOTIxODUgMTQuMTY2MyA0LjY5MjQ2IDE0LjE2NjMgNC4zMzYzMlYzLjg3NjQ0QzE0LjE2NjMgMy40MTkxMiAxNC4xNjUxIDMuMTI5NyAxNC4xMzQ4IDIuOTE3ODlDMTQuMTA3IDIuNzIzOSAxNC4wNjQgMi42NzAwNCAxNC4wMzEzIDIuNjM5NDRDMTMuOTk0MiAyLjYwNDYxIDEzLjkyMzUgMi41NTg0OCAxMy43MDEgMi41MzA0MkMxMy40NjczIDIuNTAwOTQgMTMuMTUxIDIuNSAxMi42NjYzIDIuNUgzLjMzMzAxQzIuODQ4MzIgMi41IDIuNTMyMDQgMi41MDA5NCAyLjI5ODMxIDIuNTMwNDJaIiBmaWxsPSIjNzk3ODc4Ii8+Cjwvc3ZnPgo="
                  alt=""
                />
                <span className="d_none d_inline_block_sm">Filters</span>
              </button>
              <div className="search_bar">
                <input
                  className="form_control"
                  type="text"
                  placeholder="Search users..."
                  value={searchObj.keyword}
                  onChange={(e)=>{setSearchObj({...searchObj,keyword:e.target.value.trim()})}}
                />
                <button className="btn btn_icon" onClick={addFifter}>
                  <img
                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzQ0MDhfMjUyNykiPgo8cGF0aCBkPSJNMTUuNTAwNiAxNC4wMDA2SDE0LjcxMDZMMTQuNDMwNiAxMy43MzA2QzE1LjYzMDYgMTIuMzMwNiAxNi4yNTA2IDEwLjQyMDYgMTUuOTEwNiA4LjM5MDYzQzE1LjQ0MDYgNS42MTA2MyAxMy4xMjA2IDMuMzkwNjMgMTAuMzIwNiAzLjA1MDYzQzYuMDkwNjMgMi41MzA2MyAyLjUzMDYzIDYuMDkwNjMgMy4wNTA2MyAxMC4zMjA2QzMuMzkwNjMgMTMuMTIwNiA1LjYxMDYzIDE1LjQ0MDYgOC4zOTA2MyAxNS45MTA2QzEwLjQyMDYgMTYuMjUwNiAxMi4zMzA2IDE1LjYzMDYgMTMuNzMwNiAxNC40MzA2TDE0LjAwMDYgMTQuNzEwNlYxNS41MDA2TDE4LjI1MDYgMTkuNzUwNkMxOC42NjA2IDIwLjE2MDYgMTkuMzMwNiAyMC4xNjA2IDE5Ljc0MDYgMTkuNzUwNkMyMC4xNTA2IDE5LjM0MDYgMjAuMTUwNiAxOC42NzA2IDE5Ljc0MDYgMTguMjYwNkwxNS41MDA2IDE0LjAwMDZaTTkuNTAwNjMgMTQuMDAwNkM3LjAxMDYzIDE0LjAwMDYgNS4wMDA2MyAxMS45OTA2IDUuMDAwNjMgOS41MDA2M0M1LjAwMDYzIDcuMDEwNjMgNy4wMTA2MyA1LjAwMDYzIDkuNTAwNjMgNS4wMDA2M0MxMS45OTA2IDUuMDAwNjMgMTQuMDAwNiA3LjAxMDYzIDE0LjAwMDYgOS41MDA2M0MxNC4wMDA2IDExLjk5MDYgMTEuOTkwNiAxNC4wMDA2IDkuNTAwNjMgMTQuMDAwNloiIGZpbGw9IiM4RThFOEUiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMF80NDA4XzI1MjciPgo8cmVjdCB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="
                    alt=""
                  />
                </button>
              </div>
              <button className="btn btn_primary add_new ms_auto" onClick={() => navigate('/account/new-form?typeId=3')}>
                + New User
              </button>
            </div>
            <ul className="row mt_5">

              {
                userList.map((v,i)=>{
                  return(
                    <li className="col col_2_sm" key={i}>
                    <div className="card h_100">
                      <div className="card_title marker">
                        <div className={`${v.isActiveByOrganization?'text_succes':'text_fail'}`}>
                          <span className="ms_1" >{v.firstName||''}&nbsp;{v.lastName}&nbsp;</span>
                          <span className="suffix ms_1"> { "("+v.userType+")"}</span>
                        </div>
                        <button className="btn edit ms_auto" onClick={() => { goUpdate(v)}} />
                        <div className={`form_switch ${v.isActiveByOrganization?'active':''}`} onClick={()=>alterSwitch(v)} />
                      </div>
                      <div className="card_content my_2 my_4_md">
                        <span className="color_gray fs_sm">Email:</span>
                        <span>{v.email||''}</span>
                        <div className="mt_3 mt_4_md">
                          <div className="color_primary700">
                            <i className="icon i_location" />
                            <span className="fw_medium">legal name</span> (owner name)
                          </div>
                          <div className="mt_2">
                            4000 La Rica Ave Suite D Baldwin Park, CA 91706
                          </div>
                        </div>
                        <div className="mt_3 mt_4_md">
                          <div className="color_primary700">
                            <i className="icon i_location" />
                            <span className="fw_medium t_middle">legal name</span>{" "}
                            (owner name)
                          </div>
                          <div className="mt_2">
                            4000 La Rica Ave Suite D Baldwin Park, CA 91706
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                 ) })
              }
            

              
            </ul>
          </div>
        </div>
      </div>

      <div className={`offcanvas ${showFilter ? 'show' : ''}`} >
        <div className="offcanvas_box">
          <div className="offcanvas_header">
            <span className="title">Filters</span>
            <button className="btn_close ms_auto" onClick={closeFifter} />
          </div>
          <div className="offcanvas_content filter_content">
            <div className="mt_5 mt_4_md">
              <div className="filter_title">Status</div>
              <div className="sort_list my_3">
                {renderStateList()}
              
                {/* <div className="item active">All</div>
                <div className="item">Active</div>
                <div className="item">Inactive</div> */}
              </div>
            </div>

            <div className="filter_container">
              <div className="mt_5 mt_4_md">
                <div className="filter_title">Locations</div>
                <div className="search_bar mt_3">
                  <input
                    className="form_control"
                    type="text"
                    placeholder="Search locations..."
                    value={searchObj.locationStr}
                    onChange={(e)=>{setSearchObj({...searchObj,locationStr:e.target.value.trim()})}}
                  />
                  <button className="btn btn_icon" onClick={serchLocationList}>
                    <img
                      src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzQ0MDhfMjUyNykiPgo8cGF0aCBkPSJNMTUuNTAwNiAxNC4wMDA2SDE0LjcxMDZMMTQuNDMwNiAxMy43MzA2QzE1LjYzMDYgMTIuMzMwNiAxNi4yNTA2IDEwLjQyMDYgMTUuOTEwNiA4LjM5MDYzQzE1LjQ0MDYgNS42MTA2MyAxMy4xMjA2IDMuMzkwNjMgMTAuMzIwNiAzLjA1MDYzQzYuMDkwNjMgMi41MzA2MyAyLjUzMDYzIDYuMDkwNjMgMy4wNTA2MyAxMC4zMjA2QzMuMzkwNjMgMTMuMTIwNiA1LjYxMDYzIDE1LjQ0MDYgOC4zOTA2MyAxNS45MTA2QzEwLjQyMDYgMTYuMjUwNiAxMi4zMzA2IDE1LjYzMDYgMTMuNzMwNiAxNC40MzA2TDE0LjAwMDYgMTQuNzEwNlYxNS41MDA2TDE4LjI1MDYgMTkuNzUwNkMxOC42NjA2IDIwLjE2MDYgMTkuMzMwNiAyMC4xNjA2IDE5Ljc0MDYgMTkuNzUwNkMyMC4xNTA2IDE5LjM0MDYgMjAuMTUwNiAxOC42NzA2IDE5Ljc0MDYgMTguMjYwNkwxNS41MDA2IDE0LjAwMDZaTTkuNTAwNjMgMTQuMDAwNkM3LjAxMDYzIDE0LjAwMDYgNS4wMDA2MyAxMS45OTA2IDUuMDAwNjMgOS41MDA2M0M1LjAwMDYzIDcuMDEwNjMgNy4wMTA2MyA1LjAwMDYzIDkuNTAwNjMgNS4wMDA2M0MxMS45OTA2IDUuMDAwNjMgMTQuMDAwNiA3LjAxMDYzIDE0LjAwMDYgOS41MDA2M0MxNC4wMDA2IDExLjk5MDYgMTEuOTkwNiAxNC4wMDA2IDkuNTAwNjMgMTQuMDAwNloiIGZpbGw9IiM4RThFOEUiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMF80NDA4XzI1MjciPgo8cmVjdCB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="
                      alt=""
                    />
                  </button>
                </div>
                <ul className="" style={{maxHeight:"200px",overflow:"auto"}}>
                    {renderLocationList()}
                </ul>
              </div>
            </div>
          </div>
          <div className="offcanvas_footer filter_footer">
            <div className="btn_group">
              <button className="btn btn_primary_line_transparent" onClick={closeFifter}>Cancel</button>
              <button className="btn btn_primary ms_auto col" onClick={addFifter} >Apply</button>
            </div>
          </div>
        </div>
      </div>

    </>



  );
};

UserManagement.propTypes = {};

export default UserManagement;
