import React, { useState, useRef } from "react";
import { useLocation, useNavigate } from "react-router-dom";

import { ToastContainer } from 'react-toastify';
import { checkIsEmail, ToastInfo, ToastError, } from "../../utils/check.js";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";

import logo from "../../assets/logo-1.svg"
const PatientStart = (props) => {
  const toastId = useRef(null)
  const navigate = useNavigate();
  const [ssn, setSsn] = useState("")
  const [error, setError] = useState(false)
  const ssnLogin = () => {
    if (!ssn) {
      setSsn(true)
      return
    }
    else if (ssn.length !== 4) {
      setSsn(true)
      return
    }
    else {
      setSsn(false)
    }
    navigate('/account/patient-home')
  }
  const login = async () => {
    const data = {}
    const url = ""
    try {
      const res = await sendPostByJson(url, data)
      if (res.data) {

      }
    } catch (error) {
      ToastError("Login failed, please try again latter!", toastId)
    }

  }
  return (
    <>
      <ToastContainer limit={1} />
      <div className="container">
        <div className="login_container applicant_container">
          <div className="logo mb_4">
            <img style={{ width: "128px", height: "30px" }}
              src={logo}
              alt=""
            />
          </div>
          <div className="t_welcome">Welcome to Dentirent</div>
          <p className="fw_bold mt_7 mt_8_md">
            Please enter your SSN last 4 digit to continue:
          </p>
          <form className="form mt_4 mt_6_md">
            <input
              className="form_control"
              type="text"
              placeholder="SSN last 4 digit"
              value={ssn}
              maxLength={4}
              onChange={e => setSsn(e.target.value.trim())}
            />
            <div className="fs_sm color_error mt_1" style={{ visibility: error ? 'visible' : 'hidden' }}>Please enter SSN last 4 digit</div>
          </form>
          <div className="service mt_5 mt_6_md">
            <span>By continuing, you agree to our </span>
            <a className="link_primary fw_bold" href="">
              Terms of Service
            </a>
          </div>
          <button className="btn btn_primary w_100 mt_3" onClick={ssnLogin}>Continue</button>
        </div>
      </div>
    </>




  );
};

PatientStart.propTypes = {};

export default PatientStart;
