import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import TitleBar from "../../components/header/titleBar.js";

import { checkIsEmail } from "../../utils/check.js";

import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";

import { setUserInfo } from "../../utils/storage.js"


import SelectLang from "../../components/selectLanguge"
import PageFooter from "../../components/pageFooter";
import LenderItem from '../../components/lenderItem'
import tip from '../../assets/tip.png'

const PatientHome = (props) => {
  const navigate = useNavigate();
  const [showTip,setShowTip]=useState(false)
  const [showls,setShowls]=useState(true)
  const [applyOk,setApplyOk]=useState(false)
  const searchValue = useLocation();

 

 const renderApplyResult=(arg)=>{
 
 
    if(arg){
      setApplyOk(true)
      setShowls(false)
    }else{
      setApplyOk(false)
      setShowls(true)
      setShowTip(true)
    }
 }

  return (
    <>
      <div className="layout">
        <header className="header_patient">
          <div className="nav container">
            <div className="title">Frankie Dejong</div>
             <SelectLang/>
          </div>
        </header>
        <div className="main main_patient">
          <div className="main_patient_box">
            <div className="bg_white">
              <div className="container">
                <div className="patient_top">
                  <table className="table">
                    <tbody className="">
                      <tr>
                        <td className="color_gray fs_sm">Amount:</td>
                        <td className="fw_bold">$4500</td>
                      </tr>
                      <tr>
                        <td className="color_gray fs_sm">Treatment type:</td>
                        <td>Treatment type</td>
                      </tr>
                      <tr>
                        <td className="color_gray fs_sm">
                          preferred monthly payment:
                        </td>
                        <td>$200</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="d_flex">
                  <a className="link_primary ms_auto mb_3" href="">
                    Advertisement Disclosure
                  </a>
                </div>
              </div>
            </div>
            <div className="container">
              <div style={{ display: showTip?"true":'none' }} id="patientError">
                <div className="patient_error mt_5 mt_3_sm mt_6_md">
                  <img
                    className="icon_error my_0_sm"
                    src={tip}
                    alt=""
                  />
                  <div>
                    <span className="d_block d_inline_sm">
                      there is no preapproval for your request.
                    </span>{" "}
                    <span className="d_block d_inline_sm">
                      contact suggested lender for further processing
                    </span>
                  </div>
                </div>
                <div className="t_center fs_lg color_gray fw_medium mt_3 mt_5_sm">
                  <div className="d_inline_sm">
                    You can fill your ptreferred application
                  </div>
                  <div className="color_primary fs_lg fw_bold d_inline_sm"> here </div>
                  <div className="d_inline_sm">it may help you with lenders</div>
                </div>
              </div>


              <div className="t_center fs_lg mt_5 mt_6_sm" style={{ display:showls? "block":"none" }} id="patient">
                <div className="fs_lg fw_bold me_1_sm d_inline_sm">Congratulation!</div>
                <div className="mt_2 fw_medium mt_0_sm d_inline_sm">We got you pre-approved with <span className="fs_lg color_primary fw_bold">12</span> lenders</div>
              </div>
              <div className="t_center fs_lg mt_5 mt_6_sm" style={{ display:applyOk?"block":"none" }} id="patientOk">
                <div className="fs_lg fw_bold me_1_sm d_inline_sm">Congratulation! </div>
                <div className="mt_2 fw_medium mt_0_sm d_inline_sm"> We got you pre-approved with <span className="fs_lg color_primary fw_bold">Andhara bank</span></div>
              </div>
             
              <LenderItem showApply={renderApplyResult} />
              
            </div>
          </div>
          <PageFooter/>
        </div>
      </div>
    </>





  );
};

PatientHome.propTypes = {};

export default PatientHome;
