import React, { useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { checkIsEmail } from "../../utils/check.js";
import HomeChart from "../../components/homeChart/homeChart.js"
// import { FormattedMessage,useIntl} from "react-intl";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";
// import PropTypes from "prop-types";
// import loginStyle from "../../css/static.css";
// import { useSnackbar } from 'notistack';
import { getItem, setUserInfo } from "../../utils/storage.js"
// import OutlinedInput from '@mui/material/OutlinedInput';

const Home = (props) => {
  const navigate = useNavigate();
  const source =parseInt(getItem("source")) 
  return (
    <>
      <div className="layout">
        <header>
          <div className="nav container">
            <h4>Home</h4>
            <div className="nav_right">
              <button className="btn btn_primary d_block_md d_none">
                Dashboard
              </button>
              <button className="btn btn_icon" onClick={() => navigate('/account/user-profile')}>
                <img
                  className="icon"
                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGNpcmNsZSBjeD0iMjQiIGN5PSIxMiIgcj0iOCIgZmlsbD0iIzFGNEE5NCIvPgo8ZWxsaXBzZSBjeD0iMjQiIGN5PSIzNCIgcng9IjE0IiByeT0iOCIgZmlsbD0iIzFGNEE5NCIvPgo8L3N2Zz4="
                  alt=""
                />
              </button>
              <button className="btn btn_icon">
                <img
                  className="icon"
                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTkuNSAyQzguNjcxNTcgMiA4IDIuNjcxNTcgOCAzLjVWNC41QzggNS4zMjg0MyA4LjY3MTU3IDYgOS41IDZIMTQuNUMxNS4zMjg0IDYgMTYgNS4zMjg0MyAxNiA0LjVWMy41QzE2IDIuNjcxNTcgMTUuMzI4NCAyIDE0LjUgMkg5LjVaIiBmaWxsPSIjMUY0QTk0Ii8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNNi41IDQuMDM2NjJDNS4yNDIwOSA0LjEwNzE5IDQuNDQ3OTggNC4zMDc2NCAzLjg3ODY4IDQuODc2OTRDMyA1Ljc1NTYyIDMgNy4xNjk4MyAzIDkuOTk4MjZWMTUuOTk4M0MzIDE4LjgyNjcgMyAyMC4yNDA5IDMuODc4NjggMjEuMTE5NkM0Ljc1NzM2IDIxLjk5ODMgNi4xNzE1NyAyMS45OTgzIDkgMjEuOTk4M0gxNUMxNy44Mjg0IDIxLjk5ODMgMTkuMjQyNiAyMS45OTgzIDIwLjEyMTMgMjEuMTE5NkMyMSAyMC4yNDA5IDIxIDE4LjgyNjcgMjEgMTUuOTk4M1Y5Ljk5ODI2QzIxIDcuMTY5ODMgMjEgNS43NTU2MiAyMC4xMjEzIDQuODc2OTRDMTkuNTUyIDQuMzA3NjQgMTguNzU3OSA0LjEwNzE5IDE3LjUgNC4wMzY2MlY0LjVDMTcuNSA2LjE1Njg1IDE2LjE1NjkgNy41IDE0LjUgNy41SDkuNUM3Ljg0MzE1IDcuNSA2LjUgNi4xNTY4NSA2LjUgNC41VjQuMDM2NjJaTTcgOS43NUM2LjU4NTc5IDkuNzUgNi4yNSAxMC4wODU4IDYuMjUgMTAuNUM2LjI1IDEwLjkxNDIgNi41ODU3OSAxMS4yNSA3IDExLjI1SDcuNUM3LjkxNDIxIDExLjI1IDguMjUgMTAuOTE0MiA4LjI1IDEwLjVDOC4yNSAxMC4wODU4IDcuOTE0MjEgOS43NSA3LjUgOS43NUg3Wk0xMC41IDkuNzVDMTAuMDg1OCA5Ljc1IDkuNzUgMTAuMDg1OCA5Ljc1IDEwLjVDOS43NSAxMC45MTQyIDEwLjA4NTggMTEuMjUgMTAuNSAxMS4yNUgxN0MxNy40MTQyIDExLjI1IDE3Ljc1IDEwLjkxNDIgMTcuNzUgMTAuNUMxNy43NSAxMC4wODU4IDE3LjQxNDIgOS43NSAxNyA5Ljc1SDEwLjVaTTcgMTMuMjVDNi41ODU3OSAxMy4yNSA2LjI1IDEzLjU4NTggNi4yNSAxNEM2LjI1IDE0LjQxNDIgNi41ODU3OSAxNC43NSA3IDE0Ljc1SDcuNUM3LjkxNDIxIDE0Ljc1IDguMjUgMTQuNDE0MiA4LjI1IDE0QzguMjUgMTMuNTg1OCA3LjkxNDIxIDEzLjI1IDcuNSAxMy4yNUg3Wk0xMC41IDEzLjI1QzEwLjA4NTggMTMuMjUgOS43NSAxMy41ODU4IDkuNzUgMTRDOS43NSAxNC40MTQyIDEwLjA4NTggMTQuNzUgMTAuNSAxNC43NUgxN0MxNy40MTQyIDE0Ljc1IDE3Ljc1IDE0LjQxNDIgMTcuNzUgMTRDMTcuNzUgMTMuNTg1OCAxNy40MTQyIDEzLjI1IDE3IDEzLjI1SDEwLjVaTTcgMTYuNzVDNi41ODU3OSAxNi43NSA2LjI1IDE3LjA4NTggNi4yNSAxNy41QzYuMjUgMTcuOTE0MiA2LjU4NTc5IDE4LjI1IDcgMTguMjVINy41QzcuOTE0MjEgMTguMjUgOC4yNSAxNy45MTQyIDguMjUgMTcuNUM4LjI1IDE3LjA4NTggNy45MTQyMSAxNi43NSA3LjUgMTYuNzVIN1pNMTAuNSAxNi43NUMxMC4wODU4IDE2Ljc1IDkuNzUgMTcuMDg1OCA5Ljc1IDE3LjVDOS43NSAxNy45MTQyIDEwLjA4NTggMTguMjUgMTAuNSAxOC4yNUgxN0MxNy40MTQyIDE4LjI1IDE3Ljc1IDE3LjkxNDIgMTcuNzUgMTcuNUMxNy43NSAxNy4wODU4IDE3LjQxNDIgMTYuNzUgMTcgMTYuNzVIMTAuNVoiIGZpbGw9IiMxRjRBOTQiLz4KPC9zdmc+Cg=="
                  alt=""
                />
              </button>
              <button className="btn btn_icon">
                <img
                  className="icon"
                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0yNi4xNzMzIDQyLjc3NTRMMjcuMjU3NSA0MC45NDM3QzI4LjA5ODUgMzkuNTIyOSAyOC41MTg5IDM4LjgxMjUgMjkuMTk0MyAzOC40MTk3QzI5Ljg2OTcgMzguMDI2OSAzMC43MjAxIDM4LjAxMjIgMzIuNDIwOCAzNy45ODI5QzM0LjkzMTUgMzcuOTM5NiAzNi41MDYyIDM3Ljc4NTggMzcuODI2OCAzNy4yMzg4QzQwLjI3NzEgMzYuMjIzOSA0Mi4yMjM5IDM0LjI3NzEgNDMuMjM4OCAzMS44MjY4QzQ0IDI5Ljk4OTEgNDQgMjcuNjU5NCA0NCAyM1YyMUM0NCAxNC40NTMxIDQ0IDExLjE3OTcgNDIuNTI2NCA4Ljc3NTAxQzQxLjcwMTggNy40Mjk0NiA0MC41NzA1IDYuMjk4MTYgMzkuMjI1IDUuNDczNkMzNi44MjAzIDQgMzMuNTQ2OSA0IDI3IDRIMjFDMTQuNDUzMSA0IDExLjE3OTcgNCA4Ljc3NTAxIDUuNDczNkM3LjQyOTQ2IDYuMjk4MTYgNi4yOTgxNiA3LjQyOTQ2IDUuNDczNiA4Ljc3NTAxQzQgMTEuMTc5NyA0IDE0LjQ1MzEgNCAyMVYyM0M0IDI3LjY1OTQgNCAyOS45ODkxIDQuNzYxMiAzMS44MjY4QzUuNzc2MTQgMzQuMjc3MSA3LjcyMjg4IDM2LjIyMzkgMTAuMTczMiAzNy4yMzg4QzExLjQ5MzggMzcuNzg1OCAxMy4wNjg0IDM3LjkzOTYgMTUuNTc5MiAzNy45ODI5QzE3LjI3OTggMzguMDEyMiAxOC4xMzAyIDM4LjAyNjkgMTguODA1NiAzOC40MTk3QzE5LjQ4MSAzOC44MTI1IDE5LjkwMTUgMzkuNTIyOSAyMC43NDI0IDQwLjk0MzdMMjEuODI2NiA0Mi43NzU0QzIyLjc5MjkgNDQuNDA4IDI1LjIwNyA0NC40MDggMjYuMTczMyA0Mi43NzU0Wk0zMiAyNEMzMy4xMDQ2IDI0IDM0IDIzLjEwNDYgMzQgMjJDMzQgMjAuODk1NCAzMy4xMDQ2IDIwIDMyIDIwQzMwLjg5NTQgMjAgMzAgMjAuODk1NCAzMCAyMkMzMCAyMy4xMDQ2IDMwLjg5NTQgMjQgMzIgMjRaTTI2IDIyQzI2IDIzLjEwNDYgMjUuMTA0NiAyNCAyNCAyNEMyMi44OTU0IDI0IDIyIDIzLjEwNDYgMjIgMjJDMjIgMjAuODk1NCAyMi44OTU0IDIwIDI0IDIwQzI1LjEwNDYgMjAgMjYgMjAuODk1NCAyNiAyMlpNMTYgMjRDMTcuMTA0NiAyNCAxOCAyMy4xMDQ2IDE4IDIyQzE4IDIwLjg5NTQgMTcuMTA0NiAyMCAxNiAyMEMxNC44OTU0IDIwIDE0IDIwLjg5NTQgMTQgMjJDMTQgMjMuMTA0NiAxNC44OTU0IDI0IDE2IDI0WiIgZmlsbD0iIzFGNEE5NCIvPgo8L3N2Zz4K"
                  alt=""
                />
              </button>
              <button className="btn btn_icon d_block_md d_none">
                <img
                  className="icon"
                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0yNCA1LjVDMTMuNzgyNyA1LjUgNS41IDEzLjc4MjcgNS41IDI0QzUuNSAzNC4yMTczIDEzLjc4MjcgNDIuNSAyNCA0Mi41QzM0LjIxNzMgNDIuNSA0Mi41IDM0LjIxNzMgNDIuNSAyNEM0Mi41IDEzLjc4MjcgMzQuMjE3MyA1LjUgMjQgNS41Wk0yLjUgMjRDMi41IDEyLjEyNTkgMTIuMTI1OSAyLjUgMjQgMi41QzM1Ljg3NDEgMi41IDQ1LjUgMTIuMTI1OSA0NS41IDI0QzQ1LjUgMzUuODc0MSAzNS44NzQxIDQ1LjUgMjQgNDUuNUMxMi4xMjU5IDQ1LjUgMi41IDM1Ljg3NDEgMi41IDI0Wk0yNCAxNS41QzIyLjc1NzQgMTUuNSAyMS43NSAxNi41MDc0IDIxLjc1IDE3Ljc1QzIxLjc1IDE4LjU3ODQgMjEuMDc4NCAxOS4yNSAyMC4yNSAxOS4yNUMxOS40MjE2IDE5LjI1IDE4Ljc1IDE4LjU3ODQgMTguNzUgMTcuNzVDMTguNzUgMTQuODUwNSAyMS4xMDA1IDEyLjUgMjQgMTIuNUMyNi44OTk1IDEyLjUgMjkuMjUgMTQuODUwNSAyOS4yNSAxNy43NUMyOS4yNSAxOS42NzY3IDI4LjIxMTMgMjEuMzU5MSAyNi42NzA1IDIyLjI3MDdDMjYuMjc3IDIyLjUwMzUgMjUuOTUyMiAyMi43NTc3IDI1Ljc0MDYgMjMuMDA3MUMyNS41MzUxIDIzLjI0OTMgMjUuNSAyMy40MDcyIDI1LjUgMjMuNVYyNkMyNS41IDI2LjgyODQgMjQuODI4NCAyNy41IDI0IDI3LjVDMjMuMTcxNiAyNy41IDIyLjUgMjYuODI4NCAyMi41IDI2VjIzLjVDMjIuNSAyMi40ODgyIDIyLjk0MyAyMS42NjczIDIzLjQ1MzIgMjEuMDY2QzIzLjk1NzIgMjAuNDcyIDI0LjU4NTggMjAuMDE4NCAyNS4xNDI5IDE5LjY4ODhDMjUuODA4OSAxOS4yOTQ4IDI2LjI1IDE4LjU3MzEgMjYuMjUgMTcuNzVDMjYuMjUgMTYuNTA3NCAyNS4yNDI2IDE1LjUgMjQgMTUuNVpNMjQgMzRDMjUuMTA0NiAzNCAyNiAzMy4xMDQ2IDI2IDMyQzI2IDMwLjg5NTQgMjUuMTA0NiAzMCAyNCAzMEMyMi44OTU0IDMwIDIyIDMwLjg5NTQgMjIgMzJDMjIgMzMuMTA0NiAyMi44OTU0IDM0IDI0IDM0WiIgZmlsbD0iIzFGNEE5NCIvPgo8L3N2Zz4="
                  alt=""
                />
              </button>
            </div>
          </div>
          <div className="d_none_md container nav_wrap">
            <button className="btn btn_primary">Dashboard</button>
            <button className="btn btn_icon ms_auto">
              <img
                className="icon"
                src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0yNCA1LjVDMTMuNzgyNyA1LjUgNS41IDEzLjc4MjcgNS41IDI0QzUuNSAzNC4yMTczIDEzLjc4MjcgNDIuNSAyNCA0Mi41QzM0LjIxNzMgNDIuNSA0Mi41IDM0LjIxNzMgNDIuNSAyNEM0Mi41IDEzLjc4MjcgMzQuMjE3MyA1LjUgMjQgNS41Wk0yLjUgMjRDMi41IDEyLjEyNTkgMTIuMTI1OSAyLjUgMjQgMi41QzM1Ljg3NDEgMi41IDQ1LjUgMTIuMTI1OSA0NS41IDI0QzQ1LjUgMzUuODc0MSAzNS44NzQxIDQ1LjUgMjQgNDUuNUMxMi4xMjU5IDQ1LjUgMi41IDM1Ljg3NDEgMi41IDI0Wk0yNCAxNS41QzIyLjc1NzQgMTUuNSAyMS43NSAxNi41MDc0IDIxLjc1IDE3Ljc1QzIxLjc1IDE4LjU3ODQgMjEuMDc4NCAxOS4yNSAyMC4yNSAxOS4yNUMxOS40MjE2IDE5LjI1IDE4Ljc1IDE4LjU3ODQgMTguNzUgMTcuNzVDMTguNzUgMTQuODUwNSAyMS4xMDA1IDEyLjUgMjQgMTIuNUMyNi44OTk1IDEyLjUgMjkuMjUgMTQuODUwNSAyOS4yNSAxNy43NUMyOS4yNSAxOS42NzY3IDI4LjIxMTMgMjEuMzU5MSAyNi42NzA1IDIyLjI3MDdDMjYuMjc3IDIyLjUwMzUgMjUuOTUyMiAyMi43NTc3IDI1Ljc0MDYgMjMuMDA3MUMyNS41MzUxIDIzLjI0OTMgMjUuNSAyMy40MDcyIDI1LjUgMjMuNVYyNkMyNS41IDI2LjgyODQgMjQuODI4NCAyNy41IDI0IDI3LjVDMjMuMTcxNiAyNy41IDIyLjUgMjYuODI4NCAyMi41IDI2VjIzLjVDMjIuNSAyMi40ODgyIDIyLjk0MyAyMS42NjczIDIzLjQ1MzIgMjEuMDY2QzIzLjk1NzIgMjAuNDcyIDI0LjU4NTggMjAuMDE4NCAyNS4xNDI5IDE5LjY4ODhDMjUuODA4OSAxOS4yOTQ4IDI2LjI1IDE4LjU3MzEgMjYuMjUgMTcuNzVDMjYuMjUgMTYuNTA3NCAyNS4yNDI2IDE1LjUgMjQgMTUuNVpNMjQgMzRDMjUuMTA0NiAzNCAyNiAzMy4xMDQ2IDI2IDMyQzI2IDMwLjg5NTQgMjUuMTA0NiAzMCAyNCAzMEMyMi44OTU0IDMwIDIyIDMwLjg5NTQgMjIgMzJDMjIgMzMuMTA0NiAyMi44OTU0IDM0IDI0IDM0WiIgZmlsbD0iIzFGNEE5NCIvPgo8L3N2Zz4="
                alt=""
              />
            </button>
          </div>
        </header>
        <div className="main">
          <div className="container">
            <form className="form_row mt_2">
              <label className="form_label">Show data for:</label>
              <div className="form_select show">
                <select className="form_control" placeholder="Enter ID">
                  <option value='1' >week1</option>
                  <option value='2' >week2</option>
                </select>
                {/* <div className="option_box">
              <div className="scroll_bar">
                <div className="option">1 week</div>
                <div className="option">1 week</div>
                <div className="option">1 week</div>
              </div>
            </div> */}
              </div>
            </form>
            <ul className="home_data mt_5">
              <li className="item bg_orange">
                <div className="count">12</div>
                <div className="name">applications</div>
              </li>
              <li className="item bg_success">
                <div className="count">32</div>
                <div className="name">pre-approvals</div>
              </li>
              <li className="item bg_teal">
                <div className="count">56</div>
                <div className="name">leads</div>
              </li>
            </ul>
            <div className="chart_container mt_5">
              <HomeChart />
            </div>
            <div className="home_menu mt_6">
              {
                (source === 200 || source === 201) &&
                // eslint-disable-next-line jsx-a11y/anchor-is-valid
                <a className="item bg_primary"  onClick={() => { navigate("/account/leads") }}>
                  <img
                    className="icon"
                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjUiIGhlaWdodD0iMjgiIHZpZXdCb3g9IjAgMCAyNSAyOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0yLjA2MjEgMi4yMjg3MkMwLjUgMy43OTA4MiAwLjUgNi4zMDQ5OCAwLjUgMTEuMzMzM1YxNi42NjY2QzAuNSAyMS42OTQ5IDAuNSAyNC4yMDkxIDIuMDYyMSAyNS43NzEyQzMuNjI0MTkgMjcuMzMzMyA2LjEzODM1IDI3LjMzMzMgMTEuMTY2NyAyNy4zMzMzSDEzLjgzMzNDMTguODYxNiAyNy4zMzMzIDIxLjM3NTggMjcuMzMzMyAyMi45Mzc5IDI1Ljc3MTJDMjQuNSAyNC4yMDkxIDI0LjUgMjEuNjk0OSAyNC41IDE2LjY2NjZWMTEuMzMzM0MyNC41IDYuMzA0OTggMjQuNSAzLjc5MDgyIDIyLjkzNzkgMi4yMjg3MkMyMS4zNzU4IDAuNjY2NjI2IDE4Ljg2MTYgMC42NjY2MjYgMTMuODMzMyAwLjY2NjYyNkgxMS4xNjY3QzYuMTM4MzUgMC42NjY2MjYgMy42MjQxOSAwLjY2NjYyNiAyLjA2MjEgMi4yMjg3MlpNMTMuNSA1Ljk5OTk2QzEzLjUgNS40NDc2NyAxMy4wNTIzIDQuOTk5OTYgMTIuNSA0Ljk5OTk2QzExLjk0NzcgNC45OTk5NiAxMS41IDUuNDQ3NjcgMTEuNSA1Ljk5OTk2VjcuNjY2NjNIOS44MzMyOUM5LjI4MTAxIDcuNjY2NjMgOC44MzMyOSA4LjExNDM0IDguODMzMjkgOC42NjY2M0M4LjgzMzI5IDkuMjE4OTEgOS4yODEwMSA5LjY2NjYzIDkuODMzMjkgOS42NjY2M0gxMS41TDExLjUgMTEuMzMzM0MxMS41IDExLjg4NTYgMTEuOTQ3NyAxMi4zMzMzIDEyLjUgMTIuMzMzM0MxMy4wNTIzIDEyLjMzMzMgMTMuNSAxMS44ODU2IDEzLjUgMTEuMzMzM0wxMy41IDkuNjY2NjNMMTUuMTY2NiA5LjY2NjYzQzE1LjcxODkgOS42NjY2MyAxNi4xNjY2IDkuMjE4OTEgMTYuMTY2NiA4LjY2NjYzQzE2LjE2NjYgOC4xMTQzNCAxNS43MTg5IDcuNjY2NjMgMTUuMTY2NiA3LjY2NjYzSDEzLjVWNS45OTk5NlpNNy4xNjY2NyAxNS42NjY2QzYuNjE0MzggMTUuNjY2NiA2LjE2NjY3IDE2LjExNDMgNi4xNjY2NyAxNi42NjY2QzYuMTY2NjcgMTcuMjE4OSA2LjYxNDM4IDE3LjY2NjYgNy4xNjY2NyAxNy42NjY2SDE3LjgzMzNDMTguMzg1NiAxNy42NjY2IDE4LjgzMzMgMTcuMjE4OSAxOC44MzMzIDE2LjY2NjZDMTguODMzMyAxNi4xMTQzIDE4LjM4NTYgMTUuNjY2NiAxNy44MzMzIDE1LjY2NjZINy4xNjY2N1pNOC41IDIxQzcuOTQ3NzIgMjEgNy41IDIxLjQ0NzcgNy41IDIyQzcuNSAyMi41NTIyIDcuOTQ3NzIgMjMgOC41IDIzSDE2LjVDMTcuMDUyMyAyMyAxNy41IDIyLjU1MjIgMTcuNSAyMkMxNy41IDIxLjQ0NzcgMTcuMDUyMyAyMSAxNi41IDIxSDguNVoiIGZpbGw9IndoaXRlIi8+Cjwvc3ZnPgo="
                    alt=""
                  />
                  <div className="name">Leads</div>
                </a>
              }
              {(source === 1 || source === 100) &&
                <a className="item bg_primary"  onClick={() =>  navigate("/account/applications") }>
                  <img
                    className="icon"
                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik04LjM0MzE1IDYuMzQzMTVDNiA4LjY4NjI5IDYgMTIuNDU3NSA2IDIwVjI4QzYgMzUuNTQyNSA2IDM5LjMxMzcgOC4zNDMxNSA0MS42NTY5QzEwLjY4NjMgNDQgMTQuNDU3NSA0NCAyMiA0NEgyNkMzMy41NDI1IDQ0IDM3LjMxMzcgNDQgMzkuNjU2OSA0MS42NTY5QzQyIDM5LjMxMzcgNDIgMzUuNTQyNSA0MiAyOFYyMEM0MiAxMi40NTc1IDQyIDguNjg2MjkgMzkuNjU2OSA2LjM0MzE1QzM3LjMxMzcgNCAzMy41NDI1IDQgMjYgNEgyMkMxNC40NTc1IDQgMTAuNjg2MyA0IDguMzQzMTUgNi4zNDMxNVpNMjUuNSAxMkMyNS41IDExLjE3MTYgMjQuODI4NCAxMC41IDI0IDEwLjVDMjMuMTcxNSAxMC41IDIyLjUgMTEuMTcxNiAyMi41IDEyVjE0LjVIMTkuOTk5OUMxOS4xNzE1IDE0LjUgMTguNDk5OSAxNS4xNzE2IDE4LjQ5OTkgMTZDMTguNDk5OSAxNi44Mjg0IDE5LjE3MTUgMTcuNSAxOS45OTk5IDE3LjVIMjIuNUwyMi41IDIwQzIyLjUgMjAuODI4NCAyMy4xNzE1IDIxLjUgMjQgMjEuNUMyNC44Mjg0IDIxLjUgMjUuNSAyMC44Mjg0IDI1LjUgMjBMMjUuNSAxNy41TDI3Ljk5OTkgMTcuNUMyOC44Mjg0IDE3LjUgMjkuNDk5OSAxNi44Mjg0IDI5LjQ5OTkgMTZDMjkuNDk5OSAxNS4xNzE2IDI4LjgyODQgMTQuNSAyNy45OTk5IDE0LjVIMjUuNVYxMlpNMTYgMjYuNUMxNS4xNzE2IDI2LjUgMTQuNSAyNy4xNzE2IDE0LjUgMjhDMTQuNSAyOC44Mjg0IDE1LjE3MTYgMjkuNSAxNiAyOS41SDMyQzMyLjgyODQgMjkuNSAzMy41IDI4LjgyODQgMzMuNSAyOEMzMy41IDI3LjE3MTYgMzIuODI4NCAyNi41IDMyIDI2LjVIMTZaTTE4IDM0LjVDMTcuMTcxNiAzNC41IDE2LjUgMzUuMTcxNiAxNi41IDM2QzE2LjUgMzYuODI4NCAxNy4xNzE2IDM3LjUgMTggMzcuNUgzMEMzMC44Mjg0IDM3LjUgMzEuNSAzNi44Mjg0IDMxLjUgMzZDMzEuNSAzNS4xNzE2IDMwLjgyODQgMzQuNSAzMCAzNC41SDE4WiIgZmlsbD0id2hpdGUiLz4KPC9zdmc+Cg=="
                    alt=""
                  />
                  <div className="name">Applications</div>
                </a>}
              {
                (source === 1 || source === 200 || source === 100) &&

                <a className="item bg_primary"  onClick={() =>  navigate("/account/user-management") }>
                  <img
                    className="icon"
                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTMxIDE1QzMxIDE4Ljg2NiAyNy44NjYgMjIgMjQgMjJDMjAuMTM0IDIyIDE3IDE4Ljg2NiAxNyAxNUMxNyAxMS4xMzQgMjAuMTM0IDggMjQgOEMyNy44NjYgOCAzMSAxMS4xMzQgMzEgMTVaIiBmaWxsPSJ3aGl0ZSIvPgo8cGF0aCBkPSJNMzYgMzNDMzYgMzYuODY2IDMwLjYyNzQgNDAgMjQgNDBDMTcuMzcyNiA0MCAxMiAzNi44NjYgMTIgMzNDMTIgMjkuMTM0IDE3LjM3MjYgMjYgMjQgMjZDMzAuNjI3NCAyNiAzNiAyOS4xMzQgMzYgMzNaIiBmaWxsPSJ3aGl0ZSIvPgo8cGF0aCBkPSJNMTQuMjQ0MSAxMEMxNC41OTkgMTAgMTQuOTQ1NSAxMC4wMzQ4IDE1LjI4MDEgMTAuMTAxMUMxNC40NjUgMTEuNTQ4OSAxNCAxMy4yMjAyIDE0IDE1QzE0IDE2LjczNjUgMTQuNDQyNiAxOC4zNjk2IDE1LjIyMTIgMTkuNzkyN0MxNC45MDQ5IDE5Ljg1MTcgMTQuNTc4MiAxOS44ODI1IDE0LjI0NDEgMTkuODgyNUMxMS40MTUzIDE5Ljg4MjUgOS4xMjIwNSAxNy42NzAyIDkuMTIyMDUgMTQuOTQxM0M5LjEyMjA1IDEyLjIxMjMgMTEuNDE1MyAxMCAxNC4yNDQxIDEwWiIgZmlsbD0id2hpdGUiLz4KPHBhdGggZD0iTTEwLjg5NDcgMzcuOTcxOUM5Ljc1ODg0IDM2LjYxNDIgOSAzNC45NDggOSAzM0M5IDMxLjExMTUgOS43MTMxNSAyOS40ODggMTAuNzkxNiAyOC4xNTM1QzYuOTgyMTkgMjguNDQ4OSA0IDMwLjUzMjQgNCAzMy4wNTg4QzQgMzUuNjA4OCA3LjAzNDYxIDM3LjcwNzcgMTAuODk0NyAzNy45NzE5WiIgZmlsbD0id2hpdGUiLz4KPHBhdGggZD0iTTMzLjk5OTggMTVDMzMuOTk5OCAxNi43MzY1IDMzLjU1NzIgMTguMzY5NiAzMi43Nzg2IDE5Ljc5MjdDMzMuMDk0OSAxOS44NTE3IDMzLjQyMTYgMTkuODgyNSAzMy43NTU3IDE5Ljg4MjVDMzYuNTg0NiAxOS44ODI1IDM4Ljg3NzggMTcuNjcwMiAzOC44Nzc4IDE0Ljk0MTNDMzguODc3OCAxMi4yMTIzIDM2LjU4NDYgMTAgMzMuNzU1NyAxMEMzMy40MDA4IDEwIDMzLjA1NDMgMTAuMDM0OCAzMi43MTk3IDEwLjEwMTFDMzMuNTM0OCAxMS41NDg5IDMzLjk5OTggMTMuMjIwMiAzMy45OTk4IDE1WiIgZmlsbD0id2hpdGUiLz4KPHBhdGggZD0iTTM3LjEwNTIgMzcuOTcxOUM0MC45NjUyIDM3LjcwNzcgNDMuOTk5OCAzNS42MDg4IDQzLjk5OTggMzMuMDU4OEM0My45OTk4IDMwLjUzMjQgNDEuMDE3NiAyOC40NDg5IDM3LjIwODMgMjguMTUzNUMzOC4yODY3IDI5LjQ4OCAzOC45OTk4IDMxLjExMTUgMzguOTk5OCAzM0MzOC45OTk4IDM0Ljk0OCAzOC4yNDEgMzYuNjE0MiAzNy4xMDUyIDM3Ljk3MTlaIiBmaWxsPSJ3aGl0ZSIvPgo8L3N2Zz4K"
                    alt=""
                  />
                  <div className="name">User management</div>
                </a>
              }
              {
                (source === 1 || source === 100) &&
                <a className="item bg_primary"  onClick={() =>  navigate("/account/locations") }>
                  <img
                    className="icon"
                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0yNCA0QzE1LjE2MzQgNCA4IDEyLjAwNTIgOCAyMUM4IDI5LjkyNDQgMTMuMTA2NiAzOS42MjQ4IDIxLjA3NDEgNDMuMzQ4OUMyMi45MzE1IDQ0LjIxNyAyNS4wNjg1IDQ0LjIxNyAyNi45MjU5IDQzLjM0ODlDMzQuODkzNCAzOS42MjQ4IDQwIDI5LjkyNDMgNDAgMjFDNDAgMTIuMDA1MiAzMi44MzY2IDQgMjQgNFpNMjQgMjRDMjYuMjA5MSAyNCAyOCAyMi4yMDkxIDI4IDIwQzI4IDE3Ljc5MDkgMjYuMjA5MSAxNiAyNCAxNkMyMS43OTA5IDE2IDIwIDE3Ljc5MDkgMjAgMjBDMjAgMjIuMjA5MSAyMS43OTA5IDI0IDI0IDI0WiIgZmlsbD0id2hpdGUiLz4KPC9zdmc+Cg=="
                    alt=""
                  />
                  <div className="name">Locations</div>
                </a>
              }
              {
                (source === 1 || source === 100 || source === 200 || source === 201) &&
                <a className="item bg_primary"  onClick={() =>  navigate("/account/dashboard") }>
                  <img
                    className="icon"
                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDgiIGhlaWdodD0iNDgiIHZpZXdCb3g9IjAgMCA0OCA0OCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTQgMTIuNDIxMUM0IDguNDUxMzMgNCA2LjQ2NjQ3IDUuMzE4MDIgNS4yMzMyM0M2LjYzNjA0IDQgOC43NTczNiA0IDEzIDRDMTcuMjQyNiA0IDE5LjM2NCA0IDIwLjY4MiA1LjIzMzIzQzIyIDYuNDY2NDcgMjIgOC40NTEzMyAyMiAxMi40MjExVjM1LjU3OUMyMiAzOS41NDg3IDIyIDQxLjUzMzUgMjAuNjgyIDQyLjc2NjhDMTkuMzY0IDQ0IDE3LjI0MjYgNDQgMTMgNDRDOC43NTczNiA0NCA2LjYzNjA0IDQ0IDUuMzE4MDIgNDIuNzY2OEM0IDQxLjUzMzUgNCAzOS41NDg3IDQgMzUuNTc4OVYxMi40MjExWiIgZmlsbD0id2hpdGUiLz4KPHBhdGggZD0iTTI2IDMwLjhDMjYgMjYuNjUxNiAyNiAyNC41Nzc1IDI3LjMxOCAyMy4yODg3QzI4LjYzNiAyMiAzMC43NTc0IDIyIDM1IDIyQzM5LjI0MjYgMjIgNDEuMzY0IDIyIDQyLjY4MiAyMy4yODg3QzQ0IDI0LjU3NzUgNDQgMjYuNjUxNiA0NCAzMC44VjM1LjJDNDQgMzkuMzQ4NCA0NCA0MS40MjI1IDQyLjY4MiA0Mi43MTEzQzQxLjM2NCA0NCAzOS4yNDI2IDQ0IDM1IDQ0QzMwLjc1NzQgNDQgMjguNjM2IDQ0IDI3LjMxOCA0Mi43MTEzQzI2IDQxLjQyMjUgMjYgMzkuMzQ4NCAyNiAzNS4yVjMwLjhaIiBmaWxsPSJ3aGl0ZSIvPgo8cGF0aCBkPSJNMjYgMTFDMjYgOC44MjU2MSAyNiA3LjczODQxIDI2LjM0MjUgNi44ODA4MUMyNi43OTkzIDUuNzM3MzUgMjcuNjc1MyA0LjgyODg3IDI4Ljc3NzkgNC4zNTUyM0MyOS42MDQ5IDQgMzAuNjUzMyA0IDMyLjc1IDRIMzcuMjVDMzkuMzQ2NyA0IDQwLjM5NTEgNCA0MS4yMjIxIDQuMzU1MjNDNDIuMzI0NyA0LjgyODg3IDQzLjIwMDcgNS43MzczNSA0My42NTc1IDYuODgwODFDNDQgNy43Mzg0MSA0NCA4LjgyNTYxIDQ0IDExQzQ0IDEzLjE3NDQgNDQgMTQuMjYxNiA0My42NTc1IDE1LjExOTJDNDMuMjAwNyAxNi4yNjI3IDQyLjMyNDcgMTcuMTcxMSA0MS4yMjIxIDE3LjY0NDhDNDAuMzk1MSAxOCAzOS4zNDY3IDE4IDM3LjI1IDE4SDMyLjc1QzMwLjY1MzMgMTggMjkuNjA0OSAxOCAyOC43Nzc5IDE3LjY0NDhDMjcuNjc1MyAxNy4xNzExIDI2Ljc5OTMgMTYuMjYyNyAyNi4zNDI1IDE1LjExOTJDMjYgMTQuMjYxNiAyNiAxMy4xNzQ0IDI2IDExWiIgZmlsbD0id2hpdGUiLz4KPC9zdmc+Cg=="
                    alt=""
                  />
                  <div className="name">Dashboard</div>
                </a>
              }

              {
                (source === 200) &&
                <a className="item bg_primary"  onClick={() =>  navigate("/account/billing") }>
                  <img
                    className="icon"
                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzMiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAzMyAzMiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik02LjA2MjEgNC4yMjg3MkM0LjUgNS43OTA4MiA0LjUgOC4zMDQ5OCA0LjUgMTMuMzMzM1YxOC42NjY2QzQuNSAyMy42OTQ5IDQuNSAyNi4yMDkxIDYuMDYyMSAyNy43NzEyQzcuNjI0MTkgMjkuMzMzMyAxMC4xMzg0IDI5LjMzMzMgMTUuMTY2NyAyOS4zMzMzSDE3LjgzMzNDMjIuODYxNiAyOS4zMzMzIDI1LjM3NTggMjkuMzMzMyAyNi45Mzc5IDI3Ljc3MTJDMjguNSAyNi4yMDkxIDI4LjUgMjMuNjk0OSAyOC41IDE4LjY2NjZWMTMuMzMzM0MyOC41IDguMzA0OTggMjguNSA1Ljc5MDgyIDI2LjkzNzkgNC4yMjg3MkMyNS4zNzU4IDIuNjY2NjMgMjIuODYxNiAyLjY2NjYzIDE3LjgzMzMgMi42NjY2M0gxNS4xNjY3QzEwLjEzODQgMi42NjY2MyA3LjYyNDE5IDIuNjY2NjMgNi4wNjIxIDQuMjI4NzJaTTEwLjE2NjcgMTAuNjY2NkMxMC4xNjY3IDEwLjExNDMgMTAuNjE0NCA5LjY2NjYzIDExLjE2NjcgOS42NjY2M0gyMS44MzMzQzIyLjM4NTYgOS42NjY2MyAyMi44MzMzIDEwLjExNDMgMjIuODMzMyAxMC42NjY2QzIyLjgzMzMgMTEuMjE4OSAyMi4zODU2IDExLjY2NjYgMjEuODMzMyAxMS42NjY2SDExLjE2NjdDMTAuNjE0NCAxMS42NjY2IDEwLjE2NjcgMTEuMjE4OSAxMC4xNjY3IDEwLjY2NjZaTTEwLjE2NjcgMTZDMTAuMTY2NyAxNS40NDc3IDEwLjYxNDQgMTUgMTEuMTY2NyAxNUgyMS44MzMzQzIyLjM4NTYgMTUgMjIuODMzMyAxNS40NDc3IDIyLjgzMzMgMTZDMjIuODMzMyAxNi41NTIyIDIyLjM4NTYgMTcgMjEuODMzMyAxN0gxMS4xNjY3QzEwLjYxNDQgMTcgMTAuMTY2NyAxNi41NTIyIDEwLjE2NjcgMTZaTTExLjE2NjcgMjAuMzMzM0MxMC42MTQ0IDIwLjMzMzMgMTAuMTY2NyAyMC43ODEgMTAuMTY2NyAyMS4zMzMzQzEwLjE2NjcgMjEuODg1NiAxMC42MTQ0IDIyLjMzMzMgMTEuMTY2NyAyMi4zMzMzSDE3LjgzMzNDMTguMzg1NiAyMi4zMzMzIDE4LjgzMzMgMjEuODg1NiAxOC44MzMzIDIxLjMzMzNDMTguODMzMyAyMC43ODEgMTguMzg1NiAyMC4zMzMzIDE3LjgzMzMgMjAuMzMzM0gxMS4xNjY3WiIgZmlsbD0id2hpdGUiLz4KPC9zdmc+Cg=="
                    alt=""
                  />
                  <div className="name">Billing</div>
                </a>
              }



            </div>
          </div>
        </div>
      </div>
    </>

  );
};

Home.propTypes = {};

export default Home;
