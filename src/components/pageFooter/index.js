
import logo from '../../assets/logo-1.svg'
const PageFooter =(props)=>{

    return (
        <div className="footer_patient">
            <div className="container">
              <div className="row justify_between mx_0">
                <div className="footer_logo">
                  <img
                    className="logo" style={{width:"120px"}}
                    src={logo}
                    alt=""
                  />
                </div>
                <div className="">
                  <a className="link link_white" href="">
                    About us
                  </a>
                  <a className="link link_white" href="">
                    Terms of Service
                  </a>
                  <a className="link link_white" href="">
                    Privacy policy
                  </a>
                  <a className="link link_white" href="">
                    Term &amp; Conditions
                  </a>
                </div>
                <div className="">
                  <a className="link link_white" href="">
                    Advertisement Disclosure
                  </a>
                  <a className="link link_white" href="">
                    Help Center
                  </a>
                  <a className="link link_white" href="">
                    Cookies Settings
                  </a>
                </div>
              </div>
            </div>
          </div>
    )
}

export default PageFooter;