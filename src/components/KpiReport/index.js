import active_user from "../../assets/active.png"
import avage from "../../assets/avage.png"
import cardUser from "../../assets/card.png"
import check from "../../assets/check.png"
import counter from "../../assets/count.png"
import dallar from "../../assets/dollar.png"
import file_add from "../../assets/file.png"
import location from "../../assets/location.png"
import person from "../../assets/person.png"
import radio from "../../assets/radio.png"
import upper from "../../assets/up.png"


const KpiExport=(props)=>{

    return (
        <>
             <div className="filter_box mt_2">
              <button className="btn btn_filter border btn_md d_none d_inline_block_sm ">
                <img
                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0zLjMwMTY4IDEuNUMzLjMxMjA5IDEuNSAzLjMyMjUzIDEuNSAzLjMzMzAxIDEuNUwxMi42OTc3IDEuNUMxMy4xNDIzIDEuNDk5OTggMTMuNTIyNCAxLjQ5OTk2IDEzLjgyNjIgMS41MzgyOEMxNC4xNDg0IDEuNTc4OTIgMTQuNDU5OCAxLjY3MDIgMTQuNzE1NiAxLjkxMDE2QzE0Ljk3NTggMi4xNTQzNiAxNS4wNzkzIDIuNDU5MDQgMTUuMTI0NyAyLjc3NjAzQzE1LjE2NjQgMy4wNjcyMyAxNS4xNjY0IDMuNDI5MzEgMTUuMTY2MyAzLjg0MjM5TDE1LjE2NjMgNC4zNjAwOEMxNS4xNjY0IDQuNjg1NzUgMTUuMTY2NCA0Ljk2NzMgMTUuMTQyIDUuMjAyMDdDMTUuMTE1OSA1LjQ1NDcxIDE1LjA1ODcgNS42OTE3MyAxNC45MjE0IDUuOTE5MjlDMTQuNzg1MSA2LjE0NTEzIDE0LjYwMTUgNi4zMDkwNyAxNC4zOSA2LjQ1NjAyQzE0LjE5MDcgNi41OTQ0NiAxMy45MzYgNi43Mzc4IDEzLjYzNjYgNi45MDYzN0wxMS42NzQ5IDguMDEwNjNDMTEuMjI4NCA4LjI2MTk5IDExLjA3MjkgOC4zNTI1IDEwLjk2OTEgOC40NDI2MkMxMC43MzA3IDguNjQ5NTggMTAuNTk0MiA4Ljg3OTIxIDEwLjUzMDEgOS4xNjY5MkMxMC41MDI2IDkuMjg5OTEgMTAuNDk5NyA5LjQ0NDgxIDEwLjQ5OTcgOS45MTUyOUwxMC40OTk3IDExLjczNjdDMTAuNDk5NyAxMi4zMzc1IDEwLjQ5OTcgMTIuODQ3NiAxMC40Mzc5IDEzLjIzOTdDMTAuMzcyMiAxMy42NTY3IDEwLjIxOTUgMTQuMDU2NiA5LjgxOTUyIDE0LjMwNjhDOS40Mjg2MiAxNC41NTEzIDguOTk4MDEgMTQuNTI4OSA4LjU3OTU0IDE0LjQyOTVDOC4xNzY1MSAxNC4zMzM4IDcuNjc5ODUgMTQuMTM5NiA3LjA4MzkxIDEzLjkwNjZMNy4wMjU5OSAxMy44ODRDNi43NDY4NCAxMy43NzQ5IDYuNTAyNDEgMTMuNjc5MyA2LjMwODkxIDEzLjU3OTRDNi4xMDA5NCAxMy40NzIgNS45MDc4MSAxMy4zMzg0IDUuNzYwMDQgMTMuMTMwNUM1LjYxMDYyIDEyLjkyMDIgNS41NTA3MSAxMi42OTQ3IDUuNTI0IDEyLjQ2NDJDNS40OTk2MyAxMi4yNTQgNS40OTk2NSAxMi4wMDE4IDUuNDk5NjggMTEuNzIwNEw1LjQ5OTY4IDkuOTE1MjlDNS40OTk2OCA5LjQ0NDgxIDUuNDk2NzEgOS4yODk5MSA1LjQ2OTI5IDkuMTY2OTJDNS40MDUxNCA4Ljg3OTIxIDUuMjY4NjIgOC42NDk1OCA1LjAzMDIzIDguNDQyNjJDNC45MjY0MyA4LjM1MjUgNC43NzA5NSA4LjI2MTk5IDQuMzI0NDMgOC4wMTA2M0wyLjM2MjgxIDYuOTA2MzdDMi4wNjMzMyA2LjczNzggMS44MDg3IDYuNTk0NDYgMS42MDk0IDYuNDU2MDJDMS4zOTc4NCA2LjMwOTA3IDEuMjE0MjggNi4xNDUxMyAxLjA3Nzk5IDUuOTE5MjlDMC45NDA2NjQgNS42OTE3MyAwLjg4MzQ3OSA1LjQ1NDcxIDAuODU3MzA2IDUuMjAyMDdDMC44MzI5ODQgNC45NjczIDAuODMyOTk2IDQuNjg1NzUgMC44MzMwMSA0LjM2MDA4TDAuODMzMDExIDMuODc2NDRDMC44MzMwMTEgMy44NjUwNSAwLjgzMzAxIDMuODUzNjkgMC44MzMwMSAzLjg0MjM4QzAuODMyOTc4IDMuNDI5MzEgMC44MzI5NTEgMy4wNjcyMyAwLjg3NDY3OSAyLjc3NjAzQzAuOTIwMTA0IDIuNDU5MDQgMS4wMjM1MSAyLjE1NDM2IDEuMjgzNzkgMS45MTAxNkMxLjUzOTU2IDEuNjcwMiAxLjg1MDk3IDEuNTc4OTIgMi4xNzMxNiAxLjUzODI4QzIuNDc2OTYgMS40OTk5NiAyLjg1NzA1IDEuNDk5OTggMy4zMDE2OCAxLjVaTTIuMjk4MzEgMi41MzA0MkMyLjA3NTg4IDIuNTU4NDggMi4wMDUxNCAyLjYwNDYxIDEuOTY4MDEgMi42Mzk0NEMxLjkzNTQgMi42NzAwNCAxLjg5MjM2IDIuNzIzOSAxLjg2NDU3IDIuOTE3ODlDMS44MzQyMSAzLjEyOTcgMS44MzMwMSAzLjQxOTEyIDEuODMzMDEgMy44NzY0NFY0LjMzNjMyQzEuODMzMDEgNC42OTI0NiAxLjgzMzYzIDQuOTIxODUgMS44NTE5OCA1LjA5OTAzQzEuODY5MDggNS4yNjQwNCAxLjg5ODQyIDUuMzQzMzggMS45MzQxNyA1LjQwMjYxQzEuOTcwOTQgNS40NjM1NSAyLjAzMjAxIDUuNTMyIDIuMTc5OSA1LjYzNDczQzIuMzM1NzEgNS43NDI5NiAyLjU0ODU4IDUuODYzMzkgMi44NzI5NiA2LjA0NTk5TDQuODE0OTggNy4xMzkyMkM0LjgzMzE2IDcuMTQ5NDUgNC44NTEwOCA3LjE1OTUzIDQuODY4NzIgNy4xNjk0NkM1LjI0MTE5IDcuMzc5IDUuNDk0ODYgNy41MjE3MiA1LjY4NTgxIDcuNjg3NDlDNi4wODAwNSA4LjAyOTc1IDYuMzMyOTkgOC40NDU0OCA2LjQ0NTMyIDguOTQ5M0M2LjQ5OTkyIDkuMTk0MTcgNi40OTk4MiA5LjQ2ODc0IDYuNDk5NjkgOS44NTYzMkM2LjQ5OTY4IDkuODc1NjkgNi40OTk2OCA5Ljg5NTM0IDYuNDk5NjggOS45MTUyOVYxMS42OTQ5QzYuNDk5NjggMTIuMDA5NyA2LjUwMDQ1IDEyLjIwMzMgNi41MTczNSAxMi4zNDkxQzYuNTMyNjIgMTIuNDgwOSA2LjU1NjY5IDEyLjUyNTIgNi41NzUyMSAxMi41NTEyQzYuNTk1NCAxMi41Nzk3IDYuNjM1MzkgMTIuNjIyNSA2Ljc2NzcgMTIuNjkwOUM2LjkwOTMxIDEyLjc2NCA3LjEwNDQgMTIuODQwOSA3LjQxMjQ5IDEyLjk2MTRDOC4wNTMxNiAxMy4yMTE4IDguNDg0MSAxMy4zNzkgOC44MTA2MiAxMy40NTY2QzkuMTI5NjkgMTMuNTMyMyA5LjIzNDgzIDEzLjQ5MyA5LjI4OTIzIDEzLjQ1OUM5LjMzNDUxIDEzLjQzMDcgOS40MDQ1MyAxMy4zNzMxIDkuNDUwMTIgMTMuMDg0QzkuNDk4MTkgMTIuNzc5MSA5LjQ5OTY4IDEyLjM0OSA5LjQ5OTY4IDExLjY5NDlWOS45MTUyOUM5LjQ5OTY4IDkuODk1MzUgOS40OTk2NyA5Ljg3NTcgOS40OTk2NiA5Ljg1NjMzQzkuNDk5NTMgOS40Njg3NSA5LjQ5OTQ0IDkuMTk0MTcgOS41NTQwMyA4Ljk0OTNDOS42NjYzNyA4LjQ0NTQ3IDkuOTE5MzEgOC4wMjk3NSAxMC4zMTM1IDcuNjg3NDlDMTAuNTA0NSA3LjUyMTcyIDEwLjc1ODIgNy4zNzkwMSAxMS4xMzA2IDcuMTY5NDdDMTEuMTQ4MyA3LjE1OTU0IDExLjE2NjIgNy4xNDk0NiAxMS4xODQ0IDcuMTM5MjFMMTMuMTI2NCA2LjA0NTk5QzEzLjQ1MDggNS44NjMzOSAxMy42NjM2IDUuNzQyOTYgMTMuODE5NSA1LjYzNDczQzEzLjk2NzMgNS41MzIgMTQuMDI4NCA1LjQ2MzU1IDE0LjA2NTIgNS40MDI2MUMxNC4xMDA5IDUuMzQzMzggMTQuMTMwMyA1LjI2NDA0IDE0LjE0NzQgNS4wOTkwM0MxNC4xNjU3IDQuOTIxODUgMTQuMTY2MyA0LjY5MjQ2IDE0LjE2NjMgNC4zMzYzMlYzLjg3NjQ0QzE0LjE2NjMgMy40MTkxMiAxNC4xNjUxIDMuMTI5NyAxNC4xMzQ4IDIuOTE3ODlDMTQuMTA3IDIuNzIzOSAxNC4wNjQgMi42NzAwNCAxNC4wMzEzIDIuNjM5NDRDMTMuOTk0MiAyLjYwNDYxIDEzLjkyMzUgMi41NTg0OCAxMy43MDEgMi41MzA0MkMxMy40NjczIDIuNTAwOTQgMTMuMTUxIDIuNSAxMi42NjYzIDIuNUgzLjMzMzAxQzIuODQ4MzIgMi41IDIuNTMyMDQgMi41MDA5NCAyLjI5ODMxIDIuNTMwNDJaIiBmaWxsPSIjNzk3ODc4Ii8+Cjwvc3ZnPgo="
                  alt=""
                />
                <span className="d_none d_inline_block_sm">Filters</span>
              </button>
              <div className="search_bar ms_auto ">
                <select className="form_control">
                  <option  style={{padding:'12px 0'}}>One Week</option>
                  <option  style={{padding:'12px 0'}}>One Month</option>
                  <option  style={{padding:'12px 0'}}>One Year</option>
                  <option  style={{padding:'12px 0'}}>Max</option>
                </select>
              
             
              </div>
            </div>
            <div className="dash_kpi_date">
                  Through   7/11/2023
                </div>
            <ul className="home_data mt_3 d_none_sm">
              <li className="item border">
                <div className="count color_balck_E">12</div>
                {/* <div className="name">applications</div> */}
              </li>
              <li className="item  border ">
                <div className="count color_balck_E">32</div>
                {/* <div className="name">pre-approvals</div> */}
              </li>
              <li className="item border " >
                <div className="count color_balck_E">56</div>
                {/* <div className="name">leads</div> */}
              </li>
            </ul>
            <div className="d_flex fs_lg d_none_sm">
                 <div className="name col t_center">Active Users</div>
                 <div className="name col t_center">Active Users</div>
                 <div className="name col t_center">Locations</div>
            </div>
            <div className="filter_box mt_3 btn_md d_none_sm ">
              <button className="btn btn_filter  " style={{height:'40px'}}>
                <img
                  src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0zLjMwMTY4IDEuNUMzLjMxMjA5IDEuNSAzLjMyMjUzIDEuNSAzLjMzMzAxIDEuNUwxMi42OTc3IDEuNUMxMy4xNDIzIDEuNDk5OTggMTMuNTIyNCAxLjQ5OTk2IDEzLjgyNjIgMS41MzgyOEMxNC4xNDg0IDEuNTc4OTIgMTQuNDU5OCAxLjY3MDIgMTQuNzE1NiAxLjkxMDE2QzE0Ljk3NTggMi4xNTQzNiAxNS4wNzkzIDIuNDU5MDQgMTUuMTI0NyAyLjc3NjAzQzE1LjE2NjQgMy4wNjcyMyAxNS4xNjY0IDMuNDI5MzEgMTUuMTY2MyAzLjg0MjM5TDE1LjE2NjMgNC4zNjAwOEMxNS4xNjY0IDQuNjg1NzUgMTUuMTY2NCA0Ljk2NzMgMTUuMTQyIDUuMjAyMDdDMTUuMTE1OSA1LjQ1NDcxIDE1LjA1ODcgNS42OTE3MyAxNC45MjE0IDUuOTE5MjlDMTQuNzg1MSA2LjE0NTEzIDE0LjYwMTUgNi4zMDkwNyAxNC4zOSA2LjQ1NjAyQzE0LjE5MDcgNi41OTQ0NiAxMy45MzYgNi43Mzc4IDEzLjYzNjYgNi45MDYzN0wxMS42NzQ5IDguMDEwNjNDMTEuMjI4NCA4LjI2MTk5IDExLjA3MjkgOC4zNTI1IDEwLjk2OTEgOC40NDI2MkMxMC43MzA3IDguNjQ5NTggMTAuNTk0MiA4Ljg3OTIxIDEwLjUzMDEgOS4xNjY5MkMxMC41MDI2IDkuMjg5OTEgMTAuNDk5NyA5LjQ0NDgxIDEwLjQ5OTcgOS45MTUyOUwxMC40OTk3IDExLjczNjdDMTAuNDk5NyAxMi4zMzc1IDEwLjQ5OTcgMTIuODQ3NiAxMC40Mzc5IDEzLjIzOTdDMTAuMzcyMiAxMy42NTY3IDEwLjIxOTUgMTQuMDU2NiA5LjgxOTUyIDE0LjMwNjhDOS40Mjg2MiAxNC41NTEzIDguOTk4MDEgMTQuNTI4OSA4LjU3OTU0IDE0LjQyOTVDOC4xNzY1MSAxNC4zMzM4IDcuNjc5ODUgMTQuMTM5NiA3LjA4MzkxIDEzLjkwNjZMNy4wMjU5OSAxMy44ODRDNi43NDY4NCAxMy43NzQ5IDYuNTAyNDEgMTMuNjc5MyA2LjMwODkxIDEzLjU3OTRDNi4xMDA5NCAxMy40NzIgNS45MDc4MSAxMy4zMzg0IDUuNzYwMDQgMTMuMTMwNUM1LjYxMDYyIDEyLjkyMDIgNS41NTA3MSAxMi42OTQ3IDUuNTI0IDEyLjQ2NDJDNS40OTk2MyAxMi4yNTQgNS40OTk2NSAxMi4wMDE4IDUuNDk5NjggMTEuNzIwNEw1LjQ5OTY4IDkuOTE1MjlDNS40OTk2OCA5LjQ0NDgxIDUuNDk2NzEgOS4yODk5MSA1LjQ2OTI5IDkuMTY2OTJDNS40MDUxNCA4Ljg3OTIxIDUuMjY4NjIgOC42NDk1OCA1LjAzMDIzIDguNDQyNjJDNC45MjY0MyA4LjM1MjUgNC43NzA5NSA4LjI2MTk5IDQuMzI0NDMgOC4wMTA2M0wyLjM2MjgxIDYuOTA2MzdDMi4wNjMzMyA2LjczNzggMS44MDg3IDYuNTk0NDYgMS42MDk0IDYuNDU2MDJDMS4zOTc4NCA2LjMwOTA3IDEuMjE0MjggNi4xNDUxMyAxLjA3Nzk5IDUuOTE5MjlDMC45NDA2NjQgNS42OTE3MyAwLjg4MzQ3OSA1LjQ1NDcxIDAuODU3MzA2IDUuMjAyMDdDMC44MzI5ODQgNC45NjczIDAuODMyOTk2IDQuNjg1NzUgMC44MzMwMSA0LjM2MDA4TDAuODMzMDExIDMuODc2NDRDMC44MzMwMTEgMy44NjUwNSAwLjgzMzAxIDMuODUzNjkgMC44MzMwMSAzLjg0MjM4QzAuODMyOTc4IDMuNDI5MzEgMC44MzI5NTEgMy4wNjcyMyAwLjg3NDY3OSAyLjc3NjAzQzAuOTIwMTA0IDIuNDU5MDQgMS4wMjM1MSAyLjE1NDM2IDEuMjgzNzkgMS45MTAxNkMxLjUzOTU2IDEuNjcwMiAxLjg1MDk3IDEuNTc4OTIgMi4xNzMxNiAxLjUzODI4QzIuNDc2OTYgMS40OTk5NiAyLjg1NzA1IDEuNDk5OTggMy4zMDE2OCAxLjVaTTIuMjk4MzEgMi41MzA0MkMyLjA3NTg4IDIuNTU4NDggMi4wMDUxNCAyLjYwNDYxIDEuOTY4MDEgMi42Mzk0NEMxLjkzNTQgMi42NzAwNCAxLjg5MjM2IDIuNzIzOSAxLjg2NDU3IDIuOTE3ODlDMS44MzQyMSAzLjEyOTcgMS44MzMwMSAzLjQxOTEyIDEuODMzMDEgMy44NzY0NFY0LjMzNjMyQzEuODMzMDEgNC42OTI0NiAxLjgzMzYzIDQuOTIxODUgMS44NTE5OCA1LjA5OTAzQzEuODY5MDggNS4yNjQwNCAxLjg5ODQyIDUuMzQzMzggMS45MzQxNyA1LjQwMjYxQzEuOTcwOTQgNS40NjM1NSAyLjAzMjAxIDUuNTMyIDIuMTc5OSA1LjYzNDczQzIuMzM1NzEgNS43NDI5NiAyLjU0ODU4IDUuODYzMzkgMi44NzI5NiA2LjA0NTk5TDQuODE0OTggNy4xMzkyMkM0LjgzMzE2IDcuMTQ5NDUgNC44NTEwOCA3LjE1OTUzIDQuODY4NzIgNy4xNjk0NkM1LjI0MTE5IDcuMzc5IDUuNDk0ODYgNy41MjE3MiA1LjY4NTgxIDcuNjg3NDlDNi4wODAwNSA4LjAyOTc1IDYuMzMyOTkgOC40NDU0OCA2LjQ0NTMyIDguOTQ5M0M2LjQ5OTkyIDkuMTk0MTcgNi40OTk4MiA5LjQ2ODc0IDYuNDk5NjkgOS44NTYzMkM2LjQ5OTY4IDkuODc1NjkgNi40OTk2OCA5Ljg5NTM0IDYuNDk5NjggOS45MTUyOVYxMS42OTQ5QzYuNDk5NjggMTIuMDA5NyA2LjUwMDQ1IDEyLjIwMzMgNi41MTczNSAxMi4zNDkxQzYuNTMyNjIgMTIuNDgwOSA2LjU1NjY5IDEyLjUyNTIgNi41NzUyMSAxMi41NTEyQzYuNTk1NCAxMi41Nzk3IDYuNjM1MzkgMTIuNjIyNSA2Ljc2NzcgMTIuNjkwOUM2LjkwOTMxIDEyLjc2NCA3LjEwNDQgMTIuODQwOSA3LjQxMjQ5IDEyLjk2MTRDOC4wNTMxNiAxMy4yMTE4IDguNDg0MSAxMy4zNzkgOC44MTA2MiAxMy40NTY2QzkuMTI5NjkgMTMuNTMyMyA5LjIzNDgzIDEzLjQ5MyA5LjI4OTIzIDEzLjQ1OUM5LjMzNDUxIDEzLjQzMDcgOS40MDQ1MyAxMy4zNzMxIDkuNDUwMTIgMTMuMDg0QzkuNDk4MTkgMTIuNzc5MSA5LjQ5OTY4IDEyLjM0OSA5LjQ5OTY4IDExLjY5NDlWOS45MTUyOUM5LjQ5OTY4IDkuODk1MzUgOS40OTk2NyA5Ljg3NTcgOS40OTk2NiA5Ljg1NjMzQzkuNDk5NTMgOS40Njg3NSA5LjQ5OTQ0IDkuMTk0MTcgOS41NTQwMyA4Ljk0OTNDOS42NjYzNyA4LjQ0NTQ3IDkuOTE5MzEgOC4wMjk3NSAxMC4zMTM1IDcuNjg3NDlDMTAuNTA0NSA3LjUyMTcyIDEwLjc1ODIgNy4zNzkwMSAxMS4xMzA2IDcuMTY5NDdDMTEuMTQ4MyA3LjE1OTU0IDExLjE2NjIgNy4xNDk0NiAxMS4xODQ0IDcuMTM5MjFMMTMuMTI2NCA2LjA0NTk5QzEzLjQ1MDggNS44NjMzOSAxMy42NjM2IDUuNzQyOTYgMTMuODE5NSA1LjYzNDczQzEzLjk2NzMgNS41MzIgMTQuMDI4NCA1LjQ2MzU1IDE0LjA2NTIgNS40MDI2MUMxNC4xMDA5IDUuMzQzMzggMTQuMTMwMyA1LjI2NDA0IDE0LjE0NzQgNS4wOTkwM0MxNC4xNjU3IDQuOTIxODUgMTQuMTY2MyA0LjY5MjQ2IDE0LjE2NjMgNC4zMzYzMlYzLjg3NjQ0QzE0LjE2NjMgMy40MTkxMiAxNC4xNjUxIDMuMTI5NyAxNC4xMzQ4IDIuOTE3ODlDMTQuMTA3IDIuNzIzOSAxNC4wNjQgMi42NzAwNCAxNC4wMzEzIDIuNjM5NDRDMTMuOTk0MiAyLjYwNDYxIDEzLjkyMzUgMi41NTg0OCAxMy43MDEgMi41MzA0MkMxMy40NjczIDIuNTAwOTQgMTMuMTUxIDIuNSAxMi42NjYzIDIuNUgzLjMzMzAxQzIuODQ4MzIgMi41IDIuNTMyMDQgMi41MDA5NCAyLjI5ODMxIDIuNTMwNDJaIiBmaWxsPSIjNzk3ODc4Ii8+Cjwvc3ZnPgo="
                  alt=""
                />
                <span>Filters</span>
              </button>
              <div className="search_bar ms_auto ">
                <select className="form_control">
                  <option  style={{padding:'12px 0'}}>Submitted</option>
                  <option  style={{padding:'12px 0'}}>Pre-approved</option>
                  <option  style={{padding:'12px 0'}}>Leads</option>
                </select>
              
             
              </div>
            </div>
            <div className="kpi_container  d_flex mt_4">
                <div className="d_flex d_flx_1_3 bg_fff  left_container">
                <div className="col wd_sm">
                    <div className="ht_64"></div>
                    <div className="t_center mt_5">
                        <img src={counter} className="dash_img_1"></img>
                    </div>
                    <div className="t_center mt_5"> <img src={avage} className="dash_img_2"></img></div>
                    <div className="t_center mt_5"> <img src={radio} className="dash_img_1"></img></div>
                    <div className="t_center mt_5"> <img src={upper} className="dash_img_2"></img></div>
                    <div className="t_center mt_5"> <img src={dallar} className="dash_img_2"></img></div>
                </div>
                     <div className="col d_none d_inline_block_sm">
                         <div className="t_center "> <img src={file_add} className="dash_img_2"></img></div>
                         <div className="ht_64 t_center mt_5 kpi_ft">300</div>
                         <div className="ht_64 t_center mt_5 kpi_ft">30%</div>
                         <div className="ht_64 t_center mt_5 kpi_ft">$3,000</div>
                         <div className="ht_64 t_center mt_5 kpi_ft">$200</div>
                         <div className="ht_64 t_center mt_5 kpi_ft">30%</div>
                    
                    </div>
                <div className="col d_none d_inline_block_sm">
                        <div className="t_center "> <img src={check} className="dash_img_2"></img></div>
                        <div className="ht_64 t_center mt_5 kpi_ft">307</div>
                        <div className="ht_64 t_center mt_5 kpi_ft">30%</div>
                        <div className="ht_64 t_center mt_5 kpi_ft">$33,300</div>
                        <div className="ht_64 t_center mt_5 kpi_ft">$300</div>
                        <div className="ht_64 t_center mt_5 kpi_ft">10%</div>
                </div>
                <div className="col wd_sm_r">
                    <div className="t_center "> <img src={person} className="dash_img_2"></img></div>
                    <div className="ht_64 t_center mt_5 kpi_ft">300</div>
                    <div className="ht_64 t_center mt_5 kpi_ft">30</div>
                    <div className="ht_64 t_center mt_5 kpi_ft">$3,000</div>
                    <div className="ht_64 t_center mt_5 kpi_ft">$240</div>
                    <div className="ht_64 t_center mt_5 kpi_ft">19%</div>
                </div>
                </div>
                <div className="bg_100_p d_none d_inline_block_sm d_flx_1_1 ms_4 kpi_right_content">
                     <div className="item_sx">
                        <img src={cardUser}></img>
                        <div className="fs_sm">Active Users</div>
                        <div className="fw_bold fs_lg mt_1">25</div>
                     </div>
                    <div className="item_sx mt_6">
                    <img src={active_user}></img>
                       <div className="fs_sm"> Active HCG</div>
                        <div className="fw_bold fs_lg mt_1">25</div>
                    </div>
                    <div className="item_sx mt_6">
                    <img src={location}></img>
                    <div className="fs_sm">Locations</div>
                        <div className="fw_bold fs_lg mt_1">25</div>
                    </div>
                </div>

            </div>


            <button className="btn btn_primary  col mt_3" >KPI Reporting</button>

        </>
    )
}

export default KpiExport;