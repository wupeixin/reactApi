import React, { useEffect, useState } from "react";

import $ from "jquery"
import bank from '../../assets/bank.png'
import sponsore from '../../assets/sponsored.png'
import first from '../../assets/first.png'
import bouns from '../../assets/bouns.png'
import offer from '../../assets/offered.png'
import { sendPost, sendPostByJson, sendPostByKeyValue } from "../../utils/httpUtils";


const LenderItem=(props)=>{
  
  const [list,setList]=useState([1,2])

  useEffect(()=>{

  },[])
  //get lender list
  const getLenders=async()=>{
    const url=""
    const data={}
    const res= await sendPostByKeyValue(url,{})
    if(res.data){

    }
  }
  const getImgStatus=(arg)=>{
    let imgsrc=""
    switch (arg) {
      case 1:
        imgsrc=""
        break;
    
      default:
        break;
    }
    return imgsrc
  }
  const clickApply = async() => {
    const url=""
    const data={}
    const res= await sendPostByJson(url,{})
    if(res.data){

    }
    props.showApply(true)
   
    
  };
    return(
      <ul className="mt_5">
        {
          list.map((v,i)=>{
            return (
              <li className="card card_patient" key={i}>
              <div className="tag">
                <img
                  src={sponsore}
                  alt=""
                />
              </div>
              <div className="d_flex_sm justify_between align_center mt_4_sm mt_5_md">
                <div className="top mt_2 mt_0_sm">
                  <div className="img_box me_3 me_0_sm">
                    <img src={bank} alt="" />
                  </div>
                  <div className="mt_2_sm">
                    <div className="name">Andhra Bank</div>
                    <div className="mt_2 mt_1_sm">
                      Rated <span className="num fs_lg">8.2</span>/10
                    </div>
                  </div>
                </div>
                <div className="lh w_100 d_none_sm mt_3" />
                <div className="total my_3 d_none_sm">
                  Monthly payment <span className="amount ms_3">$650</span>
                </div>
                <div className="lv d_none d_block_sm"></div>
                <div className="t_center">
                  <div className="my_3 my_4_md">
                    Financing type:{" "}
                    <span className="fw_medium fs_lg">Loan</span>
                  </div>
                  <div className="my_3 my_4_md">
                    Financing amount:{" "}
                    <span className="fw_medium fs_lg">$650</span>
                  </div>
                  <div className="my_3 my_4_md">
                    Interest rate: <span className="fw_medium fs_lg">12%</span>
                  </div>
                </div>
                <div className="lv d_none d_block_sm"></div>
                <div className="total my_3 d_none d_flex_sm">
                  Monthly payment{" "}
                  <span className="amount ms_3 ms_4_md">$650</span>
                </div>
              </div>
              <div className="lh d_none d_block_sm mt_2 mt_3_md" />
              <button className="btn btn_primary mt_3 mb_2 mx_auto mt_3_sm mb_3_md" onClick={() => clickApply()}>
                Apply
              </button>
            </li>
            )
          })
        }
  
      </ul>
    )
}
export default LenderItem