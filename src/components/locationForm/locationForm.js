import React, { useState,useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";


import step_1 from '../../assets/step_1.png'
import step_2 from '../../assets/step_2.png'
import step_3 from '../../assets/step_3.png'
import step_4 from '../../assets/step_4.png'
import step_5 from '../../assets/step_5.png'
import step_6 from '../../assets/step_6.png'
import step_7 from '../../assets/step_7.png'


const LocationForm = (props) => {
    const navigate = useNavigate();
    const [id,setId]=useState("")
    const [stepImg,setStepImg]=useState(step_1)
    let [count, setCount] = useState(1)
    const [locationInfo,setLocationInfo]=useState({
        id:"",
        corporateName:"",
        associatedNumber:"",
        dbaName:"",
        taxID:"",
        ownershipType:"",
        employeesCount:"",
        businessDateStarted:"",
        businessType:"",
        managementPractice:"",
        officePhone:"",
        officeWebsite:"",
        faxNumber:'',  
    })
    const [physicalAddress,setPhysicalAddress]=useState({
        addressLine1:"",
        addressLine2:"",
        city:"",
        state:"",
        zipCode:"",

    })
    const [mailing,setMailing]=useState({
        addressLine1:"",
        addressLine2:"",
        city:"",
        state:"",
        zipCode:"",   
    })
    const [owner,setOwner]=useState({
        firstName:"",
        lastName:"",
        medicalNumber:"",
        addressLine1:"",
        addressLine2:"",
        city:"",
        state:"",
        zipCode:"", 
        phoneNumber:"",
        email:"",

    })
    const [managingDoctor,setManagingDoctor]=useState({
        firstName:"",
        lastName:"",
        medicalLicense:"",
        medicalLicenseState:""
    })
    const [concate,setConcate]=useState({
        firstName:"",
        lastName:"",
        phoneNumber:"",
        email:""
    })
    const [bankInfo,setBankInfo]=useState({
        banktName:"",
        routingNumber:"",
        accountNumber:"",
        bankLatter:""
    })
    useEffect(()=>{
        if(props.id){
            setId(props.lid)
        }
       

    },[])
    const nextStep = (e) => {
        //next step check some validate
        if(count==1){
               //
        }else if(count==2){

        }else if(count==3){

        }else if(count==4){

        }else if(count==5){

        }else if(count==6){

        }
        count++;
         
        setCount(count)
        switch (count) {
            case 2:
                setStepImg(step_2)
                break;
            case 3:
                setStepImg(step_3)
                break;
            case 4:
                setStepImg(step_4)
              
                break
            case 5:
                setStepImg(step_5)
                break
            case 6:
                setStepImg(step_6)
              
                break
            case 7:
                setStepImg(step_7)
               
                break
            default: 
            setCount(1)
            setStepImg(step_1)
            ;
        }
  


    }
    const lastStep = async () => {
        
        count--;
        setCount(count)
        switch (count) {
            case 1:
                setStepImg(step_1)
                break;
            case 2:
                setStepImg(step_2)
                break
            case 3:
                setStepImg(step_3)
                break
            case 4:
                setStepImg(step_4)
                break
            case 5:
                setStepImg(step_5)

                break
            case 6:
                setStepImg(step_6)

                break
            default: 
            setStepImg(step_1)
            setCount(1)
            ;
        }
    }
    const add = async () => {
       //chek count==7

        navigate("/account/locations")

    }
    const saveData=async ()=>{
        let url=""
        let data={
            ...locationInfo,
            ...physicalAddress,
            ...mailing,
            ...owner,
            ...managingDoctor,
            ...concate,
            ...bankInfo
        }
        try {
            
            const res= await sendPostByJson(url,data)
            if(res.data){

            }
        } catch (error) {
            
        }

    }
    const getLocationInfo=async()=>{
        if(!id)return false
        const url=""
        const data={

        }
        const res= await sendPostByJson(url,data)
        if(res.data){
            //set data to every step form info
        }
    }
    useEffect(()=>{

    },[])
    return (<>
        <div className="layout">
            <header>
                <div className="nav container">
                    <button className="btn_return me_3" onClick={() => navigate("/account/locations")} />
                    <div className="title">New Location</div>
                </div>
            </header>

           
            <div className="container" >

                <div className=" mb_4" style={{ marginTop: "5vh", marginLeft: "5px" }}>

                    <img className="mb_6" id="imgOne"
                        src={stepImg}
                        alt=""
                    />

                </div>
                <form className="form mb_2" id="formOne" style={{ display:`${count==1?'block':'none'}` }}>

                    <div className="row"><div className="mb_4 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Corporate/legal name
                        </label>

                        <input
                            className="form_control"
                            type="text"
                            onChange={e=>setLocationInfo({...locationInfo,corporateName:e.target.value.trim()})}
                            value={locationInfo.corporateName}
                        />
                    </div></div>
                    <div className="row"><div className="mb_4 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Associated Practice Name or Store Number
                        </label>
                        <input
                            className="form_control"
                            type="text"
                            onChange={e=>setLocationInfo({...locationInfo,associatedNumber:e.target.value.trim()})}
                            value={locationInfo.associatedNumber}

                        />
                    </div></div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Office/DBA Name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setLocationInfo({...locationInfo,dbaName:e.target.value.trim()})}
                                value={locationInfo.dbaName}
                            />
                        </div>
                    </div>
                    <div className="row"><div className="mb_4 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Tax ID Number
                        </label>

                        <input
                            className="form_control"
                            type="text"
                            onChange={e=>setLocationInfo({...locationInfo,taxID:e.target.value.trim()})}
                                value={locationInfo.taxID}
                        />
                    </div></div>
                    <div className="row">  <div className="mb_4 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Type of ownership
                        </label>

                        <input
                            className="form_control"
                            type="text"
                            onChange={e=>setLocationInfo({...locationInfo,ownershipType:e.target.value.trim()})}
                            value={locationInfo.ownershipType}
                        />
                    </div></div>
                    <div className="row"><div className="mb_4 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Total number of employees
                        </label>

                        <input
                            className="form_control"
                            type="text"
                            onChange={e=>setLocationInfo({...locationInfo,employeesCount:e.target.value.trim()})}
                            value={locationInfo.employeesCount}
                        />
                    </div></div>
                    <div className="row"><div className="mb_4 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Date Business Started
                        </label>

                        <input
                            className="form_control"
                            type="text"
                            onChange={e=>setLocationInfo({...locationInfo,businessDateStarted:e.target.value.trim()})}
                            value={locationInfo.businessDateStarted}
                        />
                    </div></div>
                    <div className="row"> <div className="mb_4 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Practice/Medical Specialty, or Business Type
                        </label>

                        <input
                            className="form_control"
                            type="text"
                            onChange={e=>setLocationInfo({...locationInfo,businessType:e.target.value.trim()})}
                            value={locationInfo.businessType}

                        />
                    </div></div>
                    <div className="row"> <div className="mb_4 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Practice Management Software
                        </label>

                        <input
                            className="form_control"
                            type="text"
                            onChange={e=>setLocationInfo({...locationInfo,managementPractice:e.target.value.trim()})}
                            value={locationInfo.managementPractice}

                        />
                    </div>
                    </div>
                    <div className="row">
                        <div className="mb_2 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Office Phone Number
                            </label>

                            <input
                                className="form_control"
                                type="text"

                                onChange={e=>setLocationInfo({...locationInfo,officePhone:e.target.value.trim()})}
                                value={locationInfo.officePhone}
                            />
                        </div>
                        <div className="mb_2 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Office Website
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setLocationInfo({...locationInfo,officeWebsite:e.target.value.trim()})}
                                value={locationInfo.officeWebsite}

                            />
                        </div>

                    </div>

                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Fax Number
                            </label>

                            <input
                                className="form_control"
                                type="select"
                                onChange={e=>setLocationInfo({...locationInfo,faxNumber:e.target.value.trim()})}
                                value={locationInfo.faxNumber}

                            />
                        </div>
                    </div>


                </form>

                <form className="form mb_2" id="formTwo" style={{ display:`${count==2?'block':'none'}` }}>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Address Line 1
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setPhysicalAddress({...physicalAddress,addressLine1:e.target.value.trim()})}
                                value={physicalAddress.addressLine1}

                            />
                        </div>
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Address Line 2
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setPhysicalAddress({...physicalAddress,addressLine2:e.target.value.trim()})}
                                value={physicalAddress.addressLine2}
   

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_2 col col_3_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                City
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setPhysicalAddress({...physicalAddress,city:e.target.value.trim()})}
                                value={physicalAddress.city}

                            />
                        </div>
                        <div className="mb_2 col col_3_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                State
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setPhysicalAddress({...physicalAddress,state:e.target.value.trim()})}
                                value={physicalAddress.state}

                            />
                        </div>
                        <div className="mb_4 col col_3_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                ZIP Code
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setPhysicalAddress({...physicalAddress,zipCode:e.target.value.trim()})}
                                value={physicalAddress.zipCode}

                            />
                        </div>
                    </div>





                </form>

                <form className="form mb_4" id="formThree" style={{ display:`${count==3?'block':'none'}` }}>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Address Line 1
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setMailing({...mailing,addressLine1:e.target.value.trim()})}
                                value={mailing.addressLine1}

                            />
                        </div>
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Address Line 2
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setMailing({...mailing,addressLine2:e.target.value.trim()})}
                                value={mailing.addressLine2}    

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_2 col col_3_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                City
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setMailing({...mailing,city:e.target.value.trim()})}
                                value={mailing.city}

                            />
                        </div>
                        <div className="mb_2 col col_3_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                State
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setMailing({...mailing,state:e.target.value.trim()})}
                                value={mailing.state}

                            />
                        </div>
                        <div className="mb_4 col col_3_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                ZIP Code
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setMailing({...mailing,zipCode:e.target.value.trim()})}
                                value={mailing.zipCode}

                            />
                        </div>
                    </div>
                </form>

                <form className="form mb_4" id="formFor" style={{ display:`${count==4?'block':'none'}` }}>
                    <div className="row">
                        <div className="mb_2 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                First Name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setOwner({...owner,firstName:e.target.value.trim()})}
                                value={owner.firstName}

                            />
                        </div>
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Last Name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setOwner({...owner,lastName:e.target.value.trim()})}
                                value={owner.lastName}

                            />
                        </div>

                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Medical/Business/State License Number
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setOwner({...owner,medicalNumber:e.target.value.trim()})}
                                value={owner.medicalNumber}

                            />
                        </div>
                    </div>
                  
                    <div className="row">
                        <div className="mb_2 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Address Line 1
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setOwner({...owner,addressLine1:e.target.value.trim()})}
                                value={owner.addressLine1}

                            />
                        </div>
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Address Line 2
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setOwner({...owner,addressLine2:e.target.value.trim()})}
                                value={owner.addressLine2}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_2 col col_3_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                City
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setOwner({...owner,city:e.target.value.trim()})}
                                value={owner.city}    

                            />
                        </div>
                        <div className="mb_2 col col_3_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                State
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setOwner({...owner,state:e.target.value.trim()})}
                                value={owner.state}

                            />
                        </div>
                        <div className="mb_4 col col_3_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                ZIP Code
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setOwner({...owner,zipCode:e.target.value.trim()})}
                                value={owner.zipCode}

                            />
                        </div>
                    </div>
                    <div className="row">

                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Phone Number
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setOwner({...owner,phoneNumber:e.target.value.trim()})}
                                value={owner.phoneNumber}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Email
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setOwner({...owner,email:e.target.value.trim()})}
                                value={owner.email}

                            />
                        </div>
                    </div>
                </form>

                <form className="form mb_4" id="formFive" style={{ display:`${count==5?'block':'none'}` }}>
                    <div className="row">
                        <div className="mb_2 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                First Name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setManagingDoctor({...managingDoctor,firstName:e.target.value.trim()})}
                                value={managingDoctor.firstName}

                            />
                        </div>
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Last Name
                            </label>

                            <input
                                className="form_control"
                                type="select"
                                onChange={e=>setManagingDoctor({...managingDoctor,lastName:e.target.value.trim()})}
                                value={managingDoctor.lastName}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Medical License Number
                            </label>

                            <input
                                className="form_control"
                                type="select"
                                onChange={e=>setManagingDoctor({...managingDoctor,medicalLicense:e.target.value.trim()})}
                                value={managingDoctor.medicalLicense}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Medical License State
                            </label>

                            <input
                                className="form_control"
                                type="select"
                                onChange={e=>setManagingDoctor({...managingDoctor,medicalLicenseState:e.target.value.trim()})}
                                value={managingDoctor.medicalLicenseState}

                            />
                        </div>
                    </div>



                </form>

                <form className="form mb_4" id="formSix" style={{ display:`${count==6?'block':'none'}` }}>
                    <div className="row">
                        <div className="mb_2 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                First Name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setConcate({...concate,firstName:e.target.value.trim()})}
                                value={concate.firstName}

                            />
                        </div>
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Last Name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setConcate({...concate,lastName:e.target.value.trim()})}
                                value={concate.lastName}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                            Phone number
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setConcate({...concate,phoneNumber:e.target.value.trim()})}
                                value={concate.phoneNumber}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                            Email
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                onChange={e=>setConcate({...concate,email:e.target.value.trim()})}
                                value={concate.email}

                            />
                        </div>
                    </div>


                </form>
                <form className="form mb_4" id="formSeven" style={{ display:`${count==7?'block':'none'}` }}>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Bank Name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                value={bankInfo.banktName}
                                onChange={e=>setBankInfo({...bankInfo,banktName:e.target.value.trim()})}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Routing Number
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                value={bankInfo.routingNumber}
                                onChange={e=>setBankInfo({...bankInfo,routingNumber:e.target.value.trim()})}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Account Number
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                value={bankInfo.accountNumber}
                                onChange={e=>setBankInfo({...bankInfo,accountNumber:e.target.value.trim()})}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Bank Letter or Void Check as an attachment
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                 value={bankInfo.bankLatter}
                                onChange={e=>setBankInfo({...bankInfo,bankLatter:e.target.value.trim()})}
                            />
                        </div>
                    </div>




                </form>
              

                <div className="offcanvas_footer filter_footer" id="buttonThree" >
                    <div className="btn_group">
                        {
                          count>1? <button className="btn btn_primary_line_transparent"  onClick={lastStep}>Back</button>:<></>
                        }
                        {
                           count==7? <button className="btn btn_primary ms_auto col" onClick={add} >Submit</button>
                           :<button className="btn btn_primary ms_auto col" onClick={nextStep} >Next</button>
                        }
                        
                    </div>
                </div>
            </div>




        </div>
    </>);
};

LocationForm.propTypes = {};

export default LocationForm;
