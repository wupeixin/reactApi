import React,{useState}from "react";

import download from '../../assets/downLoad.png'
import styles from './index.module.css'
const downLoadFile=(props)=>{

    const downfile=()=>{
        let url=props.fileUrl
        if(!url){return false}
         let fileName= new URL(url).pathname.split('/').pop()
        const link = document.createElement('a');  
        link.href = props.fileUrl; 
        link.download = '';  
        link.style.display = 'none';  
        document.body.appendChild(link);  
        link.click();  
        document.body.removeChild(link);  
    }
    return(
        <div className={`${styles.down_load}`} onClick={downfile}>
            <img src={download}> </img>
        </div>
    )
}
export default downLoadFile
