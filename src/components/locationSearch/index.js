import React,{useState,useEffect} from "react";
import { sendPostByJson } from "../../utils/httpUtils";

const LocationFilter=(props)=>{
    const [keyWord,setkeyWord]=useState("")
    const [locations,setLocations]=useState([
      {id:1,checked:false,name:"TS"},
      {id:2,checked:false,name:"TK"},
      {id:3,checked:false,name:"TF"},
    ])

    
    const filterLocation=(event)=>{
        event.preventDefault()
        console.log(keyWord);
        if(!keyWord){
          setLocations([])
          props.filterResult&&props.filterResult([]) 
          return false
        }
        //filter by keyword

    }
    const getLocations=async()=>{
        const url=""
        const data=""
        try {
            const res= await sendPostByJson(url,data)
            if(res.data){
              //add checked = false adjust checkebox
            }
            
        } catch (error) {
            console.log(error)
        }

    }
    const changeChecked=(id)=>{
    let array=  locations.map(v=>{
        if(v.id==id){
          v.checked=!v.checked
        }
        return v
      })
      setLocations(array)
      //send result to parent
      let result=array.filter(v=>v.checked)
    
      props.filterResult&&props.filterResult(result)  
    }

    return(
          <div className="mt_5 mt_4_md">
                <div className="filter_title">Locations</div>
                <div className="search_bar mt_3">
                  <input
                    className="form_control"
                    type="text"
                    placeholder="Search applicants..."
                    value={keyWord}
                    onChange={(e)=>setkeyWord(e.target.value.trim())}
                  />
                  <button className="btn btn_icon" onClick={filterLocation}>
                    <img
                      src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzQ0MDhfMjUyNykiPgo8cGF0aCBkPSJNMTUuNTAwNiAxNC4wMDA2SDE0LjcxMDZMMTQuNDMwNiAxMy43MzA2QzE1LjYzMDYgMTIuMzMwNiAxNi4yNTA2IDEwLjQyMDYgMTUuOTEwNiA4LjM5MDYzQzE1LjQ0MDYgNS42MTA2MyAxMy4xMjA2IDMuMzkwNjMgMTAuMzIwNiAzLjA1MDYzQzYuMDkwNjMgMi41MzA2MyAyLjUzMDYzIDYuMDkwNjMgMy4wNTA2MyAxMC4zMjA2QzMuMzkwNjMgMTMuMTIwNiA1LjYxMDYzIDE1LjQ0MDYgOC4zOTA2MyAxNS45MTA2QzEwLjQyMDYgMTYuMjUwNiAxMi4zMzA2IDE1LjYzMDYgMTMuNzMwNiAxNC40MzA2TDE0LjAwMDYgMTQuNzEwNlYxNS41MDA2TDE4LjI1MDYgMTkuNzUwNkMxOC42NjA2IDIwLjE2MDYgMTkuMzMwNiAyMC4xNjA2IDE5Ljc0MDYgMTkuNzUwNkMyMC4xNTA2IDE5LjM0MDYgMjAuMTUwNiAxOC42NzA2IDE5Ljc0MDYgMTguMjYwNkwxNS41MDA2IDE0LjAwMDZaTTkuNTAwNjMgMTQuMDAwNkM3LjAxMDYzIDE0LjAwMDYgNS4wMDA2MyAxMS45OTA2IDUuMDAwNjMgOS41MDA2M0M1LjAwMDYzIDcuMDEwNjMgNy4wMTA2MyA1LjAwMDYzIDkuNTAwNjMgNS4wMDA2M0MxMS45OTA2IDUuMDAwNjMgMTQuMDAwNiA3LjAxMDYzIDE0LjAwMDYgOS41MDA2M0MxNC4wMDA2IDExLjk5MDYgMTEuOTkwNiAxNC4wMDA2IDkuNTAwNjMgMTQuMDAwNloiIGZpbGw9IiM4RThFOEUiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMF80NDA4XzI1MjciPgo8cmVjdCB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="
                      alt=""
                    />
                  </button>
                </div>
                <ul className="">
                  {
                    locations.map((v,i)=>{
                      return (
                          <li className="my_3 my_5_sm d_flex" key={i}>
                          <input className="form_checkbox" type="checkbox" checked={v.checked}  onChange={()=>changeChecked(v.id)} />
                          <label className="checkbox_label mt_1 ms_2">
                            <div className="color_primary700">
                              <span className="fw_medium">legal name</span> (owner name)
                            </div>
                            <div className="mt_2 fs_sm">
                             {v.name||""}
                            </div>
                          </label>
                        </li>
                      )
                    })
                  }
                
                 
                </ul>
              </div>
    )
}
export default LocationFilter