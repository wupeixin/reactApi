import React, { useState, PureComponent } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";

const DashboardChart = (props) => {
  const navigate = useNavigate();
  const data = [
    {
      name: 'Page A',
      2019: 4000,
      2018: 2400,
      amt: 2400,
    },
    {
      name: 'Page B',
      2019: 3000,
      2018: 1398,
      amt: 2210,
    },
    {
      name: 'Page C',
      2019: 2000,
      2018: 9800,
      amt: 2290,
    },
    {
      name: 'Page D',
      2019: 2780,
      2018: 3908,
      amt: 2000,
    },
    {
      name: 'Page E',
      2019: 1890,
      2018: 4800,
      amt: 2181,
    },
    {
      name: 'Page F',
      2019: 2390,
      2018: 3800,
      amt: 2500,
    },
    {
      name: 'Page G',
      2019: 3490,
      2018: 4300,
      amt: 2100,
    },
  ];
  return (<>
  <ResponsiveContainer width="100%" height="100%">
        <BarChart
          width={500}
          height={300}
          data={data}
          margin={{
            top: 20,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis yAxisId="left" orientation="left" stroke="#8884d8" />
          <YAxis yAxisId="right" orientation="right" stroke="#82ca9d" />
          <Tooltip />
          <Legend />
          <Bar yAxisId="left" dataKey="2018" fill="#8884d8" />
          <Bar yAxisId="right" dataKey="2019" fill="#82ca9d" />
        </BarChart>
      </ResponsiveContainer>
  </>);
};

DashboardChart.propTypes = {};

export default DashboardChart;
