import React, { useState,useRef } from "react";
import { useNavigate, useSearchParams,useLocation } from "react-router-dom";
import { ToastContainer,toast } from 'react-toastify';
import { MultiSelect } from "react-multi-select-component";//mult select
import { checkIsEmail, ToastInfo, ToastError, ToastSucces, } from "../../utils/check.js";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";
import { getUserInfo ,getItem} from "../../utils/storage.js";


const defaultRoleId={
    "SuperAdministrator":101,
    "bank":201
}
const UserManagementForm = (props) => {
    const toastId = useRef(null);
    const navigate = useNavigate();
    const [showToast,setShowToast]=useState(false)
    const [user,setUser]=useState({
        "IsActiveByOrganization":true,
        "firstName": "",
        "lastName": "",
        "email": "",
        "phoneNumber": "",
        // "password": "",
        "invitationCode": "",
        "organizationId": 0,
        "province": "",
        "city": "",
        "address1": "",
        "address2": "",
        "zipCode": "",
        "reEmail":"",
        "locationfilter":""
    })
  const [lsStr,setLsStr]=useState("")
  const [locationList,setLocationList]=useState([])


    // const [step,setStep] =useState();
    const nextStep = (e) => {

        console.log(user);

            e.preventDefault();
          
          
           
       
        if(!user.firstName){
               ToastError('Please enter firstName', toastId)
            return false
        }
        if(!user.lastName){
               ToastError('Please enter lastName', toastId)
               return false
        }
        if(!checkIsEmail(user.email)){
            ToastError('Please enter a correct format email address', toastId)
            return false
        }
        if(!checkIsEmail(user.reEmail)){
            ToastError('Please enter a correct format email address', toastId)
            return false
        }
        if(user.email!==user.reEmail){
               ToastError('Please enter the same email address', toastId)
               return false
        }

        if(user.email&&user.reEmail&&user.email!=user.reEmail){
               ToastError('The mailboxes you entered twice are not the same', toastId)
               return false

        }
        // let pjpneStr= /^\d{10}$/
        // if(! pjpneStr.test( user.phoneNumber)){
        //        ToastError('Please enter a 10 digit phone number', toastId)
        //        return false
        // }
        // if(!user.password){
        //        ToastError('Please enter password', toastId)
        //        return false
        // }
        // let str=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_])[A-Za-z\d\W_]{8,30}$/
        // if(!str.test(user.password)){
        //     let errStr='Please enter passwords between 8 and 30, including uppercase and lowercase letters, numbers, and special symbols'
        //        ToastError(errStr, toastId)
        //        return false
        // }
        sendEmail()
    }
    const closePopup = () => {
        // $(".popup").removeClass("show");
        setShowToast(false)
    }
    const openPopup = () => {
        // $(".popup").addClass("show");
        setShowToast(true)
    }
    const saveDate=async(invitationCode)=>{
        const url="/Account/CreateUser"
        try {
            const loginUser=getUserInfo()
            user.invitationCode=invitationCode
            user.organizationId=loginUser.organizationId
            const res= await sendPostByJson(url,user)
            if (res.data.code===200){
                ToastSucces("Create new user success",toastId)
                setTimeout(() => {
                    navigate("/account/user-management")
                }, 1200);
              
            }else if(res.data.code===409){
                ToastError("Email already duplicated",toastId)

            }

        } catch (error) {
            if (error.response.status===409){
                ToastError("Email already duplicated",toastId)

              
            }else{
                ToastError("Create new user failed,please try again latter!",toastId)    

            }

        }

    }
    const sendEmail=async()=>{
        const loginUser=getUserInfo()
           const roleId=getItem('source')==200?201:101
            try {
              
                const url="/Account/CreateAndSendInvitation"
                const res= await sendPostByJson(url,{
                    email:user.email,
                    firstName:user.firstName,
                    lastName:user.lastName,
                    organizationId:loginUser.organizationId,
                    roleId:roleId, //201
                })
                console.log(res);

                if(res.data){
                    // ToastSucces("Create new user success and send invitation email success!",toastId)
                    saveDate(res.data.invitationCode)
                }else if(res.data.code==409 ){
                    ToastError("The email has already been used, please switch to another one!",toastId)
                }else if(res.data.code==400){
                    ToastError("Phone number not verified!",toastId)
                }
              
                
            } catch (error) {
                if (error.response.status===409){
                    ToastError("Email already duplicated",toastId)
                }else if (error.response.status===400){
                    ToastError("Phone number not verified!",toastId)
                }
            }
    }
    const filterLocation =(e)=>{
        e.preventDefault();
        console.log(lsStr);
    }

    const changeAct =()=>{
        let f=user.IsActiveByOrganization
        setUser({...user,IsActiveByOrganization:!f})
    }
    const goback=(arg)=>{
        if(arg==1){
            setShowToast(false)
            navigate("/account/user-management")
        }else{
          //stay page and save data
          setShowToast(false)
          sendEmail()
        }
    }
    const options = [
        { label: "Grapes 🍇", value: "grapes" },
        { label: "Mango 🥭", value: "mango" },
        { label: "Strawberry 🍓", value: "strawberry" },
        { label: "Watermelon 🍉", value: "watermelon" },
        { label: "Pear 🍐", value: "pear", disabled: true },
        { label: "Apple 🍎", value: "apple" },
        { label: "Tangerine 🍊", value: "tangerine" },
        { label: "Pineapple 🍍", value: "pineapple" },
        { label: "Peach 🍑", value: "peach" }
      ];
    
      const [selected, setSelected] = useState([]);
    
    return (<>
      <ToastContainer limit={1} />
        <div className="layout">
            <header>
                <div className="nav container">
                    <button className="btn_return me_3" onClick={openPopup} />
                    <div className="title">User management</div>
                </div>
                {/* <ButtonModal/> */}

            </header>

            <div className="container" >
                <div className=" mb_4" style={{ marginTop: "5vh",display:'flex' }}>
                <label className="form_label" htmlFor=""  style={{ marginRight: "20px",fontWeight:'600',}}>
                                Active
                 </label>
                <div className={`form_switch ${user.IsActiveByOrganization?'active':''}`} onClick={changeAct}  style={{cursor:"pointer"}}></div>
                    {/* <img id="imgOne"
                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIzIiBoZWlnaHQ9IjQ4IiB2aWV3Qm94PSIwIDAgMTIzIDQ4IiBmaWxsPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8cGF0aCBkPSJNNy41MDU4NiAyOUw2LjUyODMyIDI2LjIyNDZIMi43MDcwM0wxLjcyOTQ5IDI5SDBMMy43MTg3NSAxOC45NjQ4SDUuNTM3MTFMOS4yNDkwMiAyOUg3LjUwNTg2Wk02LjEwNDQ5IDI0LjgxNjRMNS4xNTQzIDIyLjA4MkM1LjExNzg0IDIxLjk2MzUgNS4wNjU0MyAyMS43OTcyIDQuOTk3MDcgMjEuNTgzQzQuOTI4NzEgMjEuMzY0MyA0Ljg2MDM1IDIxLjE0MzIgNC43OTE5OSAyMC45MTk5QzQuNzIzNjMgMjAuNjkyMSA0LjY2NjY3IDIwLjUwMDcgNC42MjEwOSAyMC4zNDU3QzQuNTc1NTIgMjAuNTMyNiA0LjUxODU1IDIwLjc0MjIgNC40NTAyIDIwLjk3NDZDNC4zODYzOSAyMS4yMDI1IDQuMzIyNTkgMjEuNDE2NyA0LjI1ODc5IDIxLjYxNzJDNC4xOTk1NCAyMS44MTc3IDQuMTUzOTcgMjEuOTcyNyA0LjEyMjA3IDIyLjA4MkwzLjE2NTA0IDI0LjgxNjRINi4xMDQ0OVpNMTMuNDUzMSAyOS4xMzY3QzEyLjczNzYgMjkuMTM2NyAxMi4xMTc4IDI4Ljk5NzcgMTEuNTkzOCAyOC43MTk3QzExLjA2OTcgMjguNDQxNyAxMC42NjYzIDI4LjAxNTYgMTAuMzgzOCAyNy40NDE0QzEwLjEwMTIgMjYuODY3MiA5Ljk1OTk2IDI2LjEzOCA5Ljk1OTk2IDI1LjI1MzlDOS45NTk5NiAyNC4zMzMzIDEwLjExNDkgMjMuNTgxNCAxMC40MjQ4IDIyLjk5OEMxMC43MzQ3IDIyLjQxNDcgMTEuMTYzMSAyMS45ODQgMTEuNzEgMjEuNzA2MUMxMi4yNjE0IDIxLjQyODEgMTIuODkyNiAyMS4yODkxIDEzLjYwMzUgMjEuMjg5MUMxNC4wNTQ3IDIxLjI4OTEgMTQuNDYyNiAyMS4zMzQ2IDE0LjgyNzEgMjEuNDI1OEMxNS4xOTYzIDIxLjUxMjQgMTUuNTA4NSAyMS42MTk1IDE1Ljc2MzcgMjEuNzQ3MUwxNS4yODUyIDIzLjAzMjJDMTUuMDA3MiAyMi45MTgzIDE0LjcyMjMgMjIuODIyNiAxNC40MzA3IDIyLjc0NTFDMTQuMTM5IDIyLjY2NzYgMTMuODU4NyAyMi42Mjg5IDEzLjU4OTggMjIuNjI4OUMxMy4xNDc4IDIyLjYyODkgMTIuNzc4NiAyMi43MjY5IDEyLjQ4MjQgMjIuOTIyOUMxMi4xOTA4IDIzLjExODggMTEuOTcyIDIzLjQxMDUgMTEuODI2MiAyMy43OTc5QzExLjY4NDkgMjQuMTg1MiAxMS42MTQzIDI0LjY2NiAxMS42MTQzIDI1LjI0MDJDMTEuNjE0MyAyNS43OTYyIDExLjY4NzIgMjYuMjY1NiAxMS44MzMgMjYuNjQ4NEMxMS45Nzg4IDI3LjAyNjcgMTIuMTk1MyAyNy4zMTM4IDEyLjQ4MjQgMjcuNTA5OEMxMi43Njk1IDI3LjcwMTIgMTMuMTIyNyAyNy43OTY5IDEzLjU0MiAyNy43OTY5QzEzLjk1NjcgMjcuNzk2OSAxNC4zMjgxIDI3Ljc0NjcgMTQuNjU2MiAyNy42NDY1QzE0Ljk4NDQgMjcuNTQ2MiAxNS4yOTQzIDI3LjQxNjMgMTUuNTg1OSAyNy4yNTY4VjI4LjY1MTRDMTUuMjk4OCAyOC44MTU0IDE0Ljk5MTIgMjguOTM2MiAxNC42NjMxIDI5LjAxMzdDMTQuMzM1IDI5LjA5NTcgMTMuOTMxNiAyOS4xMzY3IDEzLjQ1MzEgMjkuMTM2N1pNMjAuMTc5NyAyNy44Mzc5QzIwLjM4OTMgMjcuODM3OSAyMC41OTY3IDI3LjgxOTcgMjAuODAxOCAyNy43ODMyQzIxLjAwNjggMjcuNzQyMiAyMS4xOTM3IDI3LjY5NDMgMjEuMzYyMyAyNy42Mzk2VjI4Ljg1NjRDMjEuMTg0NiAyOC45MzM5IDIwLjk1NDQgMjkgMjAuNjcxOSAyOS4wNTQ3QzIwLjM4OTMgMjkuMTA5NCAyMC4wOTU0IDI5LjEzNjcgMTkuNzkgMjkuMTM2N0MxOS4zNjE3IDI5LjEzNjcgMTguOTc2NiAyOS4wNjYxIDE4LjYzNDggMjguOTI0OEMxOC4yOTMgMjguNzc5IDE4LjAyMTggMjguNTMwNiAxNy44MjEzIDI4LjE3OTdDMTcuNjIwOCAyNy44Mjg4IDE3LjUyMDUgMjcuMzQzNCAxNy41MjA1IDI2LjcyMzZWMjIuNjU2MkgxNi40ODgzVjIxLjkzODVMMTcuNTk1NyAyMS4zNzExTDE4LjEyMjEgMTkuNzUxSDE5LjEzMzhWMjEuNDMyNkgyMS4zMDA4VjIyLjY1NjJIMTkuMTMzOFYyNi43MDMxQzE5LjEzMzggMjcuMDg1OSAxOS4yMjk1IDI3LjM3MDggMTkuNDIwOSAyNy41NTc2QzE5LjYxMjMgMjcuNzQ0NSAxOS44NjUyIDI3LjgzNzkgMjAuMTc5NyAyNy44Mzc5Wk0yNC41MDY4IDIxLjQzMjZWMjlIMjIuOTAwNFYyMS40MzI2SDI0LjUwNjhaTTIzLjcxMzkgMTguNTM0MkMyMy45NiAxOC41MzQyIDI0LjE3MTkgMTguNjAwMyAyNC4zNDk2IDE4LjczMjRDMjQuNTMxOSAxOC44NjQ2IDI0LjYyMyAxOS4wOTI0IDI0LjYyMyAxOS40MTZDMjQuNjIzIDE5LjczNSAyNC41MzE5IDE5Ljk2MjkgMjQuMzQ5NiAyMC4wOTk2QzI0LjE3MTkgMjAuMjMxOCAyMy45NiAyMC4yOTc5IDIzLjcxMzkgMjAuMjk3OUMyMy40NTg3IDIwLjI5NzkgMjMuMjQyMiAyMC4yMzE4IDIzLjA2NDUgMjAuMDk5NkMyMi44OTEzIDE5Ljk2MjkgMjIuODA0NyAxOS43MzUgMjIuODA0NyAxOS40MTZDMjIuODA0NyAxOS4wOTI0IDIyLjg5MTMgMTguODY0NiAyMy4wNjQ1IDE4LjczMjRDMjMuMjQyMiAxOC42MDAzIDIzLjQ1ODcgMTguNTM0MiAyMy43MTM5IDE4LjUzNDJaTTI4LjU0IDI5TDI1LjY2MjEgMjEuNDMyNkgyNy4zNjQzTDI4LjkyMjkgMjUuODc2QzI5LjAyMzEgMjYuMTU4NSAyOS4xMTQzIDI2LjQ2MzkgMjkuMTk2MyAyNi43OTJDMjkuMjc4MyAyNy4xMjAxIDI5LjMzNTMgMjcuMzk1OCAyOS4zNjcyIDI3LjYxOTFIMjkuNDIxOUMyOS40NTM4IDI3LjM4NjcgMjkuNTE1MyAyNy4xMDg3IDI5LjYwNjQgMjYuNzg1MkMyOS42OTc2IDI2LjQ1NyAyOS43OTEgMjYuMTU0IDI5Ljg4NjcgMjUuODc2TDMxLjQ0NTMgMjEuNDMyNkgzMy4xNDA2TDMwLjI2MjcgMjlIMjguNTRaTTM3LjMwMzcgMjEuMjg5MUMzNy45NzgyIDIxLjI4OTEgMzguNTU3IDIxLjQyODEgMzkuMDQgMjEuNzA2MUMzOS41MjMxIDIxLjk4NCAzOS44OTQ1IDIyLjM3ODMgNDAuMTU0MyAyMi44ODg3QzQwLjQxNDEgMjMuMzk5MSA0MC41NDM5IDI0LjAwOTggNDAuNTQzOSAyNC43MjA3VjI1LjU4MkgzNS40OTIyQzM1LjUxMDQgMjYuMzE1OCAzNS43MDY0IDI2Ljg4MDkgMzYuMDgwMSAyNy4yNzczQzM2LjQ1ODMgMjcuNjczOCAzNi45ODcgMjcuODcyMSAzNy42NjYgMjcuODcyMUMzOC4xNDkxIDI3Ljg3MjEgMzguNTgyIDI3LjgyNjUgMzguOTY0OCAyNy43MzU0QzM5LjM1MjIgMjcuNjM5NiAzOS43NTEgMjcuNTAwNyA0MC4xNjExIDI3LjMxODRWMjguNjI0QzM5Ljc4MjkgMjguODAxOCAzOS4zOTc4IDI4LjkzMTYgMzkuMDA1OSAyOS4wMTM3QzM4LjYxMzkgMjkuMDk1NyAzOC4xNDQ1IDI5LjEzNjcgMzcuNTk3NyAyOS4xMzY3QzM2Ljg1NDggMjkuMTM2NyAzNi4yMDA4IDI4Ljk5MzIgMzUuNjM1NyAyOC43MDYxQzM1LjA3NTIgMjguNDE0NCAzNC42MzU0IDI3Ljk4MTQgMzQuMzE2NCAyNy40MDcyQzM0LjAwMiAyNi44MzMgMzMuODQ0NyAyNi4xMTk4IDMzLjg0NDcgMjUuMjY3NkMzMy44NDQ3IDI0LjQxOTkgMzMuOTg4MyAyMy42OTk5IDM0LjI3NTQgMjMuMTA3NEMzNC41NjI1IDIyLjUxNSAzNC45NjU4IDIyLjA2MzggMzUuNDg1NCAyMS43NTM5QzM2LjAwNDkgMjEuNDQ0IDM2LjYxMSAyMS4yODkxIDM3LjMwMzcgMjEuMjg5MVpNMzcuMzAzNyAyMi40OTlDMzYuNzk3OSAyMi40OTkgMzYuMzg3NyAyMi42NjMxIDM2LjA3MzIgMjIuOTkxMkMzNS43NjMzIDIzLjMxOTMgMzUuNTgxMSAyMy44MDAxIDM1LjUyNjQgMjQuNDMzNkgzOC45NzE3QzM4Ljk2NzEgMjQuMDU1MyAzOC45MDMzIDIzLjcyMDQgMzguNzgwMyAyMy40Mjg3QzM4LjY2MTggMjMuMTM3IDM4LjQ3OTUgMjIuOTA5MiAzOC4yMzM0IDIyLjc0NTFDMzcuOTkxOSAyMi41ODExIDM3LjY4MiAyMi40OTkgMzcuMzAzNyAyMi40OTlaIiBmaWxsPSIjMzMzMzMzIi8+CjxnIGZpbHRlcj0idXJsKCNmaWx0ZXIwX2RkXzM4NzFfMTU0MzApIj4KPHJlY3QgeD0iNzEiIHk9IjEzIiB3aWR0aD0iNDgiIGhlaWdodD0iMjQiIHJ4PSIxMiIgZmlsbD0iIzM0N0FGNiIvPgo8Y2lyY2xlIGN4PSI4MiIgY3k9IjI1IiByPSIxMyIgZmlsbD0id2hpdGUiLz4KPC9nPgo8ZGVmcz4KPGZpbHRlciBpZD0iZmlsdGVyMF9kZF8zODcxXzE1NDMwIiB4PSI2NSIgeT0iLTIiIHdpZHRoPSI1OCIgaGVpZ2h0PSI1OCIgZmlsdGVyVW5pdHM9InVzZXJTcGFjZU9uVXNlIiBjb2xvci1pbnRlcnBvbGF0aW9uLWZpbHRlcnM9InNSR0IiPgo8ZmVGbG9vZCBmbG9vZC1vcGFjaXR5PSIwIiByZXN1bHQ9IkJhY2tncm91bmRJbWFnZUZpeCIvPgo8ZmVDb2xvck1hdHJpeCBpbj0iU291cmNlQWxwaGEiIHR5cGU9Im1hdHJpeCIgdmFsdWVzPSIwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAxMjcgMCIgcmVzdWx0PSJoYXJkQWxwaGEiLz4KPGZlT2Zmc2V0IGR5PSI0Ii8+CjxmZUdhdXNzaWFuQmx1ciBzdGREZXZpYXRpb249IjIiLz4KPGZlQ29sb3JNYXRyaXggdHlwZT0ibWF0cml4IiB2YWx1ZXM9IjAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAuMDggMCIvPgo8ZmVCbGVuZCBtb2RlPSJub3JtYWwiIGluMj0iQmFja2dyb3VuZEltYWdlRml4IiByZXN1bHQ9ImVmZmVjdDFfZHJvcFNoYWRvd18zODcxXzE1NDMwIi8+CjxmZUNvbG9yTWF0cml4IGluPSJTb3VyY2VBbHBoYSIgdHlwZT0ibWF0cml4IiB2YWx1ZXM9IjAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDEyNyAwIiByZXN1bHQ9ImhhcmRBbHBoYSIvPgo8ZmVPZmZzZXQvPgo8ZmVHYXVzc2lhbkJsdXIgc3RkRGV2aWF0aW9uPSIxIi8+CjxmZUNvbG9yTWF0cml4IHR5cGU9Im1hdHJpeCIgdmFsdWVzPSIwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwLjA0IDAiLz4KPGZlQmxlbmQgbW9kZT0ibm9ybWFsIiBpbjI9ImVmZmVjdDFfZHJvcFNoYWRvd18zODcxXzE1NDMwIiByZXN1bHQ9ImVmZmVjdDJfZHJvcFNoYWRvd18zODcxXzE1NDMwIi8+CjxmZUJsZW5kIG1vZGU9Im5vcm1hbCIgaW49IlNvdXJjZUdyYXBoaWMiIGluMj0iZWZmZWN0Ml9kcm9wU2hhZG93XzM4NzFfMTU0MzAiIHJlc3VsdD0ic2hhcGUiLz4KPC9maWx0ZXI+CjwvZGVmcz4KPC9zdmc+Cg=="
                        alt=""
                    /> */}
                </div>
                <form className="form mb_6" id="formOne">
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                First name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Enter first name"
                                value={user.firstName}
                                onChange={(e)=>{setUser({...user,firstName:e.target.value.trim()})}}
                            />
                        </div>
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Last name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Enter last name"
                                value={user.lastName}
                                onChange={(e)=>{setUser({...user,lastName:e.target.value.trim()})}}

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Email
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Enter email address"
                                value={user.email}
                                onChange={(e)=>{setUser({...user,email:e.target.value.trim()})}}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Re-enter email
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Re-Enter email address"
                                value={user.reEmail}
                                onChange={(e)=>{setUser({...user,reEmail:e.target.value.trim()})}}
                            />
                        </div>
                    </div>
                    {/* <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                             HCG
                            </label>
                            <MultiSelect     style={{borderRadius:"16px"}}
                                options={options}
                                value={selected}
                                onChange={setSelected}
                                labelledBy={"Select"}
                                isCreatable={true}
                            />
                        
                        </div>
                    </div> */}
                  {/*   <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                              Password
                            </label>

                            <input
                                className="form_control"
                                type="password"
                                placeholder="Enter password"
                                value={user.password}
                                onChange={(e)=>{setUser({...user,password:e.target.value.trim()})}}
                            />
                        </div>
                    </div> */}
                    <p className="mb_2">Locations</p>
                    <div className="row">
                        <div className="col col_2_sm col_2_md">
                            <div className="search_bar">
                                <input
                                    className="form_control"
                                    type="text"
                                    value={lsStr}
                                    onChange={(e)=>{setLsStr(e.target.value.trim())}}
                                    placeholder="Search location..."
                                />
                                <button className="btn btn_icon" onClick={filterLocation}>
                                    <img
                                        src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzQ0MDhfMjUyNykiPgo8cGF0aCBkPSJNMTUuNTAwNiAxNC4wMDA2SDE0LjcxMDZMMTQuNDMwNiAxMy43MzA2QzE1LjYzMDYgMTIuMzMwNiAxNi4yNTA2IDEwLjQyMDYgMTUuOTEwNiA4LjM5MDYzQzE1LjQ0MDYgNS42MTA2MyAxMy4xMjA2IDMuMzkwNjMgMTAuMzIwNiAzLjA1MDYzQzYuMDkwNjMgMi41MzA2MyAyLjUzMDYzIDYuMDkwNjMgMy4wNTA2MyAxMC4zMjA2QzMuMzkwNjMgMTMuMTIwNiA1LjYxMDYzIDE1LjQ0MDYgOC4zOTA2MyAxNS45MTA2QzEwLjQyMDYgMTYuMjUwNiAxMi4zMzA2IDE1LjYzMDYgMTMuNzMwNiAxNC40MzA2TDE0LjAwMDYgMTQuNzEwNlYxNS41MDA2TDE4LjI1MDYgMTkuNzUwNkMxOC42NjA2IDIwLjE2MDYgMTkuMzMwNiAyMC4xNjA2IDE5Ljc0MDYgMTkuNzUwNkMyMC4xNTA2IDE5LjM0MDYgMjAuMTUwNiAxOC42NzA2IDE5Ljc0MDYgMTguMjYwNkwxNS41MDA2IDE0LjAwMDZaTTkuNTAwNjMgMTQuMDAwNkM3LjAxMDYzIDE0LjAwMDYgNS4wMDA2MyAxMS45OTA2IDUuMDAwNjMgOS41MDA2M0M1LjAwMDYzIDcuMDEwNjMgNy4wMTA2MyA1LjAwMDYzIDkuNTAwNjMgNS4wMDA2M0MxMS45OTA2IDUuMDAwNjMgMTQuMDAwNiA3LjAxMDYzIDE0LjAwMDYgOS41MDA2M0MxNC4wMDA2IDExLjk5MDYgMTEuOTkwNiAxNC4wMDA2IDkuNTAwNjMgMTQuMDAwNloiIGZpbGw9IiM4RThFOEUiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMF80NDA4XzI1MjciPgo8cmVjdCB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="
                                        alt=""
                                    />
                                </button>
                                <ul className="">
                                    <li className="my_3 my_5_sm d_flex">
                                        <input className="form_checkbox" type="checkbox" />
                                        <label className="checkbox_label mt_1 ms_2">
                                            <div className="color_primary700">
                                                <span className="fw_medium">legal name</span> (owner name)
                                            </div>
                                            <div className="mt_2 fs_sm">
                                                4000 La Rica Ave Suite D Baldwin Park, CA 91706
                                            </div>
                                        </label>
                                    </li>
                                    <li className="my_3 my_5_sm d_flex">
                                        <input className="form_checkbox" type="checkbox" />
                                        <label className="checkbox_label mt_1 fs_sm ms_2">
                                            <div className="color_primary700">
                                                <span className="fw_medium">legal name</span> (owner name)
                                            </div>
                                            <div className="mt_2 fs_sm">
                                                4000 La Rica Ave Suite D Baldwin Park, CA 91706
                                            </div>
                                        </label>
                                    </li>
                                </ul>
                            </div>

                        </div>


                    </div>
                    <div className="offcanvas_footer filter_footer">
                        <div className="btn_group">
                            {/* <button className="btn btn_primary_line_transparent" onClick={getBack}>Cancel</button> */}
                            <button className="btn btn_primary ms_auto col" onClick={nextStep} >Create User</button>
                        </div>
                    </div>
                </form>


                {/* <button className="btn btn_primary w_100 mt_3" onClick={() => nextStep()}>Create User</button> */}
            </div>
        </div>

        <div className={`popup ${showToast?'show':''}`}>
            <div className="popup_container">
                <div className="popup_box">
                    <button className="btn btn_close" onClick={closePopup}></button>
                    <div className="popup_content">
                        <div>do you want to save informations?</div>
                        {/* <input className="form_control mt_5 mt_6_md" type="text" placeholder="Tax ID"></input> */}
                        <div className="btn_group mt_5 mt_6_md">
                            <button className="btn btn_ok btn_primary_line" onClick={()=>goback(1)}>No</button>
                            <button className="btn btn_ok btn_primary ms_auto" onClick={() =>goback(2)}>Yes</button>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>

    </>);
};

UserManagementForm.propTypes = {};

export default UserManagementForm;
