import React, { useEffect, useState,useRef } from "react";
import { useNavigate, useSearchParams,useLocation } from "react-router-dom";

import { ToastContainer,toast } from 'react-toastify';
import { sendGet, sendPost, sendPostByJson, sendPostByKeyValue,sendGetKeyValue } from "../../utils/httpUtils.js";
import { setUserInfo } from "../../utils/storage.js";

import { checkIsEmail, ToastInfo, ToastError, ToastSucces, } from "../../utils/check.js";

const UserManagementFormUpdata = (props) => {
    const toastId=useRef(null)
    const navigate = useNavigate();
    const location = useLocation();
    const [active,setActive]=useState(true)  
    const [user,setUserInfo]=useState({
        "userId": 0,
        "email":"",
        "firstName": "",
        "lastName": "",
        "phoneNumber": "",
        "province": "",
        "city": "",
        "address1": "",
        "address2": "",
        "zipCode": "",
        "locationStr":"",
        "Password": "",
    })
    const [locationList,setLocationList]=useState([])
        useEffect(()=>{ 
           
           getUserInfoById(props.uid)
        },[props.id])
  
    // const [step,setStep] =useState();
    const getUserInfoById=async(id)=>{
        const url=`/Account/UserById`
        try {
            const res= await sendGetKeyValue(url,{UserId:id})
            if(res.data){
                setUserInfo(res.data.membershipUser)
            }
        } catch (error) {
            console.log(error);
        }
    }
    //get locations by keyword
    const getLocations=async()=>{
        try {
            const url=""
            const data={}
            const res= await sendPostByJson(url,data)
            if(res.data){
                setLocationList(res.data)
            }
        } catch (error) {
            
        }
    }
    // filter loctions
    const  filterLocations=(e)=>{
        e.preventDefault();
        console.log(user.locationStr);
    }
    const nextStep = (e) => {
        add()

    }

    const add = async () => {

        navigate("/account/user-management", { state: { sortBy: "sort3", statuse: "All", treatmentType: "3 week" } })

    }

    //set active
    const changeAct=()=>{
        setActive(!active)
    }
    //check validate
    const chekeError=()=>{
        if(!user.firstName){
            ToastError("Please enter first name",toastId)
            return false    
        }
        if(!user.lastName){
               ToastError('Please enter last name', toastId)
               return false
        }
        if(!user.phoneNumber){
            ToastError('Please enter phone number', toastId)
            return false
       }
       if(user.phoneNumber.length!=10){
            ToastError('Please enter a phone number with a length of 10', toastId)
            return false
         }
        return true
    }
    //save update 
    const saveUpdate= async ()=>{
        const flag=chekeError()
        if(flag){
            try {
                const url="/Account/UpdateUserBio"
                let data={
                    ...user
                }
                data.id=user.userId
                const res= await sendPostByJson(url,data)
                if(res.data){
                    ToastSucces('Update user info success!', toastId)
                    setTimeout(() => {
                        navigate("/account/user-management")
                    }, 200);
                }else{
                    ToastError('Update user info failed!', toastId)
                }
            } catch (error) {
                 ToastError('Update user info failed!', toastId)
            }
        }
    }
    //cancle
    const cancelUpdate=()=>{
        navigate("/account/user-management")
    }
    return (<>
      <ToastContainer limit={1} />
        <div className="layout">
            <header>
                <div className="nav container">
                    <button className="btn_return me_3" onClick={() => navigate("/account/user-management")} />
                    <div className="title">User management</div>
                </div>
            </header>
           
            <div className="container" >
            <div className=" mb_4" style={{ marginTop: "5vh",display:'flex' }}>
                <label className="form_label" htmlFor=""  style={{ marginRight: "20px",fontWeight:'600',}}>
                                Active
                 </label>
                <div className={`form_switch ${active?'active':''}`} onClick={changeAct}  style={{cursor:"pointer"}}></div>
                  
                </div>
                <form className="form mb_4" id="formOne">
                    <div className="row">
                        <div className="mb_6 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Email
                            </label>

                            <input
                                className="form_control"
                                type="email"
                                placeholder=""
                                disabled
                                value={user.email}
                                onChange={e=>{setUserInfo({...user,email:e.target.value.trim()})}}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                               First Name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="enter first name"
                                value={user.firstName}
                                onChange={e=>{setUserInfo({...user,firstName:e.target.value.trim()})}}
                            />
                        </div>

                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                               Last Name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="enter last name"
                                value={user.lastName}
                                onChange={e=>{setUserInfo({...user,lastName:e.target.value.trim()})}}
                            />
                        </div>

                    </div>
                    <div className="row">
                        <div className="mb_4 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                               Phone Number
                            </label>

                            <input
                                className="form_control"
                                type="text"
                              
                                placeholder="enter phone number"
                                maxLength={10}
                                value={user.phoneNumber}
                                onChange={e=>{setUserInfo({...user,phoneNumber:e.target.value.trim()})}}
                            />
                        </div>

                    </div>


                    <p className="mb_2">Locations</p>
                    <div className="row">
                    <div className="col col_2_sm col_2_md">
                    <div className="search_bar ">
                            <input
                                className="form_control"
                                type="text"
                                placeholder="Search users..."
                                value={user.locationStr}
                                onChange={e=>{setUserInfo({...user,locationStr:e.target.value.trim()})}}
                            />
                            <button className="btn btn_icon" onClick={filterLocations}>
                                <img
                                    src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwXzQ0MDhfMjUyNykiPgo8cGF0aCBkPSJNMTUuNTAwNiAxNC4wMDA2SDE0LjcxMDZMMTQuNDMwNiAxMy43MzA2QzE1LjYzMDYgMTIuMzMwNiAxNi4yNTA2IDEwLjQyMDYgMTUuOTEwNiA4LjM5MDYzQzE1LjQ0MDYgNS42MTA2MyAxMy4xMjA2IDMuMzkwNjMgMTAuMzIwNiAzLjA1MDYzQzYuMDkwNjMgMi41MzA2MyAyLjUzMDYzIDYuMDkwNjMgMy4wNTA2MyAxMC4zMjA2QzMuMzkwNjMgMTMuMTIwNiA1LjYxMDYzIDE1LjQ0MDYgOC4zOTA2MyAxNS45MTA2QzEwLjQyMDYgMTYuMjUwNiAxMi4zMzA2IDE1LjYzMDYgMTMuNzMwNiAxNC40MzA2TDE0LjAwMDYgMTQuNzEwNlYxNS41MDA2TDE4LjI1MDYgMTkuNzUwNkMxOC42NjA2IDIwLjE2MDYgMTkuMzMwNiAyMC4xNjA2IDE5Ljc0MDYgMTkuNzUwNkMyMC4xNTA2IDE5LjM0MDYgMjAuMTUwNiAxOC42NzA2IDE5Ljc0MDYgMTguMjYwNkwxNS41MDA2IDE0LjAwMDZaTTkuNTAwNjMgMTQuMDAwNkM3LjAxMDYzIDE0LjAwMDYgNS4wMDA2MyAxMS45OTA2IDUuMDAwNjMgOS41MDA2M0M1LjAwMDYzIDcuMDEwNjMgNy4wMTA2MyA1LjAwMDYzIDkuNTAwNjMgNS4wMDA2M0MxMS45OTA2IDUuMDAwNjMgMTQuMDAwNiA3LjAxMDYzIDE0LjAwMDYgOS41MDA2M0MxNC4wMDA2IDExLjk5MDYgMTEuOTkwNiAxNC4wMDA2IDkuNTAwNjMgMTQuMDAwNloiIGZpbGw9IiM4RThFOEUiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMF80NDA4XzI1MjciPgo8cmVjdCB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIGZpbGw9IndoaXRlIi8+CjwvY2xpcFBhdGg+CjwvZGVmcz4KPC9zdmc+Cg=="
                                    alt=""
                                />
                            </button>
                            <ul className="">
                                <li className="my_3 my_5_sm d_flex">
                                    <input className="form_checkbox" type="checkbox" />
                                    <label className="checkbox_label mt_1 ms_2">
                                        <div className="color_primary700">
                                            <span className="fw_medium">legal name</span> (owner name)
                                        </div>
                                        <div className="mt_2 fs_sm">
                                            4000 La Rica Ave Suite D Baldwin Park, CA 91706
                                        </div>
                                    </label>
                                </li>
                                <li className="my_3 my_5_sm d_flex">
                                    <input className="form_checkbox" type="checkbox" />
                                    <label className="checkbox_label mt_1 fs_sm ms_2">
                                        <div className="color_primary700">
                                            <span className="fw_medium">legal name</span> (owner name)
                                        </div>
                                        <div className="mt_2 fs_sm">
                                            4000 La Rica Ave Suite D Baldwin Park, CA 91706
                                        </div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                       
                    </div>

                </form>

                <div className=" filter_footer">
                    <div className="btn_group">
                        <button className="btn btn_primary_line_transparent" onClick={cancelUpdate}>Cancel</button>
                        <button className="btn btn_primary ms_auto col" onClick={saveUpdate} >Apply</button>
                    </div>
                </div>
                {/* <button className="btn btn_primary w_100 mt_3" onClick={() => nextStep()}>Create User</button> */}
            </div>
        </div>
    </>);
};

UserManagementFormUpdata.propTypes = {};

export default UserManagementFormUpdata;
