import React, { useState, PureComponent } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";
import { MapContainer, TileLayer, useMap,Marker,Popup } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'; 
const DashboardMap = (props) => {
  const navigate = useNavigate();
  

  return (<>
  <MapContainer center={[51.505, -0.09]} zoom={13} scrollWheelZoom={false}  style={{ width: '100%', height: '280px' }}>
  <TileLayer
    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
  />
  {/* <Marker position={[51.505, -0.09]}>
    <Popup>
      A pretty CSS3 popup. <br /> Easily customizable.
    </Popup>
  </Marker> */}
</MapContainer>
  </>);
};

DashboardMap.propTypes = {};

export default DashboardMap;
