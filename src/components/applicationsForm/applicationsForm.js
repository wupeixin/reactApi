import React, { useState,useEffect,useRef } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { ToastContainer } from 'react-toastify';
import { checkIsEmail, ToastInfo, ToastError, } from "../../utils/check.js";
import { sendGet, sendPost, sendPostByJson } from "../../utils/httpUtils.js";

import step_0_Img from '../../assets/st_0.png'
import step_1_Img from '../../assets/st_1.png'
import warn_Img from '../../assets/warn.png'

const ApplicationsForm = (props) => {
    const toastId =useRef(null)
    const navigate = useNavigate();
    const [stepImg,setStepImg]=useState(step_0_Img)
    const [step,setStep]=useState(1)
    
    const [application,setApplication]=useState({
        id:"",
        referenceID:"",
        treatmentType:"",
        amount:"",
        paymentMonth:"",
        firstName:"",
        lastName:"",
        middleName:"",
        suffix:"",
        ssn:"",
        ssnLastFour:'',
        date_year:"",
        date_month:"",
        date_day:"",
        income:"",
        email:"",
        address:"",
        street:"",
        city:"",
        state:"",
        zipCode:"",
        country:"",
        primaryPhone:"",
        alternatePhone:""
    })  



  
    const nextStep = (e) => {
        //check first page input
        if(!application.referenceID){
            ToastError("Please enter your reference ID!",toastId)
            return false
        }
       setStepImg(step_1_Img)
       setStep(2)

    }
    const getBack = async () => {
        navigate("/account/applications")
    }
    const add = async () => {
        if(!application.firstName){
            ToastError("Please enter your first name!",toastId)
            return false
        }
        if(!application.lastName){
            ToastError("Please enter your last name!",toastId)
            return false
        }
        if(application.email&&!checkIsEmail(application.email)){
            ToastError("Please enter the correct email address format!",toastId)
            return false
        }
        if(application.primaryPhone&&application.primaryPhone.length!=10){
            ToastError("Please enter a 10 digit primary phone number!",toastId)
            return false
        }
        if(application.alternatePhone&&application.alternatePhone.length!=10){
            ToastError("Please enter a 10 digit alternate phone number!",toastId)
            return false
        }
        saveDate()
        // navigate("/account/applications", { state: { sortBy: "Date", statuse: "All", treatmentType: "1 week" } })
    }
    const saveDate=async()=>{
        try {
            const url=""
            const data={}
            const res= await sendPostByJson(url,data)
            if(res.data){
                navigate("/account/applications")
            }
        } catch (error) {
            console.log(error);
        }
    }
    const backStep=()=>{
        if(step==1){
            return false
        }
        setStep(1)
        setStepImg(step_0_Img)
    }
    return (<>
     <ToastContainer limit={1} />
        <div className="layout">
            <header>
                <div className="nav container">
                    <button className="btn_return me_3" onClick={() => navigate("/account/applications")} />
                    <div className="title">New applications</div>
                </div>
            </header>
            <div className="login_container"  style={{maxWidth:"100%"}}> <div className=" mb_5" style={{ marginTop: "5vh" }}>
                <img id="imgOne"   onClick={backStep}
                   src={stepImg}
                />
              
            </div></div>

            <div className="container" >

                <form className="form mb_5" id="formOne" style={{display:`${step==1?'block':'none'}`}}>
                    <div className="row"> 
                        <div className="mb_3 col col_2_sm col_2_md">
                            <label className="form_label" htmlFor="">
                                Your Reference ID
                            </label>

                            <input
                                className="form_control mb_1"
                                type="text"
                                value={application.referenceID}
                                placeholder="Please enter Reference ID"
                                onChange={(e)=>{setApplication({...application,referenceID:e.target.value.trim()})}}
                            />
                            <div className="d_flex ">
                                <img src={warn_Img} style={{width:"16px",height:"16px"}}></img>
                                <span className="notic_text">This is your reference ID which can be used later to find your applicants.</span>
                            </div>
                        </div>
                    </div>
                    <div className="row"> <div className="mb_5 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Treatment type
                        </label>
                        <select className="form_control" value={application.treatmentType} onChange={(e)=>{setApplication({...application,treatmentType:e.target.value})}} 
                         placeholder="Please select treatment type">
                            <option value='1' >type1</option>
                            <option value='2' >type2</option>
                        </select>
                    </div></div>
                    <div className="row"><div className="mb_5 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Amount
                        </label>

                        <input
                            className="form_control"
                            type="select"
                            placeholder="Please enter amount"
                            value={application.amount}
                            onChange={(e)=>{setApplication({...application,amount:e.target.value.trim()})}}
                        />
                    </div></div>
                    <div className="row"> <div className="mb_5 col col_2_sm col_2_md">
                        <label className="form_label" htmlFor="">
                            Preferred monthly payment
                        </label>

                        <input
                            className="form_control"
                            type="select"
                            placeholder="Please enter  preferred monthly paymentunt"
                            value={application.paymentMonth}
                            onChange={(e)=>{setApplication({...application,paymentMonth:e.target.value.trim()})}}
                        />
                    </div></div>




                </form>

                <form className="form mb_5" id="formTwo" style={{display:`${step==2?'block':'none'}`}}>
                  
                    <div className="row">
                        <div className="mb_5 col col_2_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                First name*
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter first name"

                            />
                        </div>
                        <div className="mb_5 col col_2_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                Last name*
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter last name"

                            />
                        </div>
                        <div className="mb_5 col col_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                Middle name
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter middle name"

                            />
                        </div>

                    </div>
                    <div className="row">
                        <div className="mb_5 col col_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                Suffix
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter your suffix"

                            />
                        </div>
                        <div className="mb_5 col col_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                SSN
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter your SSN number"

                            />
                        </div>
                        <div className="mb_5 col col_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                SSN last 4 digit
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter your SSN last 4 digit"

                            />
                        </div>


                    </div>

                    <div className="row">
                        <div className="col col_2_sm col_1_md">
                            <div className="row">

                                <div className="mb_5 col_3 col_3_sm col_3_md">
                                    <label className="form_label" htmlFor="">
                                        Date of birth
                                    </label>
                                    <select className="form_control" placeholder="Year">
                                        <option value='1' >type1</option>
                                        <option value='2' >type2</option>
                                    </select>
                                </div>
                                <div className="mb_5 col_3 col_3_sm col_3_md">
                                    <label className="form_label" htmlFor="">
                                        Date of birth
                                    </label>
                                    <select className="form_control" placeholder="Month">
                                        <option value='1' >type1</option>
                                        <option value='2' >type2</option>
                                    </select>
                                </div> <div className="mb_5 col_3 col_3_sm col_3_md">
                                    <label className="form_label" htmlFor="">Date of birth
                                    </label>
                                    <select className="form_control" placeholder="Date">
                                        <option value='1' >type1</option>
                                        <option value='2' >type2</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="row">
                        <div className="mb_5 col col_sm col_1_md">
                            <label className="form_label" htmlFor="">
                                Annual income
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter  annual income"

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_5 col col_sm col_1_md">
                            <label className="form_label" htmlFor="">
                                Email
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter email address"

                            />
                        </div>
                    </div>


                    <hr className="mb_5" style={{ borderTop: "1px dashed #BDBDBD " }}></hr>
                    <h5 className="mb_5">Address</h5>
                    <div className="row">
                        {/* <div className="mb_5 col col_2_sm col_2_md"> */}
                        {/* <div className="row"> */}
                        <div className="mb_5 col col_2_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                Street 1
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter street address"

                            />
                        </div>
                        <div className="mb_5 col col_2_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                Street 2
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter street address"

                            />
                        </div>
                    

                    </div>
                    <div className="row">
                        <div className=" col_2 col_4_sm col_4_md">
                            <label className="form_label" htmlFor="">
                                City
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="please enter city name "

                            />
                        </div>
                        <div className=" col_2 col_4_sm col_4_md">
                            <label className="form_label" htmlFor="">
                                State
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter state name"

                            />
                        </div>
                        <div className="mb_5 col_2 col_4_sm col_4_md">
                            <label className="form_label" htmlFor="">
                                Zip code
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter zip code"

                            />
                        </div>
                        <div className="mb_5 col_2 col_4_sm col_4_md">
                            <label className="form_label" htmlFor="">
                                Country
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter country name"

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="mb_5 col col_2_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                Primary phone number
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter  primary phone number"

                            />
                        </div>
                        <div className="mb_5 col col_2_sm col_3_md">
                            <label className="form_label" htmlFor="">
                                Alternate phone number
                            </label>

                            <input
                                className="form_control"
                                type="text"
                                placeholder="Please enter alternate phone number"

                            />
                        </div>
                    </div>






                </form>

                <div className="offcanvas_footer filter_footer" id="buttonOne" style={{display:`${step==1?'block':'none'}`}}>
                    <div className="btn_group">
                        {/* <button className="btn btn_primary_line_transparent" onClick={getBack}>Cancel</button> */}
                        <button className="btn btn_primary ms_auto col" onClick={nextStep} >Next</button>
                    </div>
                </div>

                <div className="offcanvas_footer filter_footer" id="buttonTwo" style={{display:`${step==2?'block':'none'}`}}>
                    <div className="btn_group">
                        <button className="btn btn_primary_line_transparent" onClick={getBack}>Cancel</button>
                        <button className="btn btn_primary ms_auto col" onClick={add} >Confirm</button>
                    </div>
                </div>
            </div>
        </div>
    </>);
};

ApplicationsForm.propTypes = {};

export default ApplicationsForm;
