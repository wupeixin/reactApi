const webpack = require("webpack");
const path = require("path");
const protocol = process.env.HTTPS === "true" ? "https" : "http";
const Dotenv = require("dotenv-webpack");
const plugins = [
  new Dotenv({
    path: `./.env`,
    allowEmptyValues: true, // allow empty variables (e.g. `FOO=`) (treat it as empty string, rather than missing)
    systemvars: true, // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
    silent: false, // dont hide any errors
    defaults: false, // load '.env.defaults' as the default values if empty.
  }),
  new webpack.ProvidePlugin({
    process: "process/browser",
    Buffer: ["buffer", "Buffer"],
  }),
];
module.exports = {
  plugins: plugins,
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
        exclude: /\.module\.(sa|sc|c)ss$/,
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
              modules: {
                localIdentName: "[local]--[hash:base64:5]-wcd",
              },
            },
          },
          "sass-loader",
        ],
        include: /\.module\.(sa|sc|c)ss$/,
      },
    ],
  },
  // Other webpack config...
};
